import logging
import os
from jira import JIRA
from functools import lru_cache
import pandas as pd
import requests

from xdc_core.xdc_helper.helper import get_1digit_nace_code
from xdc_core.xdc_error import ErrorCodes, XdcError
import xdc_core.xdc_helper.economic_adjustments_helper as EconomicAdjustmentHelper
import xdc_core.xdc_helper.data_api_helper as DataApiHelper

logging.getLogger(__name__)


def get_jira_login() -> JIRA:
    """Log in Jira by either using the env variable JIRA_USER
    or asking to input a jira user (without the domain)

    :return: An instance of jira
    :rtype: JIRA
    """
    try:
        user = os.environ['JIRA_USER']
    except:
        user = input(
            'Provide user name for jira (domain @right-basedonscience.de):\n')
    try:
        apikey = os.environ['JIRA_TOKEN']
    except:
        apikey = input('Provide the token for the user %s:\n' % user)

    user = user + '@right-basedonscience.de'
    print('Using %s as jira user' % user)

    server = 'https://right-basedonscience.atlassian.net'

    options = {'server': server}

    jira = JIRA(options, basic_auth=(user, apikey))

    return jira


class DataHelper:
    """Provides functionality to query data from the Database"""
    @staticmethod
    def fetch_company_data_from_db(df: pd.DataFrame, base_year: int):
        """Retrieve company data from the DB

        :param df: dataframe containing ISINs to query
        :type df: pd.DataFrame
        :param base_year: year for which the data should be retrieved
        :type base_year: int
        :raises XdcError: Abort if no ISINs are found
        :raises XdcError: Abort if DB gives error
        :return: dataframe with isins, gva, scope1-3, region and nace
        :rtype: pd.DataFrame
        """
        if not 'isin' in df.columns.to_list():
            miss_key = 'isin'
            er_msg = "The following necessary input data is missing: %s" % miss_key
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)

        isin_list = df['isin'].values.tolist()

        db_data = DataApiHelper.get_companies_from_isins(base_year=base_year,
                                                         currency="usd",
                                                         isin=isin_list)

        # TODO: deal with eventual errors when connecting to the db
        if 'error' in db_data:
            logging.error(db_data['message'])
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, db_data['message'])

        company_dicts = db_data['data']
        company_count = db_data['companyCount']
        obtained_isin_list = list(map(lambda c_d: c_d['isin'], company_dicts))
        # Check that all necessary data can be retrieved
        if company_count < len(isin_list):
            isins = list(set(isin_list) - set(obtained_isin_list))

            logging.error(
                "Data for the following %i isin codes are missing: %s",
                len(isins), str(isins))
        elif company_count > len(isin_list):
            import collections
            duplicate = [
                item for item, count in collections.Counter(
                    obtained_isin_list).items() if count > 1
            ]
            logging.warning("Possible duplicate ISIN found, %s",
                            str(duplicate))

        # Fill dataframe with DB data
        for i in range(len(df)):
            target_isin = df.loc[i]['isin']

            if target_isin in obtained_isin_list:
                target_dict = list(
                    filter(lambda c_d: c_d['isin'] == target_isin,
                           company_dicts))[0]
                if target_dict['gva'] != None:
                    if target_dict['gva'] > 0 and target_dict[
                            'scope1'] >= 0 and target_dict[
                                'scope2'] >= 0 and target_dict['scope3'] >= 0:
                        df.loc[i, 'gva'] = target_dict['gva']
                        df.loc[i, 'scope1'] = target_dict['scope1']
                        df.loc[i, 'scope2'] = target_dict['scope2']
                        df.loc[i, 'scope3'] = target_dict['scope3']
                        df.loc[i, 'region'] = target_dict['regionCode']
                        df.loc[i, 'nace'] = target_dict['naceCode']
                        df.loc[i, 'country'] = target_dict['countryCode']
                    else:
                        if target_dict['gva'] <= 0:
                            print("Discarded %s due to negative GVA: %i" %
                                  (target_dict['isin'], target_dict['gva']))
                        else:
                            print(
                                "Discarded %s due to negative emissions: %s" %
                                (target_dict['isin'], [
                                    target_dict['scope1'],
                                    target_dict['scope2'],
                                    target_dict['scope3']
                                ]))
        df.dropna(axis=1, how="all", inplace=True)
        return df

    @staticmethod
    def fetch_sector_data_from_db(df: pd.DataFrame,
                                  base_year: int,
                                  currency: str = "usd"):
        """Query sector EEI for a df with nace codes for base_year from the DB

        :param df: dataframe containing data, including a column of nace codes
        :type df: pd.DataFrame
        :param base_year: year for which the sector EEI should be retrieved
        :type base_year: int
        :raises XdcError: abort if no nace codes are found
        :return: dataframe provided with the sector eei added to them
        :rtype: pd.DataFrame
        """
        if 'nace' not in df.columns.to_list():
            miss_key = 'nace'
            er_msg = "The following necessary input data is missing: %s" % miss_key
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)
        # the set makes this a list of unique entries, split removes the 3,4 digit keeps only first 1-2 digit of nace code,
        # zfill pads with 0 if it's a single digit nace code (eg 6 instead of 06).
        # nace_list_1dig collects all nace codes that are single letters
        nace_list = set([(str(x).split('.')[0]).zfill(2)
                         for x in df['nace'].values.tolist()
                         if (str(x).split('.')[0].isdigit())])
        nace_list_1dig = set([
            str(x) for x in df['nace'].values.tolist()
            if (len(str(x)) == 1) and (str(x).isalpha())
        ])
        nace_list.update(nace_list_1dig)

        nace_dict = {}

        # TODO: why do we need this?
        nace_dict['na'] = [
            float('nan'),
            float('nan'),
            float('nan'),
            float('nan')
        ]
        for nace in nace_list:
            company_count, db_output = EconomicAdjustmentHelper.get_adjusted_sector_eei(
                nace_code=nace, base_year=base_year, currency=currency)
            #company_count = db_output['company_count']
            if company_count < 30:
                # nace code fallback to top level is already handled by
                # data_api_helper. Atm we do not propagate the actual used
                # nace code into the tool.
                nace_dict[nace] = [
                    nace, "Number of companies too low:", company_count,
                    float("Nan")
                ]
            else:
                nace_dict[nace] = [
                    nace, db_output['eei_scope1'], db_output['eei_scope2'],
                    db_output['eei_scope3']
                ]

        for nace in nace_list_1dig:
            nace_code = nace.upper()
            if not nace_code in nace_dict.keys():
                db_output = EconomicAdjustmentHelper.get_adjusted_sector_eei(
                    nace_code=nace, base_year=base_year, currency=currency)
                if db_output['company_count'] < 30:
                    er_msg = "Sector with nace_code" + nace_code + \
                        " has less than 30 companies and no parent is known"
                    logging.warning(er_msg)
                    nace_dict[nace_code] = [
                        nace_code, "Number of companies too low:",
                        db_output['company_count'],
                        float("Nan")
                    ]
                else:
                    # add the top-level sector to the dictionary
                    nace_dict[nace] = [
                        nace_code, db_output['eei_scope1'],
                        db_output['eei_scope2'], db_output['eei_scope3']
                    ]

        # Fill dataframe with EEIs from DB
        for index, row in df.iterrows():
            nace_code = str(row['nace'])
            if (len(str(nace_code)) == 1) and (str(nace_code).isalpha()):
                dict_index = nace_code
            else:
                dict_index = nace_code.split('.')[0][:2].zfill(2)
            #df.loc[index, 'nace used'] = nace_dict[dict_index][0]
            df.loc[index, 'sector_eei_scope1'] = nace_dict[dict_index][1]
            df.loc[index, 'sector_eei_scope2'] = nace_dict[dict_index][2]
            df.loc[index, 'sector_eei_scope3'] = nace_dict[dict_index][3]

        return df
