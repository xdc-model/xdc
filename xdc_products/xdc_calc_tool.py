#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import argparse
import logging
import os
import re
import sys
from copy import deepcopy
from openpyxl import load_workbook
import pandas as pd
from tqdm import tqdm
from dataclasses import dataclass

import xdc_core.xdc_logging as xdc_logging
from xdc_core import xdc_constants as consts
from xdc_core.emission import Emission
from xdc_core.growth import Growth
from xdc_core.scenario_code import ScenarioCode
from xdc_core.country_code import CountryCode
from xdc_core.xdc import XDC
from xdc_core.xdc_error import ErrorCodes, XdcError
from xdc_products.data_helper import DataHelper
from xdc_core.xdc_helper.helper import get_iea_sector_for_nace_code, check_and_get_food_mode_nace_code
import xdc_core.xdc_helper.economic_adjustments_helper as EconomicAdjustmentsHelper


@dataclass
class XDCCalcTool:

    sheet_name: int
    precision: int
    base_year: int
    target_year: int
    TwoDS: bool
    BeyondTwoDS: bool
    sector: bool
    target: bool
    baseline: bool
    portfolio: bool
    verbose: int
    # filtered: bool
    # no_report: bool
    data_complete: bool = True
    eei_complete: bool = True
    food_manufacture_company: bool = False
    ssp_scenario = 'ssp2'

    def __post_init__(self):
        if self.target:
            self.TwoDS = True
            self.BeyondTwoDS = True

    def _check_file_existance(self, file_path: str, message: str = None):
        """Checks that the input file exists.

        :param file_path: Path of the input file
        :type file_path: str
        :param message: Custom error message, defaults to None
        :type message: str, optional
        :raises FileNotFoundError: In case the input file cannot be found
        """
        if not os.path.exists(file_path):
            if message:
                msg = message
            else:
                msg = "File " + file_path + " could not be found"

            logging.error(msg)
            raise FileNotFoundError(msg)

    def load_input_data(self, input_file_path: str):
        """Starting from the input file path the following steps are performed:
            1) check that the file exists
            2) if no sheet name is specified look for the first valid sheet
            3) read the input into a dataframe and adjust columns
            4) check that columns match the calculation mode

        :param input_file_path: Path of the input file
        :type input_file_path: str
        :raises XdcError: For mismatch between calculation mode and input data
        :raises ValueError: For incompatible weight values
        """
        logging.info("Start loading data")
        # 1) check that the file exists
        self._check_file_existance(input_file_path)

        # 2) if no sheet name is specified look for the first valid sheet
        if self.sheet_name is None:
            xl = load_workbook(input_file_path)
            sheet_names = xl.sheetnames
            for sheet_name in sheet_names:
                if sheet_name != "__FDSCACHE__":
                    self.sheet_name = sheet_name
                    logging.debug("Sheet name set to %s", self.sheet_name)
                    break
        self.input_file_name = input_file_path

        # 3) read the input into a dataframe and adjust columns
        try:
            self.df = pd.read_excel(input_file_path,
                                    self.sheet_name,
                                    index=True,
                                    engine="openpyxl")
            logging.debug("Reading Excel file")
        except:
            self.df = pd.read_json(input_file_path)
            logging.debug("Reading JSON file")

        # convert column names into lower cases
        self.df.columns = map(str.lower, self.df.columns)

        # check column names for alpanum characters
        self.df.columns = map(lambda x: re.sub(r"\W+", "", x), self.df.columns)

        # 4) check that columns match the calculation mode

        # If we need sector values and nace code is not given, query company data:
        if self.sector and "nace" not in self.df.columns:
            self.baseline = True
        expected_columns = []

        if self.portfolio:
            logging.info("Portfolio mode enabled, expecting weights")
            if "weight" not in self.df.columns:
                miss_key = "weight"
                er_msg = ("weight is missing and portfolio mode has been " +
                          "chosen, input can not be processed")
                # Check that the necessary columns are a subset of the existing
                # ones (won't throw an error if nace are presents, but sector
                # xdcs aren't calculated)
                logging.error(er_msg)
                raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)
                # Check that all weights are positive
            negative_weights = list(filter(lambda w: w < 0, self.df.weight))
            if len(negative_weights) != 0:
                message = u"\N{cross mark} " + "Negative weight(s) found"
                logging.error(message)
                raise ValueError(message)

            # Check that it is equal to 1 and return false otherwise
            if abs(1 - self.df.weight.sum()) > 10e-6:
                message = (u"\N{cross mark} " +
                           "Sum of weights is not equal to 1. Difference %e" %
                           (1 - self.df.weight.sum()))
                logging.error(message)
                raise ValueError(message)

            logging.debug("Weights are ok")
        if "isin" not in self.df.columns:
            miss_key = "ISIN"
            er_msg = "ISIN is missing, input can not be processed"
            # Check that the necessary columns are a subset of the existing
            # ones (won't throw an error if nace are presents, but sector
            # xdcs aren't calculated)
            logging.error(er_msg)
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)
        expected_columns = ["gva", "scope1", "scope2", "scope3", "country"]

        if self.sector or self.target:
            logging.info("Sector mode enabled, expecting NACE code")
            expected_columns.append("nace")

        # If none of the expected columns are present, load from DB:
        if all([column not in self.df.columns for column in expected_columns]):

            self.df = DataHelper.fetch_company_data_from_db(
                df=self.df, base_year=self.base_year)
        # If some of the data is missing abort:
        elif any(
            [column not in self.df.columns for column in expected_columns]):
            miss_key = str(set(expected_columns) - set(self.df.columns))
            er_msg = (
                "Local data only provided partially. Can not mix " +
                "self-reported and public data. Please add the columns: %s" %
                (miss_key))
            # Check that the necessary columns are a subset of the existing ones
            # (won't throw an error if nace are presents,
            # but sector xdcs aren't calculated)
            logging.error(er_msg)
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)
        # else use the DF as it's been loaded from the file
        if self.sector or self.target:
            sector_eei_list = (
                "sector_eei_scope1",
                "sector_eei_scope2",
                "sector_eei_scope3",
            )
            expected_columns.extend(sector_eei_list)
            # If none of the EEIs are provided use info from DB
            if all(column not in self.df.columns
                   for column in sector_eei_list):
                self.df = DataHelper.fetch_sector_data_from_db(
                    df=self.df, base_year=self.base_year)

        self.df["base_year"] = self.base_year
        if not set(expected_columns) <= set(self.df.columns.to_list()):
            miss_key = str(set(expected_columns) - set(self.df.columns))
            er_msg = "The following necessary input data is missing: %s" % miss_key
            logging.error(er_msg)
            raise XdcError(ErrorCodes.ERR_MISSING_VAL, miss_key, er_msg)

    def run_calculation(self, silent=False):
        """
        Executes the actual calculation according to the input read into self.df
        Baseline, Sector and Target XDC are calculated for each row depending
        on which flag is active.
        If portfolio omde is enabled, the returned
        emissions are added up to calculate the portfolio XDCs at the end.
        """
        if self.portfolio:
            self.portfolioxdcs = {}
        cache_xdc = {}
        if silent:
            for index, row in self.df.iterrows():
                self.run_calculation_wrapped(index, row, cache_xdc)
        else:
            for index, row in tqdm(self.df.iterrows(),
                                   total=self.df.shape[0],
                                   desc='Calculating dataframe XDCs'):
                self.run_calculation_wrapped(index, row, cache_xdc)

    def run_calculation_wrapped(self, index, row, cache_xdc):
        if not row.isnull().values.any():
            logging.info("Calculating values for ISIN %s", row["isin"])
            logging.debug("Values in dataframe row: %s", row)
            nace_code = str(row["nace"])
            # These should always be zero unless food_manufacture is on
            # and the company is in sector 10 or 11
            self.food_manufacture_company = False
            food_mode, _ = check_and_get_food_mode_nace_code(nace_code)
            if food_mode:
                _, nace_code = check_and_get_food_mode_nace_code(nace_code)
                self.food_manufacture_company = True
            if self.baseline:
                xdctype = "baseline"
                baseline_emissions = self.calc_baseline_emissions(
                    company_gva=row["gva"],
                    company_scopes=[
                        row["scope1"], row["scope2"], row["scope3"]
                    ],
                    country=CountryCode[row["country"]],
                    nace_code=nace_code,
                    portfolio=self.portfolio,
                    ssp_scenario=self.ssp_scenario)
                if self.portfolio:
                    # baseline_portfolio_emissions = self.calc_baseline_emissions(
                    #     company_gva=row["gva"],
                    #     company_scopes=[
                    #         row["scope1"], row["scope2"], row["scope3"]
                    #     ],
                    #     country=CountryCode.value_of(row["country"]),
                    #     nace_code=nace_code,
                    #     portfolio=True)
                    self.add_to_portfolio_xdc(
                        emissions=baseline_emissions,
                        weight=row["weight"],
                        xdctype=xdctype,
                    )
                self.add_xdc_to_df(index, baseline_emissions, xdctype)

            if self.sector:
                xdctype = "sector"
                sector_eeis = [
                    row["sector_eei_scope1"], row["sector_eei_scope2"],
                    row["sector_eei_scope3"]
                ]
                cache_key = str(xdctype + "_" + row["country"] + "_" + "_" +
                                str(row["nace"]))
                if cache_key in cache_xdc.keys():
                    logging.debug("XDC taken from cache")
                    sector_emissions = cache_xdc[cache_key]
                else:
                    sector_emissions = self.calc_sector_emissions(
                        sector_eeis=sector_eeis,
                        nace_code=nace_code,
                        portfolio=self.portfolio,
                        ssp_scenario=self.ssp_scenario)
                    cache_xdc[cache_key] = deepcopy(sector_emissions)
                if self.portfolio:
                    # USE sector wrapper
                    # sector_portfolio_emissions = self.calc_sector_emissions(
                    #     sector_eeis=sector_eeis,
                    #     nace_code=nace_code,
                    #     portfolio=True)
                    self.add_to_portfolio_xdc(
                        emissions=sector_emissions,
                        weight=row["weight"],
                        xdctype=xdctype,
                    )
                self.add_xdc_to_df(index, sector_emissions, xdctype)
            nace_code = str(row['nace']).split('.')[0].zfill(2)
            if self.target and nace_code not in ('01', '02', '03', '0A'):

                sector_eeis = [
                    row["sector_eei_scope1"],
                    row["sector_eei_scope2"],
                    row["sector_eei_scope3"],
                ]
                iea_sector = get_iea_sector_for_nace_code(str(row["nace"]))
                self.df.loc[index, "IEA sector"] = iea_sector
                scenarios = []
                if self.BeyondTwoDS:
                    scenarios.append(ScenarioCode.BTDS)
                if self.TwoDS:
                    scenarios.append(ScenarioCode.TDS)
                for scenario in scenarios:
                    xdctype = "Target_%s" % scenario.value
                    cache_key = str(xdctype + "_" + row["country"] + "_" +
                                    "_" + str(row["nace"]))
                    if cache_key in cache_xdc.keys():
                        logging.debug("XDC taken from cache")
                        target_emissions = cache_xdc[cache_key]
                    else:
                        target_emissions = self.calc_target_emissions(
                            scenario=scenario,
                            eei_values=sector_eeis,
                            nace_code=nace_code,
                            portfolio=self.portfolio)
                        cache_xdc[cache_key] = deepcopy(target_emissions)
                    if self.portfolio:
                        # target_portfoilio_emissions = self.calc_target_emissions(
                        #     scenario=scenario,
                        #     sector=iea_sector,
                        #     eei_values=sector_eeis,
                        #     nace_code=nace_code,
                        #     portfolio=True)
                        self.add_to_portfolio_xdc(
                            emissions=target_emissions,
                            weight=row["weight"],
                            xdctype=xdctype,
                        )
                    self.add_xdc_to_df(index, target_emissions, xdctype)
        else:
            self.data_complete = False
            self.eei_complete = False

    def calc_baseline_emissions(self,
                                company_gva: float,
                                company_scopes: list,
                                country: CountryCode,
                                nace_code: str,
                                portfolio: bool = False,
                                ssp_scenario: str = 'ssp2'):
        """"Calculates the three scopes and adds them to an emission object

        :param company_gva: GVA of the company
        :type company_gva: float
        :param company_scopes: Scope 1, 2 and 3 of the company
        :type company_scopes: list
        :param country: Country code of the company
        :type country: str
        :param nace_code: Nace code of the company's sector
        :type nace_code: str
        :param portfolio: If set, XDCs are consolidated, defaults to False
        :type portfolio: bool, optional
        :return: Emissions object
        :rtype: Emissions
        """

        if self.food_manufacture_company:
            emissions = Emission.for_food_mode_company_baseline(
                base_year=self.base_year,
                target_year=self.target_year,
                country_code=country,
                company_gva=company_gva,
                company_scopes=company_scopes,
                nace_code=nace_code,
                portfolio=portfolio,
                ssp_scenario=ssp_scenario)
        else:
            emissions = Emission.for_company_baseline(
                base_year=self.base_year,
                target_year=self.target_year,
                country_code=country,
                company_gva=company_gva,
                company_scopes=company_scopes,
                ssp_scenario=ssp_scenario)

        return emissions

    def calc_sector_emissions(self,
                              sector_eeis: list,
                              nace_code: str,
                              portfolio: bool = False,
                              ssp_scenario: str = ssp_scenario):
        """Calculates the three scopes and adds them to an emission object
        
        :param sector_eeis: EEIs of the sector
        :type sector_eeis: list
        :param nace_code: Nace code of the sector
        :type nace_code: str
        :param portfolio: If set, XDCs are consolidated, defaults to False
        :type portfolio: bool, optional
        :return: Emissions object containing the scopes
        :rtype: Emission
        """
        if self.food_manufacture_company:
            emissions = Emission.for_food_mode_sector_baseline(
                base_year=self.base_year,
                target_year=self.target_year,
                sector_eeis=sector_eeis,
                nace_code=nace_code,
                portfolio=portfolio,
                ssp_scenario=ssp_scenario)
        else:
            emissions = Emission.for_sector_baseline(
                base_year=self.base_year,
                target_year=self.target_year,
                sector_eeis=sector_eeis,
                nace_code=nace_code,
                ssp_scenario=ssp_scenario)

        return emissions

    def calc_target_emissions(self,
                              scenario: ScenarioCode,
                              eei_values: list,
                              nace_code: str,
                              portfolio: bool = False):
        """Calculates the three scopes based on the selected IEA/RCP budget
        and adds them to an emission object that is returned
        
        :param scenario: Reduction scenario code of [2DS, B2DS]
        :type scenario: ScenarioCode
        :param eei_values: EEI values of the sector
        :type eei_values: list
        :param nace_code: Nace code of the sector
        :type nace_code: str
        :param portfolio: If set, XDCs are consolidated, defaults to False
        :type portfolio: bool, optional
        :return: Emissions object containing the scopes
        :rtype: Emission
        """
        #scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        if self.food_manufacture_company:
            emissions = Emission.for_food_mode_sector_target(
                base_year=self.base_year,
                target_year=self.target_year,
                scenario_code=scenario,
                sector_eeis=eei_values,
                nace_code=nace_code,
                portfolio=portfolio)
        else:
            emissions = Emission.for_sector_target(
                base_year=self.base_year,
                target_year=self.target_year,
                scenario_code=scenario,
                sector_eeis=eei_values,
                nace_code=nace_code)

        return emissions

    def add_to_portfolio_xdc(self, emissions: Emission, weight: float,
                             xdctype: str):
        """Adds the specified emissions to the portfolio emissions,
        multiplying it with the weight first.


        :param emissions: Emissions to be added to the portfolio emissions
        :type emissions: Emission
        :param weight: weight with which the emissions enter into the portfolio
        :type weight: float
        :param xdctype: type of XDC that is being calculated.
        :type xdctype: str
        """
        scaled_emissions = deepcopy(emissions)
        if "total_scope" in scaled_emissions.scopes.keys():
            scaled_emissions.remove_scope("total_scope")
        scaled_emissions.scale(weight)
        if xdctype not in self.portfolioxdcs.keys():
            self.portfolioxdcs[xdctype] = scaled_emissions
        else:

            self.portfolioxdcs[
                xdctype] = self.portfolioxdcs[xdctype] + scaled_emissions

    def add_xdc_to_df(self,
                      index: int,
                      emissions: Emission,
                      xdctype: str,
                      ssp_scenario: str = 'ssp2'):
        """Takes the emission object and calculates the XDC. Adds the XDC for
        each of the scopes to the internal table using the index to associate
        itself to the correct company. The xdctype is added to the column name
         to identify the type of XDC has been calculated.

        :param index: index of the company for which the xdc has been calculated
        :type index: int
        :param emissions: Emission object with the emissions of the company
        :type emissions: Emission
        :param xdctype: Type of XDC that was calculated (eg "Baseline")
        :type xdctype: str
        """
        if xdctype == 'target':
            world_curve = Growth.get_world_growth_curve(
                self.base_year, self.target_year, "SSP2", "rcp26")
        else:

            world_curve = Growth.get_world_growth_curve(
                self.base_year, self.target_year, ssp_scenario, "Baseline")

        scope_list = emissions.get_scaled_emission(
            EconomicAdjustmentsHelper.get_adjusted_world_gva(self.base_year),
            world_curve)
        xdc_dict = XDC.emissions_to_fair_xdc(emissions=scope_list,
                                             base_year=self.base_year,
                                             target_year=self.target_year)
        for key, xdc in xdc_dict.items():
            key_name = xdctype + "_" + key
            self.df.loc[index, key_name] = round(xdc, self.precision)
        self.df.loc[index, xdctype + "_id"] = emissions.obj_uuid

    def add_portfolio_df(self):
        """Generate the portfolio dataframe"""
        self.pf = pd.DataFrame()
        if self.eei_complete and self.data_complete:
            index = 0
            for scenario, emissions in self.portfolioxdcs.items():
                self.pf.loc[index, 'Type'] = scenario.capitalize() + " XDC"
                if scenario == 'baseline' or scenario == 'sector':
                    ssp_forcing = 'baseline'
                else:
                    ssp_forcing = 'rcp26'
                world_curve = Growth.get_world_growth_curve(
                    self.base_year,
                    self.target_year,
                    ssp_scenario=self.ssp_scenario,
                    ssp_forcing=ssp_forcing)

                scope_list = emissions.get_scaled_emission(
                    EconomicAdjustmentsHelper.get_adjusted_world_gva(
                        self.base_year), world_curve)
                xdc_dict = XDC.emissions_to_fair_xdc(
                    emissions=scope_list,
                    base_year=self.base_year,
                    target_year=self.target_year,
                )

                for key, xdc in xdc_dict.items():
                    self.pf.loc[index, key] = round(xdc, self.precision)
                self.pf.loc[index, "Calculation UUID"] = emissions.obj_uuid
                index = index + 1
            self.pf.rename(columns={
                'scope1': "Scope 1 XDC",
                "scope2": "Scope 2 XDC",
                'scope3': "Scope 3 XDC",
                "total_scope": "Total XDC"
            },
                           inplace=True)
        else:
            self.pf.loc[0, "Missing Data"] = "Can not calculate Portfolio XDCs"

    def write_output(self, output_file_path: str):
        """Creates an excel output file containing one sheet for the
        asset xdc calculations and one for the portfolio xdc.

        :param output_file_path: Path of the output file
        :type output_file_path: str
        """
        # Create output file location if not existing
        if os.path.dirname(output_file_path) == "":
            parent_folder = os.getcwd()
        else:
            parent_folder = os.path.dirname(output_file_path)

        if not os.path.exists(parent_folder):
            os.makedirs(parent_folder)
        self.df.rename(columns={
            'isin': 'ISIN',
            'weight': 'Weight',
            'gva': '$ GVA',
            'scope1': 'Scope 1',
            'scope2': 'Scope 2',
            'scope3': 'Scope 3',
            'nace': 'NACE',
            'country': 'Country',
            'region': 'Region'
        },
                       inplace=True)
        # drop columns only related to agri/non_agri
        # self.df.drop("baseline_scope3_agri",
        #              "baseline_scope3_non_agri",
        #              "sector_scope3_agri",
        #              "sector_scope3_non_agri",
        #              "Target_B2DS_scope3_agri",
        #              "Target_B2DS_scope3_non_agri",
        #              "Target_2DS_scope3_agri",
        #              "Target_2DS_scope3_non_agri",
        #              inplace=True)
        writer = pd.ExcelWriter(output_file_path, engine='xlsxwriter')
        company_data_columns = [
            'ISIN', 'Weight', '$ GVA', 'Scope 1', 'Scope 2', 'Scope 3', 'NACE',
            'nace used', 'Country', 'Region', 'base_year', 'sector_eei_scope1',
            'IEA sector', 'sector_eei_scope2', 'sector_eei_scope3'
        ]
        company_data = self.df[self.df.columns.intersection(
            company_data_columns)]
        company_data.to_excel(writer, sheet_name="Company Data", index=False)
        if self.portfolio:
            self.add_portfolio_df()
            self.pf.to_excel(writer, sheet_name="Portfolio XDCs", index=False)
        if self.baseline:
            baseline_columns = [
                'ISIN', 'Weight', 'Country', 'Region', 'baseline_scope1',
                'baseline_scope2', 'baseline_scope3', 'baseline_total_scope',
                'baseline_id'
            ]

            baseline_results = self.df[self.df.columns.intersection(
                baseline_columns)]
            baseline_results.rename(columns={
                'baseline_scope1': 'Scope 1 XDC',
                'baseline_scope2': 'Scope 2 XDC',
                'baseline_scope3': 'Scope 3 XDC',
                'baseline_total_scope': 'Total XDC',
                'baseline_id': "Calculation UUID"
            },
                                    inplace=True)
            baseline_results.to_excel(writer,
                                      sheet_name="Baseline XDCs",
                                      index=False)
        if self.sector:
            sector_columns = [
                'ISIN', 'Weight', 'Country', 'Region', 'NACE', 'nace used',
                'sector_scope1', 'sector_scope2', 'sector_scope3',
                'sector_total_scope', 'sector_id'
            ]

            sector_results = self.df[self.df.columns.intersection(
                sector_columns)]
            sector_results.rename(columns={
                'sector_scope1': 'Scope 1 XDC',
                'sector_scope2': 'Scope 2 XDC',
                'sector_scope3': 'Scope 3 XDC',
                'sector_total_scope': 'Total XDC',
                'sector_id': "Calculation UUID"
            },
                                  inplace=True)
            sector_results.to_excel(writer,
                                    sheet_name="Sector XDCs",
                                    index=False)
        if self.target:
            target_columns = [
                'ISIN', 'Weight', 'IEA sector', 'Country', 'Region',
                'Target_B2DS_scope1', 'Target_B2DS_scope2',
                'Target_B2DS_scope3', 'Target_B2DS_total_scope',
                'Target_B2DS_id', 'Target_2DS_scope1', 'Target_2DS_scope2',
                'Target_2DS_scope3', 'Target_2DS_total_scope', 'Target_2DS_id'
            ]
            target_results = self.df[self.df.columns.intersection(
                target_columns)]
            target_results.rename(columns={
                'Target_B2DS_scope1': 'Scope 1 B2DS Target XDC',
                'Target_B2DS_scope2': 'Scope 2 B2DS Target XDC',
                'Target_B2DS_scope3': 'Scope 3 B2DS Target XDC',
                'Target_B2DS_total_scope': 'Total B2DS Target XDC',
                'Target_B2DS_id': "B2DS Calculation  UUID",
                'Target_2DS_scope1': 'Scope 1 2DS Target XDC',
                'Target_2DS_scope2': 'Scope 2 2DS Target XDC',
                'Target_2DS_scope3': 'Scope 3 2DS Target XDC',
                'Target_2DS_total_scope': 'Total 2DS Target XDC',
                'Target_2DS_id': "2DS Calculation  UUID"
            },
                                  inplace=True)
            target_results.to_excel(writer,
                                    sheet_name="Target XDCs",
                                    index=False)
        writer.close()

        logging.info("Output file: %s" % output_file_path)
        #print("- See file ", output_file_path)


def main(argv):

    parser = argparse.ArgumentParser()

    # I/O
    parser.add_argument("-i",
                        "--input_file",
                        help="Path to the input file",
                        required=True)
    parser.add_argument("-o",
                        "--output_file",
                        help="Path to the output file",
                        required=True)
    parser.add_argument(
        "-n",
        "--sheet_name",
        help="Name of the Excel Sheet (optional)",
        default=None,
        type=int,
    )
    parser.add_argument(
        "-p",
        "--precision",
        help="Sets the number of decimal places in the results [0:4]",
        default=2,
        type=int,
        choices=range(0, 5),
    )

    # Parameters
    parser.add_argument('-y',
                        '--base_year',
                        help='Base year',
                        type=int,
                        default=2017)
    parser.add_argument('-t',
                        '--target_year',
                        help='Sets the target year',
                        default=2050,
                        type=int)
    parser.add_argument('--TwoDS',
                        help='If set, 2DS XDCs are calculated (optional)',
                        action='store_true')
    parser.add_argument('--BeyondTwoDS',
                        help='If set, B2DS XDCs are calculated (optional)',
                        action='store_true')

    # Calculation options
    parser.add_argument(
        "-S",
        "--sector",
        help="If set, sector XDCs are calculated (optional)",
        action="store_true",
    )
    parser.add_argument(
        "-T",
        "--target",
        help="If set, target XDCs are calculated (optional)",
        action="store_true",
    )
    parser.add_argument(
        "-B",
        "--baseline",
        help="If set, baseline XDCs are calculated (optional)",
        action="store_true",
    )
    parser.add_argument(
        "-P",
        "--portfolio",
        help="If set, the portfolio mode is enabled (optional)",
        action="store_true",
    )
    # parser.add_argument(
    #     "-F",
    #     "--food_manufacture",
    #     help="Triggers the food manufacture mode",
    #     action="store_true",
    # )

    # Report Level
    parser.add_argument('-v',
                        '--verbose',
                        help='increase output verbosity',
                        action='count',
                        default=0)
    # parser.add_argument('-f',
    #                     '--filtered',
    #                     help='Sets report level to filtered',
    #                     action='store_true')
    # parser.add_argument('--no_report',
    #                     help='Disables report generation',
    #                     action='store_true')

    try:
        args = parser.parse_args(args=argv)
    except:
        parser.print_help()
        sys.exit(1)

    # File names should be ./Reports/xdc_CalcTool*
    xdc_logging.enable_logging('XDCCalcTool')

    # Set logging level by the number of v in -vvvv flag.
    xdc_logging.log_level_handler(logging.ERROR - 10 * args.verbose, "console")
    # Info messages to file (default)
    xdc_logging.log_level_handler(20, 'file')

    calc_tool = XDCCalcTool(sheet_name=args.sheet_name,
                            precision=args.precision,
                            base_year=args.base_year,
                            target_year=args.target_year,
                            TwoDS=args.TwoDS,
                            BeyondTwoDS=args.BeyondTwoDS,
                            sector=args.sector,
                            target=args.target,
                            baseline=args.baseline,
                            portfolio=args.portfolio,
                            verbose=args.verbose)  #,
    # filtered=args.filtered,
    # no_report=args.no_report)

    calc_tool.load_input_data(input_file_path=args.input_file)

    calc_tool.run_calculation()

    calc_tool.write_output(output_file_path=args.output_file)


if __name__ == "__main__":
    main(sys.argv[1:])
