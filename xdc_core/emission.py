#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import uuid
from copy import deepcopy
import numpy as np
from xdc_core.country_code import CountryCode
from xdc_core.growth import Growth
import sys
from pprint import pprint

import xdc_core.xdc_helper.economic_adjustments_helper as EconomicAdjustmentsHelper
import xdc_core.xdc_helper.data_api_helper as DataApiHelper
from xdc_core.scope import Scope
from xdc_core.region import Region
from xdc_core.scenario_code import ScenarioCode
from xdc_core.scope import Scope
from xdc_core.xdc_constants import AGRICULTURE_SECTOR_SHARES
from xdc_core.xdc_helper.helper import get_iea_sector_for_nace_code, check_and_get_food_mode_nace_code
from xdc_core.xdc_error import XdcError, ErrorCodes
import xdc_core.pathways as Pathways
from xdc_core.xdc import XDC

logging.getLogger(__name__)


class Emission:
    """
    Class that generates the Emission object. The class takes Scope objects
    and manipulates them using growth curves from the Growth class. The
    Emission object can be extended by calculating the total scope of all
    emissions. Further, it allows to get scaled emissions for each scope, EEI
    pathways for each scope and emission pathways for each scope.
    """

    base_year_limits = (2010, 2018)
    target_year_limits = (2030, 2050)

    def __init__(self, base_year: int, target_year: int):
        """Initialize the object.
        generate the list of years in self.years, generate the empty
        dictionary for self.scopes to which scopes then get added

        :param base_year: base year of the emissions
        :type base_year: int
        :param target_year: target year for which the XDC is calculated
        :type target_year: int
        """
        self.scopes = {}
        self._check_base_year(base_year)
        self._check_target_year(target_year)
        self.base_year = base_year
        self.target_year = target_year
        for year in (base_year, target_year):
            if not isinstance(year, int):
                logging.error(
                    "Base year and Target year must be from type integer.")
                raise TypeError("""Base year and Target year must be from type
                                 integer.""")
        self.years = list(range(self.base_year, self.target_year + 1))
        self.obj_uuid = str(uuid.uuid4())
        logging.debug(
            "Generated new Emissions_object with UUID: %s. Range: %i,%i",
            self.obj_uuid, self.base_year, self.target_year)

    def __str__(self):
        """Returns a printable version of the emission object

        :return: String with the emissions paramters
        :rtype: str
        """
        return str(self.__dict__)

    def __repr__(self):
        """Returns a printable version of the emission object

        :return: String with the emissions paramters
        :rtype: str
        """
        return str(self.__dict__)

    def __add__(self, other):
        """Add the 'other' object to the current Emission object.

        :param other: Object you want to add to the current object
        :type other: Emission
        :return: Emission object with added up scopes
        :rtype: Emission
        """
        if self.target_year != other.target_year:
            logging.error("The Emission must have the same target year")
            raise ValueError("The Emission must have the same target year")
        if self.base_year != other.base_year:
            logging.error("The Emission must have the same base year")
            raise ValueError("The Emission must have the same base year")
        if self.scopes.keys() != other.scopes.keys():
            msg = "The scopes of the object do not match: %s, %s" % (
                self.scopes.keys(),
                other.scopes.keys(),
            )
            logging.error(msg)
            raise ValueError(msg)
        emissions = Emission(base_year=self.base_year,
                             target_year=self.target_year)
        for scope in self.scopes.keys():
            emissions.add_scope(self.scopes[scope] + other.scopes[scope],
                                scope)
        return emissions

    def __mul__(self, factor: float):
        emissions = deepcopy(self)
        emissions.scale(factor)

        return emissions

    def __rmul__(self, factor: float):
        return self.__mul__(factor)

    def __eq__(self, other):
        """Compare 'other' Emission object and check if they are the same

        :param other: The emission object to be compared with the object
        :type other: Emission
        :return: Equivalency between the current object and the other object
        :rtype:bool
        """
        return self.base_year == other.base_year and\
            self.target_year == other.target_year and\
            self.scopes == other.scopes

    def scale(self, scale: float):
        """Wrapper that will scale all scopes in an emission with the same
        factor
        :param scale: Scaling factor to sale the scopes with
        :type scale: float
        """

        for scope in self.scopes.keys():
            self.scopes[scope].scale(scale)

    def _check_base_year(self, base_year: int):
        """Function for testing that the base_year is is the expected range.
        :param base_year: Base year of the object to be tested
        :type base_year: int
        :raises NotImplementedError: Raise error if base year is not valid
        """
        if not self.base_year_limits[0] <= base_year <= self.base_year_limits[
                1]:
            msg = ("Base year %d indicated exceed limits \
                %s" % (base_year, self.base_year_limits))
            logging.error(msg)
            raise NotImplementedError(msg)

    def _check_target_year(self, target_year: int):
        """Function for testing that the target_year is is the expected range.
        :param target_year: Base year of the object to be tested
        :type target_year: int
        :raises NotImplementedError:  Raise error if target year is not valid
        """
        if not self.target_year_limits[
                0] <= target_year <= self.target_year_limits[1]:
            msg = ("Target year %d indicated is not \
                supported. Must be in range: %s" %
                   (target_year, self.target_year_limits))
            logging.error(msg)
            raise NotImplementedError(msg)

    def add_scope(self, scope, scope_name: str):
        """Append a scope to the dictionary of scopes of the emissions object.

        We also add the scope's name to the scope object itself.

        :param scope: The new scope to be added to the emissions
        :type scope: Scope
        :raises ValueError: Raise error if length of scope doesn't match the 
            period defined in Emission object
        """
        logging.debug("added scope %s", scope_name)
        if len(scope.yearly_eei) != (self.target_year - self.base_year + 1):
            msg = "Lenth of Scope objects yearly_eei must be equal\
                to Emission objects initialized lenght.\n"

            logging.error(msg)
            raise ValueError(msg)
        scope.name = scope_name
        self.scopes.update({scope_name: scope})
        logging.debug("Updated emissions object scope dict: %s", self.scopes)

    def remove_scope(self, scope_name: str):
        """Removes the scope from the dict of scopes of the emission object.

        :param scope_name: Name of the scope to be removed
        :type scope_name: str
        """
        if scope_name not in self.scopes.keys():
            logging.warning(
                "Could not remove %s from scopes as it is not present",
                scope_name)
        self.scopes.pop(scope_name)
        logging.debug("Updated emissions object scope dict: %s", self.scopes)

    def _get_weighted_sum_of_scopes(self,
                                    tag: str = "",
                                    unweighted: bool = False):
        """Get a weighted sum of all/ tagged scopes.

        If no tag is specified, then all scopes are summed. If a tag
        is specified then both the scope's tag list and the scope's name
        are checked for a match. This ensures backwards compatibility with
        scope objects not using tags, by using it's name as a tag.

        :param tag: tag identifier to sum over, defaults to ""
        :type tag: str, optional
        :return: A combination of name (total/ tag) and scope object
        :rtype: dict
        """

        summed_eei = np.zeros(shape=(self.target_year - self.base_year + 1))

        weights = []
        for scope in self.scopes.values():
            if unweighted:
                scope_weight = 1
            else:
                assert (scope.scope_weight is not None)
                scope_weight = scope.scope_weight
            tags = scope.tags + [scope.name]
            if tag == "" or tag in tags:
                summed_eei += scope.yearly_eei * scope_weight
                weights.append(scope_weight)

        if tag == "":
            tag = "total_scope"
        # Scope weights have been applied, if all weights are the same we
        # reset the weight to its original otherwise setting scope weight to 1
        if all(x == weights[0] for x in weights):
            summed_eei /= weights[0]
            weight = weights[0]
        else:
            weight = 1
        summed_scope = {tag: Scope(yearly_eei=summed_eei, scope_weight=weight)}

        logging.debug("Calculating total scope: %s", summed_scope)
        return summed_scope

    def _consolidate_tagged_scopes(self, tag: str):
        """Cosolidate tagged scopes into a single scope and store in obj

        Identify all scopes with a specific tag and sum them up. The weight
        of the scopes to be summed needs to be identical and is kept as the
        new summed scope's weight.

        :param tag: An existing scope tag
        :type tag: str
        :return: Summed scope object with the same weight as the tagged scopes
        :rtype: Scope
        """

        summed_scopes = Scope(start_eei=0,
                              growth_curve=Growth.get_null_curve(
                                  self.base_year, self.target_year),
                              scope_name=tag)
        tags_list = []
        tags = []
        for scope_name, scope in list(self.scopes.items()):
            if tag in scope.tags:
                # we can only sum scopes with the same weight
                # summation will break if the scope weights change in the loop
                summed_scopes.scope_weight = scope.scope_weight
                summed_scopes += scope
                tags_list.append(scope.tags)
                self.remove_scope(scope_name)
        #Check if any tags are present in all scopes and add it to the sum of
        #scopes
        #For each tag in the first list of tags
        for tagged in tags_list[0]:
            # Check that the tag existed in all scope-tag-lists
            tag_exists = all([tagged in tag_list for tag_list in tags_list])
            if tag_exists:
                tags.append(tagged)
        if len(tags) != 0:
            tags.append(tag)
            summed_scopes.tags = tags
        self.add_scope(summed_scopes, tag)

    def get_scaled_emission(self,
                            world_amount: float,
                            world_growth_curve: np.array,
                            total_scope: bool = True):
        """Scale each scope/ summed scope tag to world emissions.

        This method scales the object's eei up to the world level,
        by also taking the world growth into account. It returns a
        list of scaled emissions per year for every scope.

        Scopes are identified based on their tags, so for every tag, a
        scaled sum of tagged scopes is derived.

        :param world_amount: World amount in base year
        :type world_amount: float
        :param world_growth_curve: Growth curve of the world amount
        :type world_growth_curve: np.array
        :return: Scaled world emissions per scope
        :rtype: Dict[str, list[float]]
        """

        world_amount_grown = world_amount * world_growth_curve
        scaled_emissions = {}

        tags = []
        for s in self.scopes.values():
            # Add scope name to list of tags
            tags += [s.name] + s.tags
        tags = list(set(tags))

        if total_scope:
            scopes = self._get_weighted_sum_of_scopes()
            if len(tags) == 1:
                return {
                    'total_scope_emission_pathway':
                    np.array(scopes['total_scope'].yearly_eei)
                }
        else:
            scopes = {}

        for t in tags:
            s = self._get_weighted_sum_of_scopes(t)
            scopes = dict(scopes, **s)

        for scope_tag, scope in scopes.items():
            yearly_scaled_emissions = scope.yearly_eei * world_amount_grown * scope.scope_weight
            scaled_emissions.update({scope_tag: yearly_scaled_emissions})

        return scaled_emissions

    def get_eei_pathways(self, total_scope: bool = True):
        """Returns the unweighted emission pathway for each of the scopes in Scope

        :return: Dictionary of scopes with their eei pathways as list of
                 tuples with year and EEI respectively.
        :rtype: Dict[str,list[int, float]]
        """

        scope_eei_dict = {}
        tags = []
        for s in self.scopes.values():
            # Add scope name to list of tags
            tags += [s.name] + s.tags
        tags = list(set(tags))
        if total_scope:
            scopes = self._get_weighted_sum_of_scopes()
            if len(tags) == 1:
                return {
                    'total_scope_eei_pathway':
                    list(scopes['total_scope'].yearly_eei)
                }
        else:
            scopes = {}
        for t in tags:
            s = self._get_weighted_sum_of_scopes(t)
            scopes = dict(scopes, **s)
        for scope_tag, scope in scopes.items():
            scope_eei_dict.update(
                {str(scope_tag) + "_eei_pathway": list(scope.yearly_eei)})
        return scope_eei_dict

    def get_entity_emission_pathway(self,
                                    company_amount: float,
                                    company_growth_curve: np.array,
                                    total_scope: bool = True) -> dict:
        """Returns the unweighted eei pathways for each of the scope in emissions

        :param company_amount: denominator for the EEI of the company (e.g. GVA)
        :type company_amount: float
        :param company_growth_curve: growth of the EEI denominator with time
        :type company_growth_curve: np.array
        :return: Dictionary of scopes with their eei pathways as list of
                 tuples with year and emissions respectively.
        :rtype: Dict[str,list[int, float]]
        """
        company_amount_grown = company_amount * company_growth_curve

        scope_emission_dict = {}
        tags = []
        for s in self.scopes.values():
            # Add scope name to list of tags
            tags += [s.name] + s.tags
        tags = list(set(tags))
        if total_scope:
            scopes = self._get_weighted_sum_of_scopes(unweighted=True)
        else:
            scopes = {}
        for t in tags:
            s = self._get_weighted_sum_of_scopes(t)
            scopes = dict(scopes, **s)

        for scope_tag, scope in scopes.items():
            scope_emission_dict.update({
                str(scope_tag) + "_emission_pathway":
                list(scope.yearly_eei * company_amount_grown)
            })
        return scope_emission_dict

    @staticmethod
    def for_company_baseline(base_year: int,
                             target_year: int,
                             country_code: CountryCode,
                             company_gva: float,
                             company_scopes: list,
                             growth: Growth = None,
                             ssp_scenario: str = "SSP2",
                             location_shares: list = [1 / 3, 1 / 3, 1 / 3],
                             scope_weights: list = [1, 0.5, 0.5]):
        """Create an emission object for company baseline growth.

        Based on company base year values (scope / gva) and baseline
        specific growth assumptions, create a three Growth objects, nine
        Scope objects, compile them into an Emission object which is
        then returned.

        A custom growth object can also be supplied, which would then
        override the standard baseline growth object.

        :param base_year: base year
        :type base_year: int
        :param target_year: target yeat
        :type target_year: int
        :param company_gva: company base year gva, defaults to None
        :type company_gva: float
        :param company_scopes: company base year scopes, defaults to None
        :type company_scopes: list
        :param country_code: country code, e.g. DEU or GBR
        :type country_code: country,
        :param growth: custom growth object, defaults to None
        :type growth: Growth, optional
        :param scope_weights: company scope weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :return: baseline emission object
        :rtype: Emission
        """
        gva_shares_adjusted = EconomicAdjustmentsHelper.\
            get_adjusted_baseline_gva(company_gva, base_year, country_code,
                                      location_shares)

        scope1_shares = [
            company_scopes[0] * factor for factor in location_shares
        ]
        scope2_shares = [
            company_scopes[1] * factor for factor in location_shares
        ]
        scope3_shares = [
            company_scopes[2] * factor for factor in location_shares
        ]

        company_emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )
        # renormalized_gva = deflate_gva(company_gva)
        if growth is None:
            growth_country, growth_region, growth_global = Growth.\
                from_baseline_assumptions(base_year=base_year,
                                          target_year=target_year,
                                          country=country_code,
                                          ssp_scenario=ssp_scenario,
                                          gas="kyotoGases")

        # PLEASE PUT ME IN SOME LOOPS
        # GVA
        gva_grown_country = np.array(
            [gva_shares_adjusted[0] * g for g in growth_country.entity_growth])
        gva_grown_region = np.array(
            [gva_shares_adjusted[1] * g for g in growth_region.entity_growth])
        gva_grown_global = np.array(
            [gva_shares_adjusted[2] * g for g in growth_global.entity_growth])

        gva_grown_sum = gva_grown_country + gva_grown_region + gva_grown_global

        # Scopes
        scope1_grown_country = np.array(
            [scope1_shares[0] * g for g in growth_country.emissions_growth])
        scope1_grown_region = np.array(
            [scope1_shares[1] * g for g in growth_region.emissions_growth])
        scope1_grown_global = np.array(
            [scope1_shares[2] * g for g in growth_global.emissions_growth])

        scope1_grown_sum = (scope1_grown_country + scope1_grown_region +
                            scope1_grown_global)

        scope2_grown_country = np.array(
            [scope2_shares[0] * g for g in growth_country.emissions_growth])
        scope2_grown_region = np.array(
            [scope2_shares[1] * g for g in growth_region.emissions_growth])
        scope2_grown_global = np.array(
            [scope2_shares[2] * g for g in growth_global.emissions_growth])

        scope2_grown_sum = (scope2_grown_country + scope2_grown_region +
                            scope2_grown_global)

        scope3_grown_country = np.array(
            [scope3_shares[0] * g for g in growth_country.emissions_growth])
        scope3_grown_region = np.array(
            [scope3_shares[1] * g for g in growth_region.emissions_growth])
        scope3_grown_global = np.array(
            [scope3_shares[2] * g for g in growth_global.emissions_growth])

        scope3_grown_sum = (scope3_grown_country + scope3_grown_region +
                            scope3_grown_global)

        yearly_company_eeis = [
            scope1_grown_sum / gva_grown_sum, scope2_grown_sum / gva_grown_sum,
            scope3_grown_sum / gva_grown_sum
        ]

        for i, yearly_eei, in enumerate(yearly_company_eeis):
            scope = Scope(yearly_eei=yearly_eei,
                          scope_weight=scope_weights[i],
                          scope_tags=["scope" + str(i + 1)])

            company_emissions.add_scope(scope=scope,
                                        scope_name="scope" + str(i + 1))

        return company_emissions

    @staticmethod
    def for_sector_baseline(base_year: int,
                            target_year: int,
                            nace_code: str,
                            sector_eeis: list = None,
                            growth: Growth = None,
                            scope_weights: list = [1, 0.5, 0.5],
                            ssp_scenario: str = "ssp2"):
        """Based on sector median base year values (scope/ gva/ eei) and
        baseline specific growth assumptions, create a Growth object,
        three Scope objects, compile them into an Emission object which is
        then returned.

        A custom growth object can also be supplied, which would then
        override the standard baseline growth object.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :param sector_gva: base year sector median gva, defaults to None
        :param growth: custom growth object, defaults to None
        :type growth: Growth, optional
        :param scope_weights: company scope weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :return: sector baseline emission object
        :rtype: Emission
        """
        #print(sector_eeis)
        if not sector_eeis:
            company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
                base_year=base_year, nace_code=nace_code, currency='usd')
            #company_count = sector_eeis.pop('company_count')

        # If sector EEIs are given by, e.g., XDC tool, write list into dict.
        else:
            sector_eeis = {
                "eei_scope1": sector_eeis[0],
                "eei_scope2": sector_eeis[1],
                "eei_scope3": sector_eeis[2]
            }

        sector_emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )
        # renormalized_gva = deflate_gva(sector_gva)
        if growth is None:
            growth = Growth.from_world_assumptions(base_year=base_year,
                                                   target_year=target_year,
                                                   ssp_selector=ssp_scenario,
                                                   forcing='baseline')
        growth_curve = growth.get_eei_curve()

        for i, s in enumerate(sector_eeis):
            scope = Scope(start_eei=sector_eeis[s],
                          growth_curve=growth_curve,
                          scope_weight=scope_weights[i])

            sector_emissions.add_scope(scope=scope,
                                       scope_name="scope%i" % (i + 1))

        return sector_emissions

    @staticmethod
    def for_sector_target(base_year: int,
                          target_year: int,
                          nace_code: str,
                          sector_eeis: list = None,
                          scenario_code: ScenarioCode = None,
                          growth: Growth = None,
                          scope_weights: list = [1, 0.5, 0.5]):
        """Based on sector median base year values (scope/ gva/ eei) and
        target specific growth assumptions, create a Growth object,
        three Scope objects, compile them into an Emission object which is
        then returned.

        A custom growth object can also be supplied, which would then
        override all target xdc/ scope specific growth objects. We probably
        never want to use this feature, so at least for target xdc we should
        remove the possibility to supply a custom growth object.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :param sector_eeis: base year sector eeis
        :type sector_eeis: list
        :param scenario_code: scenario code, e.g. 2DS, defaults to None
        :type scenario_code: ScenarioCode, optional
        :param region_code: region code, e.g. OECD or world, defaults to None
        :type region_code: Region, optional
        :param scope1_iea_sector: company iea sector, overruled by nace2iea mapping, defaults to None
        :type scope1_iea_sector: str, optional
        :param growth: optional growth curve, defaults to None
        :type growth: Growth, optional
        :param scope_weights: all scope's weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :return: sector target emission object
        :rtype: Emission
        """
        ssp_scenario = 'ssp2'
        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        if not sector_eeis:
            company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
                base_year=base_year, nace_code=nace_code, currency='usd')
            #company_count = sector_eeis.pop('company_count')
        # If sector EEIs are given by, e.g., XDC tool, write list into dict.
        else:
            sector_eeis = {
                "eei_scope1": sector_eeis[0],
                "eei_scope2": sector_eeis[1],
                "eei_scope3": sector_eeis[2]
            }

        target_emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )

        iea_sectors = [scope1_iea_sector, "Power", "Total"]

        # renormalized_gva = deflate_gva(target_gva)

        for i, sector_eei in enumerate(sector_eeis):
            if growth is None:
                growth_object = Growth.from_predefined_scenario(
                    iea_sectors[i],
                    base_year,
                    target_year,
                    scenario_code,
                    ssp_selector=ssp_scenario)
                #see XDC-1141 (caching issue):
                #print(iea_sectors[i], base_year, target_year, scenario_code, region_code)
                #print(Growth.from_predefined_scenario.cache_info())
                growth_curve = growth_object.get_eei_curve()
                #print(sum(growth_curve))
            else:
                growth_curve = growth.get_eei_curve()

            scope = Scope(start_eei=sector_eeis[sector_eei],
                          growth_curve=growth_curve,
                          scope_weight=scope_weights[i])

            target_emissions.add_scope(scope=scope,
                                       scope_name="scope%i" % (i + 1))

        return target_emissions

    @staticmethod
    def for_food_mode_company_baseline(
            base_year: int,
            target_year: int,
            nace_code: str,
            company_gva: float,
            company_scopes: list,
            country_code: CountryCode,
            ssp_scenario: str = "SSP2",
            location_shares: list = [1 / 3, 1 / 3, 1 / 3],
            scope_weights: list = [1, 0.5, 0.5],
            food_manu_shares: list = None,
            portfolio: bool = False):
        """Return emissions obj w/ scope3 food manufacture growth assumptions

        A custom emissions object is generated which generates scope1
        and scope2 identically to the for_company_baseline() method. Scope3
        is then broken down into a agricultural and non agricultural
        share and growth. Hence the resulting emissions object will contain
        five scopes instead of three.

        These agricultural shares are only defined for sectors 10 and 11,
        so calling this method with any other nace code will result in
        an exception.

        An additional portfolio flag can be supplied, wich indicates, that
        the emissions object needs to be compatible to regular/ non food
        mode emissions objects in the portfolio. This mode then triggers
        the consolidation of scope3 sub scopes into a single scope3, so that
        it can be summed up with other portfolio emission objects.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :param nace_code: the company's nace code
        :type nace_code: str
        :param company_gva: the company's gva, defaults to None
        :type company_gva: float, optional
        :param company_scopes: all company scopes, defaults to None
        :type company_scopes: list, optional
        :param company_eeis: all company scope eeis, defaults to None
        :type company_eeis: list, optional
        :param country_code: country code, e.g. DEU or GBR
        :type country_code: CountryCode
        :param scope_weights: all scope's weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :return: emissions object with custom scope3 food-man growth assumptions
        :rtype: Emission
        """

        gva_shares_adjusted = EconomicAdjustmentsHelper.\
            get_adjusted_baseline_gva(company_gva, base_year, country_code,
                                      location_shares)

        scope1_shares = [
            company_scopes[0] * factor for factor in location_shares
        ]
        scope2_shares = [
            company_scopes[1] * factor for factor in location_shares
        ]
        scope3_shares = [
            company_scopes[2] * factor for factor in location_shares
        ]

        emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )

        # Growth objects with kyotoGases emission growth
        kyoto_growth_country, kyoto_growth_region, kyoto_growth_global = Growth.\
            from_baseline_assumptions(base_year=base_year,
                                      target_year=target_year,
                                      country=country_code,
                                      ssp_scenario=ssp_scenario,
                                      gas="kyotoGases")

        # GVA
        gva_grown_country = np.array([
            gva_shares_adjusted[0] * g
            for g in kyoto_growth_country.entity_growth
        ])
        gva_grown_region = np.array([
            gva_shares_adjusted[1] * g
            for g in kyoto_growth_region.entity_growth
        ])
        gva_grown_global = np.array([
            gva_shares_adjusted[2] * g
            for g in kyoto_growth_global.entity_growth
        ])

        gva_grown_sum = gva_grown_country + gva_grown_region + gva_grown_global

        # Scope 1 & Scope 2 Kyoto Emissions Growth
        scope1_grown_country = np.array([
            scope1_shares[0] * g for g in kyoto_growth_country.emissions_growth
        ])
        scope1_grown_region = np.array([
            scope1_shares[1] * g for g in kyoto_growth_region.emissions_growth
        ])
        scope1_grown_global = np.array([
            scope1_shares[2] * g for g in kyoto_growth_global.emissions_growth
        ])

        scope1_grown_sum = (scope1_grown_country + scope1_grown_region +
                            scope1_grown_global)

        scope2_grown_country = np.array([
            scope2_shares[0] * g for g in kyoto_growth_country.emissions_growth
        ])
        scope2_grown_region = np.array([
            scope2_shares[1] * g for g in kyoto_growth_region.emissions_growth
        ])
        scope2_grown_global = np.array([
            scope2_shares[2] * g for g in kyoto_growth_global.emissions_growth
        ])

        scope2_grown_sum = (scope2_grown_country + scope2_grown_region +
                            scope2_grown_global)

        yearly_eeis_scope1_and_scope2 = [
            scope1_grown_sum / gva_grown_sum, scope2_grown_sum / gva_grown_sum
        ]
        scope1_scope2_weight = [scope_weights[0], scope_weights[1]]

        for i, yearly_eeis in enumerate(yearly_eeis_scope1_and_scope2):
            scope = Scope(yearly_eei=yearly_eeis,
                          scope_weight=scope1_scope2_weight[i],
                          scope_tags=["scope" + str(i + 1)])

            emissions.add_scope(scope=scope, scope_name="scope" + str(i + 1))

        # Scope 3
        active_gases = []
        if food_manu_shares is None:
            agri_shares = AGRICULTURE_SECTOR_SHARES[nace_code]
        else:
            agri_shares = {
                'AgriN2O': food_manu_shares[0],
                'AgriCH4': food_manu_shares[1],
                'AgriCO2': food_manu_shares[2],
                'nonAgri': 1 - sum(food_manu_shares)
            }
        for gas in ["N2O", "CH4", "CO2"]:
            if agri_shares["Agri" + gas] != 0:

                active_gases.append(gas)

        # Scope 3 Non Agri Kyoto Growth
        scope3_non_agri_grown_country = np.array([
            scope3_shares[0] * agri_shares["nonAgri"] * g
            for g in kyoto_growth_country.emissions_growth
        ])
        scope3_non_agri_grown_region = np.array([
            scope3_shares[1] * agri_shares["nonAgri"] * g
            for g in kyoto_growth_region.emissions_growth
        ])
        scope3_non_agri_grown_global = np.array([
            scope3_shares[2] * agri_shares["nonAgri"] * g
            for g in kyoto_growth_global.emissions_growth
        ])

        scope3_grown_sum = (scope3_non_agri_grown_country +
                            scope3_non_agri_grown_region +
                            scope3_non_agri_grown_global)

        yearly_eeis_scope3_non_agri = scope3_grown_sum / (gva_grown_sum)

        scope = Scope(yearly_eei=yearly_eeis_scope3_non_agri,
                      scope_weight=scope_weights[2],
                      scope_tags=["scope3", "scope3_non_agri"])

        emissions.add_scope(scope=scope, scope_name="scope3")

        # list of scope3 agri
        # get growth curve for active gases
        for gas in active_gases:

            growth_country, growth_region, growth_global = Growth.\
                from_baseline_assumptions(base_year=base_year,
                                          target_year=target_year,
                                          country=country_code,
                                          ssp_scenario=ssp_scenario,
                                          gas=gas)

            growth_curve_country = growth_country.get_eei_curve()
            growth_curve_region = growth_region.get_eei_curve()
            growth_curve_global = growth_global.get_eei_curve()
            growth_curves = {
                "country": growth_curve_country,
                "region": growth_curve_region,
                "global": growth_curve_global,
            }

            scope3_agri_grown_country = np.array([
                scope3_shares[0] * agri_shares["Agri%s" % gas] * g
                for g in kyoto_growth_country.emissions_growth
            ])
            scope3_agri_grown_region = np.array([
                scope3_shares[1] * agri_shares["Agri%s" % gas] * g
                for g in kyoto_growth_region.emissions_growth
            ])
            scope3_agri_grown_global = np.array([
                scope3_shares[2] * agri_shares["Agri%s" % gas] * g
                for g in kyoto_growth_global.emissions_growth
            ])

            scope3_grown_sum = (scope3_agri_grown_country +
                                scope3_agri_grown_region +
                                scope3_agri_grown_global)

            yearly_eeis_scope3_agri = scope3_grown_sum / (gva_grown_sum)

            scope = Scope(yearly_eei=yearly_eeis_scope3_agri,
                          scope_weight=scope_weights[2],
                          scope_tags=["scope3", "scope3_agri"])

            emissions.add_scope(scope=scope, scope_name="scope3_%s" % gas)

        if portfolio:
            emissions._consolidate_tagged_scopes("scope3")
        else:
            # Consolidate scope3_agri emissions
            emissions._consolidate_tagged_scopes("scope3_agri")
            # Consolidate scope3_non_agri emissions
            emissions._consolidate_tagged_scopes("scope3_non_agri")
        return emissions

    @staticmethod
    def for_food_mode_sector_baseline(base_year: int,
                                      target_year: int,
                                      nace_code: str,
                                      sector_eeis: list = None,
                                      ssp_scenario: str = "ssp2",
                                      scope_weights: list = [1, 0.5, 0.5],
                                      portfolio: bool = None):
        """Return emissions obj w/ scope3 food manufacture growth assumptions

        A custom emissions object is generated which generates scope1
        and scope2 identically to the for_sector_baseline() method. Scope3
        is then broken down into a agricultural and non agricultural
        share and growth. Hence the resulting emissions object will contain
        five scopes instead of three.

        These agricultural shares are only defined for sectors 10 and 11,
        so calling this method with any other nace code will result in
        an exception.

        An additional portfolio flag can be supplied, wich indicates, that
        the emissions object needs to be compatible to regular/ non food
        mode emissions objects in the portfolio. This mode then triggers
        the consolidation of scope3 sub scopes into a single scope3, so that
        it can be summed up with other portfolio emission objects.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :param nace_code: company nace code
        :type nace_code: str
        :param ssp_scenario:  SSP scenario to be used for retrieving data
        :type ssp_scenario: str, optional
        :param scope_weights: all scope's weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :return: emissions object with custom scope3 food manufacture growth assumptions
        :rtype: Emission
        """
        if not sector_eeis:
            company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
                base_year=base_year, nace_code=nace_code, currency='usd')
            #company_count = sector_eeis.pop('company_count')
        # If sector EEIs are given by, e.g., XDC tool, write list into dict.
        else:
            sector_eeis = {
                "eei_scope1": sector_eeis[0],
                "eei_scope2": sector_eeis[1],
                "eei_scope3": sector_eeis[2]
            }

        emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )

        scope3_eei = sector_eeis["eei_scope3"]
        scope3_weight = scope_weights[2]

        active_gases = []
        #Make sure the agri_shares are used for 2-digit nace code

        agri_shares = get_sector_agriculture_shares(nace_code)
        for gas in ["N2O", "CH4", "CO2"]:
            if agri_shares["Agri" + gas] != 0:
                active_gases.append(gas)

        # Scope 1, Scope 2 & Scope 3 (non agri) kyoto world growth
        kyoto_world_growth = Growth.from_world_assumptions(
            base_year=base_year,
            target_year=target_year,
            ssp_selector=ssp_scenario,
            forcing='baseline',
            gas="kyotoGases")

        # Scope 1 & 2
        scope1_scope2_eeis = {
            "scope1_eei": sector_eeis["eei_scope1"],
            "scope2_eei": sector_eeis["eei_scope2"]
        }
        for i, eei in enumerate(scope1_scope2_eeis):
            scope = Scope(start_eei=scope1_scope2_eeis[eei],
                          growth_curve=kyoto_world_growth.get_eei_curve(),
                          scope_weight=scope_weights[i])

            emissions.add_scope(scope=scope, scope_name="scope%i" % (i + 1))

        # Scope3 non agri

        scope = Scope(start_eei=scope3_eei * agri_shares["nonAgri"],
                      growth_curve=kyoto_world_growth.get_eei_curve(),
                      scope_weight=scope3_weight,
                      scope_tags=["scope3", "scope3_non_agri"])

        emissions.add_scope(
            scope=scope,
            scope_name="scope3_non_agri",
        )

        # list of scope3 agri

        for i, gas in enumerate(active_gases):
            growth = Growth.from_world_assumptions(base_year=base_year,
                                                   target_year=target_year,
                                                   ssp_selector=ssp_scenario,
                                                   forcing='baseline',
                                                   gas=gas)

            growth_curve = growth.get_eei_curve()

            scope = Scope(start_eei=scope3_eei * agri_shares["Agri" + gas],
                          growth_curve=growth_curve,
                          scope_weight=scope3_weight,
                          scope_tags=["scope3", "scope3_agri"])

            emissions.add_scope(
                scope=scope,
                scope_name="scope3_agri_%s" % gas,
            )

        if portfolio:
            emissions._consolidate_tagged_scopes("scope3")
        else:
            # Consolidate scope3_agri emissions
            emissions._consolidate_tagged_scopes("scope3_agri")
            # Consolidate scope3_non_agri emissions
            emissions._consolidate_tagged_scopes("scope3_non_agri")

        return emissions

    @staticmethod
    def for_food_mode_sector_target(base_year: int,
                                    target_year: int,
                                    nace_code: str,
                                    scenario_code: ScenarioCode,
                                    sector_eeis: list = None,
                                    scope_weights: list = [1, 0.5, 0.5],
                                    portfolio: bool = None):
        """Return emissions obj w/ scope3 food manufacture growth assumptions

        A custom emissions object is generated which generates scope1
        and scope2 identically to the for_sector_target() method. Scope3
        is then broken down into a agricultural and non agricultural
        share and growth. Hence the resulting emissions object will contain
        five scopes instead of three.

        These agricultural shares are only defined for sectors 10 and 11,
        so calling this method with any other nace code will result in
        an exception.

        An additional portfolio flag can be supplied, wich indicates, that
        the emissions object needs to be compatible to regular/ non food
        mode emissions objects in the portfolio. This mode then triggers
        the consolidation of scope3 sub scopes into a single scope3, so that
        it can be summed up with other portfolio emission objects.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :param sector_eeis: all scope's sector median eeis
        :type sector_eeis: list
        :param nace_code: company nace code
        :type nace_code: str
        :param scope1_iea_sector: company iea sector, overruled by nace2iea mapping, defaults to None
        :type scope1_iea_sector: str, optional
        :param scenario_code: scenario code, e.g. 2DS, defaults to None
        :type scenario_code: ScenarioCode, optional
        :param region_code: region code, e.g. OECD or world, defaults to None
        :type region_code: Region, optional
        :param scope_weights: all scope's weights, defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :param portfolio: portfolio mode, consolidate scope3 sub scopes, defaults to False
        :type portfolio: bool, optional
        :return: emissions object with custom scope3 food manufacture growth assumptions
        :rtype: Emission
        """
        ssp_scenario = "ssp2"
        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        if not sector_eeis:
            company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
                base_year=base_year, nace_code=nace_code, currency='usd')
            #company_count = sector_eeis.pop('company_count')
        # If sector EEIs are given by, e.g., XDC tool, write list into dict.
        else:
            sector_eeis = {
                "eei_scope1": sector_eeis[0],
                "eei_scope2": sector_eeis[1],
                "eei_scope3": sector_eeis[2]
            }

        iea_sectors = [scope1_iea_sector, "Power", "Total"]

        emissions = Emission(
            base_year=base_year,
            target_year=target_year,
        )

        scope3_eei = sector_eeis["eei_scope3"]
        scope3_weight = scope_weights[2]

        active_gases = []
        #Make sure the agri_shares are used for 2-digit nace code

        agri_shares = get_sector_agriculture_shares(nace_code)
        for gas in ["N2O", "CH4", "CO2"]:
            if agri_shares["Agri" + gas] != 0:
                active_gases.append(gas)

        # Scope 1 & 2
        scope1_scope2_eeis = {
            "scope1_eei": sector_eeis["eei_scope1"],
            "scope2_eei": sector_eeis["eei_scope2"]
        }
        for i, sector_eei in enumerate(scope1_scope2_eeis):
            growth_object = Growth.from_predefined_scenario(
                selector=iea_sectors[i],
                base_year=base_year,
                target_year=target_year,
                scenario=scenario_code,
                ssp_selector=ssp_scenario)
            growth_curve = growth_object.get_eei_curve()

            scope = Scope(start_eei=scope1_scope2_eeis[sector_eei],
                          growth_curve=growth_curve,
                          scope_weight=scope_weights[i])

            emissions.add_scope(scope=scope, scope_name="scope%i" % (i + 1))

        # Scope 3 - non Agri

        i = 2
        growth_object = Growth.from_predefined_scenario(
            iea_sectors[i],
            base_year,
            target_year,
            scenario_code,
            ssp_selector=ssp_scenario)
        growth_curve = growth_object.get_eei_curve()

        scope = Scope(start_eei=sector_eeis["eei_scope3"] *
                      agri_shares["nonAgri"],
                      growth_curve=growth_curve,
                      scope_weight=scope_weights[i],
                      scope_tags=["scope3", "scope3_non_agri"])

        emissions.add_scope(scope=scope, scope_name="scope3_non_agri")

        # Scope 3 - Agri

        for i, gas in enumerate(active_gases):
            growth_object = Growth.from_predefined_scenario(
                gas,
                base_year,
                target_year,
                ScenarioCode.SSP2T,
                ssp_selector=ssp_scenario)
            growth_curve = growth_object.get_eei_curve()

            scope = Scope(start_eei=scope3_eei * agri_shares["Agri" + gas],
                          growth_curve=growth_curve,
                          scope_weight=scope3_weight,
                          scope_tags=["scope3", "scope3_agri"])

            emissions.add_scope(
                scope=scope,
                scope_name="scope3_agri_%s" % gas,
            )

        if portfolio:
            emissions._consolidate_tagged_scopes("scope3")
        else:
            # Consolidate scope3_agri emissions
            emissions._consolidate_tagged_scopes("scope3_agri")
            # Consolidate scope3_non_agri emissions
            emissions._consolidate_tagged_scopes("scope3_non_agri")

        return emissions

    @staticmethod
    def for_target_xdc_company_pathway(base_year: int,
                                       target_year: int,
                                       company_emissions_per_scope: list,
                                       company_gva: float,
                                       nace_code: str,
                                       scenario_code: ScenarioCode,
                                       company_country: CountryCode,
                                       location_shares: list = None,
                                       ssp_code: str = 'SSP2',
                                       scope_weights: list = [1, 0.5, 0.5]):
        """Wrapper function that calculates the company target pathway based 
        on the XDC in the target year

        :param base_year: base_year of the calculation
        :type base_year: int
        :param target_year: target_year of the calculation
        :type target_year: int
        :param sector_eeis: EEIs for scope1,2,3 of the sector
        :type sector_eeis: list
        :param company_emissions_per_scope: emissions of the company for scope
            1,2,3
        :type company_emissions_per_scope: list
        :param company_gva: GVA of the company
        :type company_gva: float
        :param nace_code:nace code of the company
        :type nace_code: str
        :param scenario_code: Scenario used for the target xdc. 
            Can be B2DS or 2DS.
        :type scenario_code: ScenarioCode
        :param company_region: Region of the company
        :type company_region: Region
        :param scope_weights: The weights of the scopes, 
            defaults to [1, 0.5, 0.5]
        : type scope_weights: list, optional """
        if not location_shares:
            location_shares = [1 / 3, 1 / 3, 1 / 3]
        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
            base_year, nace_code, 'usd')

        iea_sectors = [scope1_iea_sector, "Power", "Total"]
        # raise error if gva is 0
        renormalized_gva = sum(
            EconomicAdjustmentsHelper.get_adjusted_baseline_gva(
                company_gva, base_year, company_country, location_shares))

        pathways_complete = True
        pathway = Pathways.xdc_target_pathway
        degree_converter = XDC.emissions_to_fair_xdc
        company_eeis = [
            x / renormalized_gva for x in company_emissions_per_scope
        ]
        company_eei_total = sum(
            [x * y for x, y in zip(company_eeis, scope_weights)])
        sector_eei_total = sum(
            [x * y for x, y in zip(sector_eeis.values(), scope_weights)])
        total_emission = np.zeros(target_year - base_year + 1)
        emission_obj = Emission(base_year, target_year)
        for i, sector_eei in enumerate(sector_eeis.values()):
            growth_obj = Growth.from_predefined_scenario(iea_sectors[i],
                                                         base_year,
                                                         target_year,
                                                         scenario_code,
                                                         ssp_selector='ssp2')

            total_emission += sector_eei / sector_eei_total * scope_weights[
                i] * growth_obj.emissions_growth

            if pathways_complete:
                company_target = Growth.create_entity_pathway_from_sector_path(
                    growth_obj,
                    company_country=company_country,
                    company_base_year_eei=company_eeis[i],
                    sector_base_year_eei=sector_eei,
                    location_shares=location_shares,
                    ssp_scenario=ssp_code,
                    pathway=pathway,
                    emissions_to_xdc=degree_converter)

                if not isinstance(company_target, Growth):
                    logging.warning(
                        "Could not computer pathway for scope %i." +
                        " No pathways for the scope will be shown", i + 1)
                    pathways_complete = False

                else:
                    scope = Scope(company_eeis[i],
                                  company_target.get_eei_curve(),
                                  scope_weight=scope_weights[i])
                    emission_obj.add_scope(scope, "scope" + str(i + 1))
        # If not all scopes could be computed, don't show any scopes
        if not pathways_complete:
            # for scope in emission_obj.scopes.keys():
            #     emission_obj.remove_scope(scope)
            emission_obj.scopes = {}
        growth_total = Growth.from_scenario(
            base_year=base_year,
            target_year=target_year,
            entity_growth=growth_obj.get_entity_amount_curve(),
            emissions_growth=total_emission)
        company_target_pathway = Growth.create_entity_pathway_from_sector_path(
            growth_total,
            company_country=company_country,
            company_base_year_eei=company_eei_total,
            sector_base_year_eei=sector_eei_total,
            location_shares=location_shares,
            ssp_scenario=ssp_code,
            pathway=pathway,
            emissions_to_xdc=degree_converter)
        # If total can't be computed, don't show any pathways.
        if not isinstance(company_target_pathway, Growth):
            logging.warning("Could not compute total company pathway")
            return None

        scope_total = Scope(company_eei_total,
                            company_target_pathway.get_eei_curve())
        if pathways_complete:
            scope3_alt = scope_total - emission_obj.scopes[
                'scope1'] - emission_obj.scopes['scope2']
            scope3_alt.scale(2)
            scope3_alt.scope_weight = 0.5
            emission_obj.remove_scope("scope3")
            emission_obj.add_scope(scope3_alt, "scope3")
        else:
            emission_obj.add_scope(scope_total, "Total")

        return emission_obj

    @staticmethod
    def for_target_xdc_food_manu_company_pathway(
            base_year: int,
            target_year: int,
            company_emissions_per_scope: list,
            company_gva: float,
            nace_code: str,
            scenario_code: ScenarioCode,
            company_country: CountryCode,
            scope_weights: list = [1, 0.5, 0.5],
            location_shares: list = None,
            ssp_code: str = 'SSP2',
            scope3_agri_share: list = None):
        """Wrapper function that calculates the company target pathway based 
        on the XDC in the target year for food manufacturing companies (nace 
        sector 10/11) 

        :param base_year: base_year of the calculation
        :type base_year: int
        :param target_year: target_year of the calculation
        :type target_year: int
        :param sector_eeis: EEIs for scope1,2,3 of the sector
        :type sector_eeis: list
        :param company_emissions_per_scope: emissions of the company for scope
            1,2,3
        :type company_emissions_per_scope: list
        :param company_gva: GVA of the company
        :type company_gva: float
        :param nace_code:nace code of the company
        :type nace_code: str
        :param scenario_code: Scenario used for the target xdc. 
            Can be B2DS or 2DS.
        :type scenario_code: ScenarioCode
        :param company_region: Region of the company
        :type company_region: Region
        :param scope_weights: The weights of the scopes, 
            defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :param scope3_agri_share: The shares of agricultural gases must be 
            given in the following order: [N2O, CH4, CO2], defaults to None
        :type scope3_agri_share: list, optional
        :raises ValueError: Eeis and emissions must be provided for 3 scopes
        :raises NotImplementedError: raises an error if the company is called 
            with a company that's not a food manufacturer.
        :raises ValueError: raises an error if the agricultural shares provided
            are not in the right format.
        :return: Company pathway for scope1,scope2,scope3, scope3-agri,
             scope3-nonagri and total scope
        :rtype: dict
        """
        if not location_shares:
            location_shares = [1 / 3, 1 / 3, 1 / 3]
        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
            base_year, nace_code, 'usd')

        if len(sector_eeis) != 3 or len(company_emissions_per_scope) != 3:
            raise ValueError("Values for scope1,2 and 3 are expected")
        #check if 4 digit, 3 digit or 2 digit nace code is in shares:
        food_mode, nace_code = check_and_get_food_mode_nace_code(nace_code)

        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        selector = [scope1_iea_sector, "Power", "Total"]
        # raise error if gva is 0
        renormalized_gva = sum(
            EconomicAdjustmentsHelper.get_adjusted_baseline_gva(
                company_gva, base_year, company_country, location_shares))
        pathways_complete = True
        pathway = Pathways.xdc_target_pathway
        degree_converter = XDC.emissions_to_fair_xdc
        company_eeis = [
            x / renormalized_gva for x in company_emissions_per_scope
        ]
        company_eei_total = sum(
            [x * y for x, y in zip(company_eeis, scope_weights)])
        sector_eei_total = sum(
            [x * y for x, y in zip(sector_eeis.values(), scope_weights)])
        total_emission = np.zeros(target_year - base_year + 1)
        emission_obj = Emission(base_year, target_year)
        #Make sure the agri_shares are used for 2-digit nace code

        agri_sector_shares = get_sector_agriculture_shares(nace_code)
        if scope3_agri_share is not None:
            if len(scope3_agri_share) != 3:
                raise ValueError("Always provide all three GHG gases in:" +
                                 "N2O, CH4, CO2 in the given order")
            agri_company_shares = {
                "nonAgri": 1 - sum(scope3_agri_share),
                "AgriN2O": scope3_agri_share[0],
                "AgriCH4": scope3_agri_share[1],
                "AgriCO2": scope3_agri_share[2]
            }
        else:
            agri_company_shares = AGRICULTURE_SECTOR_SHARES[nace_code]
        #set sector_eei[2] to be the non-agri share:
        sector_scope3_eei = sector_eeis['eei_scope3']
        company_scope3_eei = company_eeis[2]
        #sector_eeis[
        #    'eei_scope3'] = sector_scope3_eei * agri_sector_shares['nonAgri']
        company_eeis[2] = company_scope3_eei * agri_company_shares['nonAgri']
        # add the agricultural gases to the total emissions
        for gas in ["N2O", "CH4", "CO2"]:
            if agri_sector_shares["Agri" + gas] != 0 and agri_company_shares[
                    "Agri" + gas] != 0:
                growth_obj = Growth.from_predefined_scenario(
                    selector=gas,
                    base_year=base_year,
                    target_year=target_year,
                    scenario=ScenarioCode.value_of("ssp2t"),
                    ssp_selector='ssp2')

                total_emission += sector_scope3_eei / sector_eei_total * agri_sector_shares[
                    'Agri' +
                    gas] * scope_weights[2] * growth_obj.emissions_growth

        for i, sector_eei in enumerate(sector_eeis.values()):
            # Scope3 calculate only the non-agri part
            if i == 2:
                sector_eei = sector_eei * agri_sector_shares['nonAgri']
            growth_obj = Growth.from_predefined_scenario(selector[i],
                                                         base_year,
                                                         target_year,
                                                         scenario_code,
                                                         ssp_selector='ssp2')

            total_emission += sector_eei / sector_eei_total * scope_weights[
                i] * growth_obj.emissions_growth
            if pathways_complete:
                company_target = Growth.create_entity_pathway_from_sector_path(
                    growth_obj,
                    company_country=company_country,
                    company_base_year_eei=company_eeis[i],
                    sector_base_year_eei=sector_eei,
                    location_shares=location_shares,
                    ssp_scenario=ssp_code,
                    pathway=pathway,
                    emissions_to_xdc=degree_converter)
                if not isinstance(company_target, Growth):
                    logging.warning(
                        "Could not computer pathway for scope %i." +
                        " No pathways for the scope will be shown", i + 1)
                    pathways_complete = False
                else:
                    scope = Scope(company_eeis[i],
                                  company_target.get_eei_curve(),
                                  scope_weight=scope_weights[i])
                    emission_obj.add_scope(scope, "scope" + str(i + 1))
        # If not all scopes could be computed, don't show any scopes
        if not pathways_complete:
            emission_obj.scopes = {}
        growth_total = Growth.from_scenario(
            base_year=base_year,
            target_year=target_year,
            entity_growth=growth_obj.get_entity_amount_curve(),
            emissions_growth=total_emission)
        company_target_pathway = Growth.create_entity_pathway_from_sector_path(
            growth_total,
            company_country,
            company_base_year_eei=company_eei_total,
            sector_base_year_eei=sector_eei_total,
            location_shares=location_shares,
            ssp_scenario=ssp_code,
            pathway=pathway,
            emissions_to_xdc=degree_converter)
        # If total can't be computed, don't show any pathways.
        if not isinstance(company_target_pathway, Growth):
            msg = "Could not compute total company pathway"
            logging.warning(msg)
            raise XdcError(ErrorCodes.ERR_TARGET_UNREACH, 'total scope', msg)

        scope_total = Scope(company_eei_total,
                            company_target_pathway.get_eei_curve())
        if pathways_complete:
            scope3_nonagri = deepcopy(emission_obj.scopes['scope3'])
            scope3_nonagri.tags = ['scope3', 'scope3_non_agri']
            scope3_agri = scope_total - emission_obj.scopes[
                'scope1'] - emission_obj.scopes['scope2'] - scope3_nonagri
            scope3_agri.scale(2)
            scope3_agri.scope_weight = 0.5
            scope3_agri.tags = ["scope3", "scope3_agri"]
            emission_obj.remove_scope("scope3")
            emission_obj.add_scope(scope3_agri, "scope3_agri")
            emission_obj.add_scope(scope3_nonagri, "scope3_non_agri")
        else:
            emission_obj.add_scope(scope_total, "total")

        return emission_obj

    @staticmethod
    def for_target_eei_company_pathway(
            base_year: int,
            target_year: int,
            company_emissions_per_scope: list,
            company_gva: float,
            nace_code: str,
            scenario_code: ScenarioCode,
            company_country: CountryCode,
            location_shares: list = [1 / 3, 1 / 3, 1 / 3],
            ssp_scenario: str = 'ssp2',
            scope_weights: list = [1, 0.5, 0.5]):
        """Wrapper function that calculates the company target pathway based 
        on the eei in the target year without using the food mode

        :param base_year: base_year of the calculation
        :type base_year: int
        :param target_year: target_year of the calculation
        :type target_year: int
        :param sector_eeis: EEIs for scope1,2,3 of the sector
        :type sector_eeis: list
        :param company_emissions_per_scope: emissions of the company for scope
        1,2,3
        :type company_emissions_per_scope: list
        :param company_gva: GVA of the company
        :type company_gva: float
        :param nace_code:nace code of the company
        :type nace_code: str
        :param scenario_code: Scenario used for the target xdc. 
            Can be B2DS or 2DS,
        :type scenario_code: ScenarioCode
        :param company_region: Region of the company
        :type company_region: Region
        :param scope_weights: The weights of the scopes, 
        defaults to [1, 0.5, 0.5]
        """

        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        iea_sectors = [scope1_iea_sector, "Power", "Total"]
        company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
            base_year, nace_code, 'usd')

        company_adjusted_gva = sum(
            EconomicAdjustmentsHelper.get_adjusted_baseline_gva(
                company_gva, base_year, company_country, location_shares))
        scopes = {}
        company_emissions = Emission(base_year, target_year)
        for i in range(len(iea_sectors)):
            company_eei = company_emissions_per_scope[i] / company_adjusted_gva
            growth_obj = Growth.create_entity_pathway_from_predefined_scenario(
                selector=iea_sectors[i],
                base_year=base_year,
                target_year=target_year,
                scenario=scenario_code,
                company_country=company_country,
                company_base_year_eei=company_eei,
                sector_base_year_eei=sector_eeis['eei_scope' + str(i + 1)],
                location_shares=location_shares,
                ssp_scenario=ssp_scenario,
                pathway=Pathways.eei_target_pathway,
                emissions_to_xdc=XDC.emissions_to_fair_xdc)

            scopes[i] = Scope(company_eei,
                              growth_obj.get_eei_curve(),
                              scope_weight=scope_weights[i])
            company_emissions.add_scope(scopes[i], "scope" + str(i + 1))

        return company_emissions

    @staticmethod
    def for_target_eei_food_manu_company_pathway(
            base_year: int,
            target_year: int,
            company_emissions_per_scope: list,
            company_gva: float,
            nace_code: str,
            scenario_code: ScenarioCode,
            company_country: CountryCode,
            location_shares: list = [1 / 3, 1 / 3, 1 / 3],
            ssp_scenario: str = 'ssp2',
            scope_weights: list = [1, 0.5, 0.5],
            scope3_agri_share: list = None):
        """Wrapper function that calculates the company target pathway based 
        on the eei in the target year for food manufacturing companies (nace 
        sector 10/11) 

        :param base_year: base_year of the calculation
        :type base_year: int
        :param target_year: target_year of the calculation
        :type target_year: int
        :param sector_eeis: EEIs for scope1,2,3 of the sector
        :type sector_eeis: list
        :param company_emissions_per_scope: emissions of the company for scope
        1,2,3
        :type company_emissions_per_scope: list
        :param company_gva: GVA of the company
        :type company_gva: float
        :param nace_code:nace code of the company
        :type nace_code: str
        :param scenario_code: Scenario used for the target xdc. 
            Can be B2DS or 2DS,
        :type scenario_code: ScenarioCode
        :param company_region: Region of the company
        :type company_region: Region
        :param scope_weights: The weights of the scopes, 
        defaults to [1, 0.5, 0.5]
        :type scope_weights: list, optional
        :param scope3_agri_share: The shares of agricultural gases must be 
            given in the following order: [N2O, CH4, CO2], defaults to None
        :type scope3_agri_share: list, optional
        :raises NotImplementedError: raises an error if the company is called 
            with a company that's not a food manufacturer.
        :raises ValueError: Eeis and emissions must be provided for 3 scopes
        :raises ValueError: raises an error if the agricultural shares provided
            are not in the right format.
        :return: Company pathway for scope1,scope2,scope3, scope3-agri, 
            scope3-nonagri and total scope
        :rtype: dict
        """
        pathway = Pathways.eei_target_pathway
        xdc_method = XDC.emissions_to_fair_xdc
        if len(company_emissions_per_scope) != 3:
            raise ValueError("Values for scope1,2 and 3 are expected")
        #GET PPP ADJUSTED SECTOR EEIS
        company_count, sector_eeis = EconomicAdjustmentsHelper.get_adjusted_sector_eei(
            base_year, nace_code, 'usd')

        # ADJUST COMPANY GVA FOR PPP
        company_adjusted_gva = sum(
            EconomicAdjustmentsHelper.get_adjusted_baseline_gva(
                company_gva, base_year, company_country, location_shares))
        food_mode, nace_code = check_and_get_food_mode_nace_code(nace_code)
        scope1_iea_sector = get_iea_sector_for_nace_code(nace_code)
        iea_sectors = [scope1_iea_sector, "Power", "Total"]

        scopes = {}
        company_eeis = [
            x / company_adjusted_gva for x in company_emissions_per_scope
        ]
        company_emissions = Emission(base_year, target_year)
        for i in range(len(iea_sectors) - 1):
            growth_obj = Growth.create_entity_pathway_from_predefined_scenario(
                selector=iea_sectors[i],
                base_year=base_year,
                target_year=target_year,
                scenario=scenario_code,
                company_country=company_country,
                company_base_year_eei=company_eeis[i],
                sector_base_year_eei=sector_eeis['eei_scope' + str(i + 1)],
                location_shares=location_shares,
                ssp_scenario=ssp_scenario,
                pathway=pathway,
                emissions_to_xdc=xdc_method)

            scopes[i] = Scope(company_eeis[i],
                              growth_obj.get_eei_curve(),
                              scope_weight=scope_weights[i])
            company_emissions.add_scope(scopes[i], "scope" + str(i + 1))

        active_gases = []
        #Make sure the agri_shares are used for 2-digit nace code
        agri_sector_shares = get_sector_agriculture_shares(nace_code)
        if scope3_agri_share is not None:
            if len(scope3_agri_share) != 3:
                raise ValueError("Always provide all three GHG gases in:" +
                                 "N2O, CH4, CO2 in the given order")
            agri_company_shares = {
                "nonAgri": 1 - sum(scope3_agri_share),
                "AgriN2O": scope3_agri_share[0],
                "AgriCH4": scope3_agri_share[1],
                "AgriCO2": scope3_agri_share[2]
            }
        else:
            agri_company_shares = AGRICULTURE_SECTOR_SHARES[nace_code]
        for gas in ["N2O", "CH4", "CO2"]:
            if agri_sector_shares["Agri" + gas] != 0 and agri_company_shares[
                    "Agri" + gas] != 0:
                active_gases.append(gas)
            #elif agri_company_shares["Agri" + gas] != 0:
            #    active_gases.append(gas)

        #scope3 - nonagri
        growth_object = Growth.create_entity_pathway_from_predefined_scenario(
            selector=iea_sectors[2],
            base_year=base_year,
            target_year=target_year,
            scenario=scenario_code,
            company_country=company_country,
            company_base_year_eei=company_eeis[2] *
            agri_company_shares["nonAgri"],
            sector_base_year_eei=sector_eeis['eei_scope3'] *
            agri_sector_shares['nonAgri'],
            location_shares=location_shares,
            ssp_scenario=ssp_scenario,
            pathway=pathway,
            emissions_to_xdc=xdc_method)
        growth_curve = growth_object.get_eei_curve()

        scope = Scope(start_eei=company_eeis[2] *
                      agri_company_shares["nonAgri"],
                      growth_curve=growth_curve,
                      scope_weight=scope_weights[2],
                      scope_tags=["scope3", "scope3_non_agri"])

        company_emissions.add_scope(scope=scope, scope_name="scope3_non_agri")

        ## Scope 3 - Agri

        for i, gas in enumerate(active_gases):
            #print(gas, agri_sector_shares["Agri" + gas],
            #      agri_company_shares["Agri" + gas])
            growth = Growth.create_entity_pathway_from_predefined_scenario(
                selector=gas,
                base_year=base_year,
                target_year=target_year,
                scenario=ScenarioCode.value_of("ssp2t"),
                company_country=company_country,
                company_base_year_eei=company_eeis[2] *
                agri_company_shares["Agri" + gas],
                sector_base_year_eei=sector_eeis['eei_scope3'] *
                agri_sector_shares["Agri" + gas],
                location_shares=location_shares,
                ssp_scenario=ssp_scenario,
                pathway=pathway,
                emissions_to_xdc=xdc_method)

            growth_curve = growth.get_eei_curve()

            scope = Scope(start_eei=company_eeis[2] *
                          agri_company_shares["Agri" + gas],
                          growth_curve=growth_curve,
                          scope_weight=scope_weights[2],
                          scope_tags=["scope3", "scope3_agri"])
            company_emissions.add_scope(
                scope=scope,
                scope_name="scope3_agri_%s" % gas,
            )
        company_emissions._consolidate_tagged_scopes("scope3_agri")

        return company_emissions


def get_sector_agriculture_shares(nace_code):
    nace_code_sector = nace_code.split('.')[0].zfill(2)
    return AGRICULTURE_SECTOR_SHARES[nace_code_sector]
