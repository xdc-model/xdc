#!/usr/bin/python3
# -*- coding: utf-8 -*-

import numpy as np
import math
import fair
import logging

from xdc_core.xdc_helper.helper import (legacy_to_approx_fair_xdc,
                                        reverse_look_up, array_to_dict)
from xdc_core.xdc_helper.fair_helper import get_projected_emissions_for_fair
from xdc_core.xdc_helper.fair_restarts import get_restart_in
from xdc_core import xdc_constants as consts

logging.getLogger(__name__)


class XDC:
    """ Class to convert emissions into XDC value"""

    base_year_limits = (2010, 2018)
    target_year_limits = (2030, 2050)

    @staticmethod
    def _check_base_year(base_year: int):
        """Function for testing that the base_year is is the expected range.
        :param base_year: Base year of the object to be tested
        :type base_year: int
        :raises ValueError: raise Error when base_year exceeds limits
        """
        if not XDC.base_year_limits[0] <= base_year <= XDC.base_year_limits[1]:
            msg = "Base year %d indicated exceed limits %s" % (
                base_year,
                XDC.base_year_limits,
            )
            logging.error(msg)
            raise ValueError(msg)

    @staticmethod
    def _check_target_year(target_year: int):
        """Function for testing that the target_year is is the expected range.
        :param target_year: Base year of the object to be tested
        :type target_year: int
        :raises ValueError: raise Error when target_year exceeds limits
        """
        if not XDC.target_year_limits[
                0] <= target_year <= XDC.target_year_limits[1]:

            msg = "Target year %d indicated is not supported. Must be in range: %s"\
                % (
                    target_year,
                    XDC.target_year_limits,
                )
            logging.error(msg)
            raise ValueError(msg)

    @staticmethod
    def _logging_and_checks(emissions: dict,
                            base_year: int = None,
                            target_year: int = None):
        logging.debug("calculating XDCs for scopes: %s", emissions.keys())
        logging.debug("Emissions are %s", emissions)
        if not emissions:
            raise ValueError('Emissions dictionary cannot be empty')
        for emissions_array in emissions.values():
            if emissions_array.shape[0] != target_year - base_year + 1:
                raise ValueError(
                    'The length of (at least) one of the emissions arrays '
                    f'({emissions_array.shape[0]}) '
                    'does not match the number of years '
                    f'({target_year - base_year + 1}).')

        XDC._check_base_year(base_year)
        XDC._check_target_year(target_year)

    @staticmethod
    def emissions_to_legacy_xdc(
        emissions: dict,
        base_year: int = None,
        target_year: int = None,
        is_return_array: bool = False,
    ) -> dict or np.array:
        """
        Converts the list of CO2eq emissions into an XDC value with the Legacy
        XDC model. The calculation is performed in multiple steps:
        1) Derive the CO2eq concentration increase in the atmosphere in ppm
        based on the amount of emitted CO2 in tonnes.
        2) Add the GHG concentration increase to the measured CO2 concentration
        of 2016 to obtain the total concentration.
        3) Divide by the pre industrial CO2 concentration from 1750.
        4) The ratio of resulting to pre industrial CO2 is converted to
        radiative forcing.
        5) The XDC value is a result of radiative forcing times a climate
        sensitivity factor.

        :param emissions: Dictionary containing arrays of cumulative emissions
            and their labels (keys)
        :type emissions: dict
        :param base_year: base year of the emissions
        :type base_year: int
        :param target_year: target year until which the emissions are provided
        :type target_year: int
        :param is_return_array: where the return type should be an np.array,
            default is False, and the return type is a dictionary
        :raises ValueError: if the emission array(s) are either empty or not
        the size indicated by the number of years between base year and target
        :return: XDC value(s) in degree.
        :rtype: dict (or np.array if is_return_array)
        """
        XDC._logging_and_checks(emissions, base_year, target_year)

        if len(emissions) > 1:
            emissions_2d_array = np.vstack(list(emissions.values()))
            sum_of_emissions = np.sum(emissions_2d_array, axis=1)
        else:
            sum_of_emissions = np.array([np.sum(list(emissions.values()))])

        ghg_inc = sum_of_emissions / consts.CO2EQ_PPMV

        # Apply the increment caused by the emissions to the
        # concentration in 2016 and divide by pre-industrial value
        ghg_ratio = (consts.GHG_CONC[base_year - 1] +
                     ghg_inc) / consts.GHG_CONC[1750]

        # Radiative forcing factor of CO2, e.g.
        # Gohar and Shine, Weather - Nov 2007, Vol. 62, No. 11, eq. 2

        # incl. correction for non CO2 induced radiative forcing,
        # total -0.85, last column, table 8.6, page 696 IPCC AR5, WGI
        # http://www.ipcc.ch/report/ar5/wg1/
        radiative_forcing = (
            consts.RADIATIVE_FORCING_FACTOR * np.log(ghg_ratio) +
            consts.RADIATIVE_FORCING_CORRECTION)

        xdc_values = radiative_forcing * consts.CLIMATE_SENSITIVITY
        logging.info("calculated XDCs: %s.", xdc_values)

        if is_return_array:
            return xdc_values

        xdc_values_dict = array_to_dict(xdc_values, list(emissions.keys()))
        logging.info("calculated XDCs: %s.", xdc_values_dict)

        return xdc_values_dict

    @staticmethod
    def emissions_to_converted_fair_xdc(emissions: dict,
                                        base_year: int = None,
                                        target_year: int = None) -> dict:
        """Converts the list of emissions into an XDC value with the legacy
        XDC model and converts it into the FaIR value using the look up table

        :param emissions: dictionary containing labels and np.arrays of
            emissions
        :type emissions: dict
        :param base_year: base year of the emissions
        :type base_year: int
        :param target_year: target year until which the emissions are
                                provided
        :type target_year: int
        :return: XDC value(s) in degree.
        :rtype: dict
        """
        legacy_xdc = XDC.emissions_to_legacy_xdc(emissions,
                                                 base_year,
                                                 target_year,
                                                 is_return_array=True)

        fair_xdc = legacy_to_approx_fair_xdc(legacy_xdc, base_year)
        fair_xdc_dict = array_to_dict(fair_xdc, list(emissions.keys()))

        return fair_xdc_dict

    @staticmethod
    def _run_fair(emissions: np.array, base_year: int,
                  target_year: int) -> float:
        """Run FaIR

        :param emissions: array containing emissions
        :type emissions: np.array
        param base_year: the first year of emissions data
        :type base_year: int
        param target_year: the last year of emissions data
        :type target_year: int
        :return: temperature in the target year
        :type: float
        """
        np.seterr(all='raise')

        emissions_array = get_projected_emissions_for_fair(
            emissions, base_year, target_year)

        restart_in = get_restart_in(base_year)

        try:
            c, f, t = fair.forward.fair_scm(
                emissions_driven=True,
                emissions=emissions_array,
                natural=consts.natural_emissions,
                F_volcanic=0,  # no future volcanoes
                F_solar=0,  # no significant change on decadal timescales
                fixPre1850RCP=False,
                ghg_forcing="meinshausen",
                useMultigas=True,
                tcrecs=consts.historically_constrained_tcr_and_ecs,
                C_pi=consts.pi_concentrations,
                tropO3_forcing='cmip6_stevenson',
                gir_carbon_cycle=True,
                restart_in=restart_in,
            )
        except FloatingPointError:
            raise RuntimeError(
                'FaIR is crashing, probably due to too many negative emissions.'
            )

        np.seterr(all='warn')

        return t[-1]

    @staticmethod
    def emissions_to_fair_xdc(
        emissions: dict,
        base_year: int = None,
        target_year: int = None,
    ) -> dict:
        """Converts the emissions into an XDC value with the FaIR model

        :param emissions: dictionary containing labels and np.arrays of
            emissions
        :type emissions: dict
        :param base_year: base year of the emissions
        :type base_year: int
        :param target_year: target year until which the emissions are
                                provided
        :type target_year: int
        :return: XDC value(s) in degree.
        :rtype: dict
        """
        XDC._logging_and_checks(emissions, base_year, target_year)

        xdc_dict = {}
        for scope_name, emissions_array in emissions.items():
            xdc_dict[scope_name] = XDC._run_fair(emissions_array, base_year,
                                                 target_year)

        return xdc_dict

    @staticmethod
    def legacy_xdc_to_emissions(legacy_xdc: float, base_year: int) -> float:
        """Convert a legacy XDC value analytically to the sum of
        emissions used to cause the XDC

        :param legacy_xdc: XDC value
        :type legacy_xdc: float
        :param base_year: base year from which the emissions started
        :type base_year: int
        :param target_year: year up to which the emissions are projected
                            (optional as the duration is irrlevant)
        :type target_year: int
        :return: sum of total emissions
        :rtype: float
        """
        XDC._check_base_year(base_year)

        # Get radiative forcing from temperature by dividing by climate
        # sensitivity
        rf = legacy_xdc / consts.CLIMATE_SENSITIVITY

        # Revert correction for non CO2 induced radiative forcing and extract
        # ghg ratio
        ghg_ratio = math.exp((rf - consts.RADIATIVE_FORCING_CORRECTION) /
                             consts.RADIATIVE_FORCING_FACTOR)

        # Multiply ratio by pre-industrial concentration to get total
        # concentration and substract current concentration
        # To keep only concentration increase caused from now till 2050
        ghg_inc = ghg_ratio * consts.GHG_CONC[1750] - consts.GHG_CONC[base_year
                                                                      - 1]

        # Convert to tons of CO2eq
        emissions_total = ghg_inc * consts.CO2EQ_PPMV
        return emissions_total

    @staticmethod
    def fair_xdc_to_emissions(fair_xdc: float, base_year: int) -> list:
        """Use a minimizer to find the best match of emissions to meet
        the provided FairXDC

        :param fair_xdc: XDC value
        :type fair_xdc: float
        :param base_year: base year from which the emissions started
        :type base_year: int
        :param target_year: year up to which the emissions are projected
        :type target_year: int
        :return: list of emissions put into FaIR with a length of
                 target_year - base_year +1
        :rtype: list
        """
        XDC._check_base_year(base_year)
        legacy_xdc = reverse_look_up(fair_xdc, base_year)
        emissions_total = XDC.legacy_xdc_to_emissions(legacy_xdc, base_year)

        return emissions_total
