import numpy as np
from xdc_core.xdc_helper.legacy2fair import LEGACY_2_FAIR


def fit_l2f() -> str:
    """This function will fit the look-up tables with a polynomial of second degree and
    return the resulting fit parameters for each year

    :return: Fit parameters
    :rtype: str
    """
    # Rank of the polynomial
    rank = 2
    lists = {}
    x = {}
    y = {}
    res_fit = {}
    for year in LEGACY_2_FAIR.keys():
        # Read out the legacy xdc values and the xdcfair values from the look-up
        # table and put them into two arrays to fit them
        lists[year] = sorted(
            {float(k): float(v)
             for k, v in LEGACY_2_FAIR[year].items()}.items())
        x[year], y[year] = zip(*lists[year])
        fit = np.polyfit(x[year], y[year], rank)
        # This is only done to make the printing easier res_fit[year]=fit works equally as well
        res_fit[year] = [fit[0], fit[1], fit[2]]

    return str(res_fit).replace('],', '],\n\t')


if __name__ == '__main__':
    print(fit_l2f())
