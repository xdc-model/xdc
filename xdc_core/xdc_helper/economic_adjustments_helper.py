import logging
import statistics

from xdc_core.region import Region
from xdc_core.country_code import CountryCode
import xdc_core.xdc_helper.data_api_helper as DataApiHelper
from cachetools import cached, LRUCache, TTLCache

logging.getLogger(__name__)


class ReadOnlyDict(dict):
    def __readonly__(self, *args, **kwargs):
        raise RuntimeError("Cannot modify ReadOnlyDict")

    __setitem__ = __readonly__
    __delitem__ = __readonly__
    pop = __readonly__
    popitem = __readonly__
    clear = __readonly__
    update = __readonly__
    setdefault = __readonly__
    del __readonly__


@cached(cache=LRUCache(maxsize=1024))
def get_gva_adjustment_factor(year: int,
                              region: Region = None,
                              country: CountryCode = None):
    """Get gva adjustment factor.

    The GVA adjustment factor is derived as:
    ppp_conversion_2017 * gdp_deflator_2017 / gdp_deflator_year

    :param year: base year
    :type year: int
    :param region: ssp region, defaults to None
    :type region: str, optional
    :param country: ISO 3166-1 alpha-3 code, defaults to None
    :type country: str, optional
    :return: deflator factor of supplied country/ region/ global and year
    :rtype: float
    """
    ppp_2017 = None
    deflator_2017 = None
    deflator = None
    # if any of the variables is None then fall back to region
    if country is not None:
        assert (region is None)
        ppp_2017 = DataApiHelper.get_country_ppp(country, 2017)
        deflator_2017 = DataApiHelper.get_country_deflator(country, 2017)
        deflator = DataApiHelper.get_country_deflator(country, year)
        if None in [ppp_2017, deflator_2017, deflator]:
            logging.info("Country: %s is missing some information for %i:",
                         country.value, year)
            region = DataApiHelper.get_region_by_country(country)

    if region is not None:
        if None in [deflator, deflator_2017]:
            logging.debug("Use region: %s - for deflators.", region.value)
            deflator_2017 = DataApiHelper.get_region_deflator(region, 2017)
            deflator = DataApiHelper.get_region_deflator(region, year)
        if ppp_2017 is None:
            logging.debug("Use region: %s - for ppp.", region.value)
            ppp_2017 = DataApiHelper.get_region_ppp(region, 2017)
        if None in [ppp_2017, deflator_2017, deflator]:
            logging.error("Region: %s is missing some information.",
                          region.value)
            raise ValueError("Region is missing some information:",
                             region.value, [year, region, country],
                             [ppp_2017, deflator_2017, deflator])
    elif country is None:
        ppp_2017 = DataApiHelper.get_global_ppp(2017)
        deflator_2017 = DataApiHelper.get_global_deflator(2017)
        deflator = DataApiHelper.get_global_deflator(year)

    if None in [ppp_2017, deflator_2017, deflator]:
        raise ValueError('Deflators missing:', [year, region, country],
                         [ppp_2017, deflator_2017, deflator])

    return ppp_2017 * deflator_2017 / deflator


@cached(cache=TTLCache(maxsize=1024, ttl=86400))
def get_adjusted_sector_eei(base_year: int, nace_code: str, currency: str):
    """Get adjusted sector EEI.

    :param base_year: base year
    :type base_year: int
    :param nace_code: NACE sector
    :type nace_code: str
    :param currency: "eur" or "usd"
    :type currency: str
    :return: dictionary containing the company count and the
        sector median adjusted EEI values for each scope
    :rtype: dict
    """
    # This limits nace code to single or two digits
    nace_code = nace_code[:2]
    sector_companies = DataApiHelper.get_sector_companies(
        base_year, nace_code, Region.WORLD, currency)
    company_count = sector_companies['companyCount']
    adjusted_scope1_eeis, adjusted_scope2_eeis, adjusted_scope3_eeis = [], [], []
    # Default internationalization factors divides company's GVA equally
    location_shares = [1 / 3] * 3
    countries = {d['countryCode'] for d in sector_companies["data"]}
    company_gva_split_adjusted = {}
    for country in countries:
        company_gva_split_adjusted[country] = get_adjusted_baseline_gva(
            1, base_year, CountryCode[country], location_shares)

    for company in sector_companies["data"]:
        # Sum over adjusted GVA shares
        company_gva_adjusted = sum(company_gva_split_adjusted[
            company['countryCode']]) * company['gva']

        # Calculate and append scope 1, 2, 3 EEIs based on adjusted GVA
        adjusted_scope1_eeis.append(company["scope1"] / company_gva_adjusted)
        adjusted_scope2_eeis.append(company["scope2"] / company_gva_adjusted)
        adjusted_scope3_eeis.append(company["scope3"] / company_gva_adjusted)

    # Find median in EEI for scope 1, 2, 3
    adjusted_scope1_sector_eei = statistics.median(
        sorted(adjusted_scope1_eeis))
    adjusted_scope2_sector_eei = statistics.median(
        sorted(adjusted_scope2_eeis))
    adjusted_scope3_sector_eei = statistics.median(
        sorted(adjusted_scope3_eeis))
    results = ReadOnlyDict({
        "eei_scope1": adjusted_scope1_sector_eei,
        "eei_scope2": adjusted_scope2_sector_eei,
        "eei_scope3": adjusted_scope3_sector_eei,
        #"company_count": company_count
    })
    return company_count, results


#@cached(cache=LRUCache(maxsize=1024))
#This depends on company gva, not sure if it makes sense to cache...
def get_adjusted_baseline_gva(company_gva: float, base_year: int,
                              country_code: CountryCode,
                              location_shares: list):
    """Split the GVA into parts based on the location shares
    and multiply each of them with the GVA adjustment factors
    corresponding to the country / region / world.

    :param company_gva: base year GVA of the company
    :type company_gva: float/int
    :param base_year: base year
    :type base_year: int
    :param country_code: ISO 3166-1 alpha-3 code
    :type country_code: CountryCode
    :param location_shares: fraction of company GVA coming from
        the country, the region (e.g. OECD) and the world
    :type location_shares: list
    :return: adjusted GVA shares
    :rtype: list
    """
    # Get global GVA adjustment factor
    gva_adjustment_factor_global = get_gva_adjustment_factor(base_year)
    # Get region per company in sector
    region_code = DataApiHelper.get_region_by_country(country_code)
    # Get company's GVA adjustment factor based on headquarter
    gva_adjustment_factor_country = get_gva_adjustment_factor(
        base_year, country=country_code)
    # Get region's GVA adjustment factor
    gva_adjustment_factor_region = get_gva_adjustment_factor(
        base_year, region=region_code)
    # List of all GVA Adjustment factors [Country, Region, Global]
    adjustment_factors = [
        gva_adjustment_factor_country, gva_adjustment_factor_region,
        gva_adjustment_factor_global
    ]

    # Split company's GVA based on default shares
    company_gva_split = [factor * company_gva for factor in location_shares]

    # Multiply each GVA share with corresponding adjustment factor
    # based on country, region and global
    company_gva_split_adjusted = [
        factor * gva_split
        for (factor, gva_split) in zip(adjustment_factors, company_gva_split)
    ]

    return tuple(company_gva_split_adjusted)


def get_adjusted_world_gva(base_year):
    """Get the adjusted world GVA.

    :param base_year: base year
    :type base_year: int
    :return: adjusted world gva
    :rtype: float
    """
    world_gva = DataApiHelper.get_global_gva(base_year)
    world_adjusted_gva = get_gva_adjustment_factor(base_year) * world_gva
    return world_adjusted_gva
