import numpy as np
from copy import copy

from xdc_core.xdc_constants import (c_to_co2_ratio, n2_to_n2o_ratio, gwp_n2o,
                                    gwp100_ch4, gwp20_ch4)


def convert_to_co2_equivalent(emissions_array):
    """Convert methane and N2O emissions to CO2e and sum these and the
    CO2 emissions from fossil fuels and industry and land-use change.
    Put this sum into an array to be treated as CO2 by FaIR.
    Neglect all other trace greenhouse gas and aerosol emissions.

    GWP is used used to convert N2O to CO2e, while methane is converted
    to CO2e* (Allen et al. 2018), due to its shorter atmospheric
    lifetime.
    The values taken for GWP100 are:
    28 for methane and 265 for N2O
    from the IPCC AR5 (table 8.7) with no carbon cycle feedback.
    As the carbon cycle is modelled explicitly in FaIR, this feedback
    is included inside the FaIR runs.

    :param emissions_array: emissions array in the shape required by FaIR
        (number of years, 40) with the second dimension for different gases
        (indices 1-40) and containing methane (index 3) and N2O (index 4)
        explicitly. For a list of all gases and their positions in the
        array, see Table 1 of Smith et al. 2018.
    :type emissions_array: np.array
    :return: new emissions array containing the sum of co2 emissions and
        co2 equivalents for methane and N2O in a single entry
    :rtype: np.array
    """
    ch4_as_e = convert_methane_to_co2e(emissions_array[:, 3])
    n2_as_n2o = emissions_array[:, 4] / n2_to_n2o_ratio  # Mt N2O yr-1
    n2_as_co2 = n2_as_n2o * 10**-3 * gwp_n2o  # Gt CO2 yr-1
    n2_as_e = n2_as_co2 * c_to_co2_ratio  # Gt C yr-1
    co2equivalent = (emissions_array[:, 1] + emissions_array[:, 2] + ch4_as_e +
                     n2_as_e)

    emissions_eq = copy(emissions_array)  # create new array
    emissions_eq[:, 1] = co2equivalent  # set co2 as co2 equivalent
    emissions_eq[:, 2:5] = 0  # remove land-use co2, methane and n2o

    return emissions_eq


def remove_minor_gasses(emissions):
    """Remove minor gases from the emissions array.

    :param emissions: emissions array for input to FaIR
    :type emissions: np.array
    :return: emissions array with minor gases removed
    :rtype: np.array
    """
    emissions[:, 5:] = 0  # remove other trace gas emissions
    return emissions


def convert_methane_to_co2e(ch4):
    """Convert methane emissions to CO2 equivalents.

    :param ch4: annual series of methane emissions [Mt yr-1]
    :type ch4: np.array

    :return: equivalent CO2 emissions [Gt C yr-1]
    :rtype: np.array
    """
    ch4 *= 10**-3
    ch4_as_c = ch4 * gwp100_ch4 * c_to_co2_ratio
    return ch4_as_c


def get_projected_emissions_for_fair(co2e_emissions, base_year, target_year):
    """Put the 1d CO2 emissions array into a 2d array of the size
    required as input to FaIR.

    :param co2e_emissions: yearly CO2 equivalent emissions
        from Emission.get_scaled_emission() in t CO2 yr-1
    :type co2e_emissions: ndarray
    :param base_year: base year of the company data
    :type base_year: int
    :param target_year: end year of the model run (e.g. 2050)
    :type target_year: int
    :return: in the form required for FaIR, containing co2 equivalent
        emissions in Gt C yr-1
    :rtype: ndarray
    """
    emissions = np.zeros((target_year - base_year + 1, 40))
    emissions[:, 0] = list(np.arange(base_year, target_year + 1, 1))
    emissions[:, 1] = co2e_emissions * 10**-9 * c_to_co2_ratio  # Gt C yr-1
    return emissions


def n2o_to_co2_equivalent(n2o_emissions: np.array):
    """Helper function for the food manufacturer mode to convert
    tons of N2O to CO2 equivalents. Input list can be of arbitrary
    length.  

    :param n2o_emissions: tons of N2O emissions
    :type n2o_emissions: np.array
    :return: tons of CO2 equivalents
    :rtype: np.array
    """

    n2o_as_co2 = n2o_emissions * gwp_n2o  # t CO2 yr-1

    return n2o_as_co2


def ch4_to_co2_equivalent(ch4_emissions: np.array):
    """Helper function for the food manufacturer mode to convert
    tons of CH4 to CO2 equivalents. Input list can be of arbitrary
    length. Currently uses gwp100_ch4.

    :param ch4_emissions: tons of CH4 emissions
    :type ch4_emissions: np.array
    :retushould be changed to gwp20 once
    we know the value.rn: tons of CO2 equivalents
    :rtype: np.array
    """
    # TODO: change gwp100_ch4 to gwp_20_ch4 once we know the value
    ch4_as_co2 = ch4_emissions * gwp20_ch4  # t CO2 yr-1

    return ch4_as_co2
