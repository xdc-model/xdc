import hashlib
import json
import os
from datetime import datetime
import logging
try:
    import git
except ModuleNotFoundError:
    logging.warning("Git is not installed")
import numpy as np
from scipy.interpolate import interp1d

from xdc_data.scenario_budgets import (nace_1dig_map, nace_2_or_3_digit_map)
from xdc_core.xdc_helper.legacy2fair import *
from xdc_core.xdc_error import ErrorCodes, XdcError

from xdc_core.xdc_constants import AGRICULTURE_SECTOR_SHARES
module_logger = logging.getLogger(__name__)


class ReportHelper():

    report_cache: list = []


def array_to_dict(values_array, list_keys):
    """Label array values using a list of keys

    :param values_array: a 1d array of values to be labelled
    :type values_array: np.array
    :param list_keys: a list of labels
    :type list_keys: list
    :return: a dictionary of labelled values
    :rtype: dict
    """
    values_dict = {}
    for i in range(len(list_keys)):
        values_dict[list_keys[i]] = values_array[i]
    return values_dict


def rounder(num: float, up=True) -> str:
    """Returns the 2-digits rounded string value of the number 
     passed as argument, ceil if up, else floor.
    :param num: number to round
    :type num: float
    :param up: whether one should round up, defaults to True
    :type up: bool, optional
    :return: rounded 2 digit number as a string
    :rtype: str
    """
    digits = 2
    mul = 10**digits
    if up:
        return ["{:.2f}".format(np.ceil(num_i * mul) / mul) for num_i in num]
    else:
        return ["{:.2f}".format(np.floor(num_i * mul) / mul) for num_i in num]


def legacy_to_approx_fair_xdc(legacy_xdc: float or np.array,
                              base_year: int) -> np.array:
    """Returns the approximated xdcfair xdc value for the legacy xdc
    and the base year passed as argument.

    :param legacy_xdc: XDC value received from the legacy method (single or 
    array)
    :type legacy_xdc: float or np.array
    :param base_year: year for which the XDC was calculated
    :type base_year: int
    :return: array of converted XDCs
    :rtype: np.array
    """
    if isinstance(legacy_xdc, np.ndarray):
        legacy_xdc = list(legacy_xdc)
    else:
        legacy_xdc = [legacy_xdc]
    x1_str = rounder(legacy_xdc, up=False)
    x1_float = [float(x1_str_i) for x1_str_i in x1_str]
    x2_str = rounder(legacy_xdc, up=True)
    x2_float = [float(x2_str_i) for x2_str_i in x2_str]
    # Preferably use the Look-Up tables
    return_converted_xdc = []
    for i in range(len(x2_str)):
        if x2_str[i] in LEGACY_2_FAIR[base_year].keys():
            x = [x1_float[i], x2_float[i]]
            y = [
                LEGACY_2_FAIR[base_year][x1_str[i]],
                LEGACY_2_FAIR[base_year][x2_str[i]]
            ]
            f = interp1d(x, y)
            converted_xdc = f(legacy_xdc[i]).item()
        else:
            # If the look-up tables have no entry, use the fit function
            module_logger.warning(
                "Warning: Using extrapolation of L2F conversion")
            converted_xdc = legacy_to_extrapolated_fair_xdc(
                legacy_xdc[i], base_year)
        return_converted_xdc.append(converted_xdc)
    return np.array(return_converted_xdc)


def legacy_to_extrapolated_fair_xdc(legacy_xdc: float, base_year: int):
    """Returns the extrapolated xdcfair value starting from the legacy
    xdc and the base year passed as argument.
    :param legacy_xdc: XDC value
    :type legacy_xdc: float
    :param base_year: year for which the XDC was calculated
    :type base_year: int
    :return: converted XDC value using the extrapolated function
    :rtype: float
    """
    func = np.poly1d(LEGACY_2_FAIR_FIT[base_year])
    return func(legacy_xdc)


def reverse_look_up(fair_value: float, base_year: int) -> float:
    """Returns the legacy xdc value starting from the xdcfair
    xdc and the base year passed as argument.

    :param fair_value: Fair XDC Value
    :type fair_value: float
    :param base_year: year for which the XDC was calculated
    :type base_year: int
    :return: Equivalent legacy_XDC value
    :rtype: float
    """
    val = min(LEGACY_2_FAIR[base_year].values(),
              key=lambda v: abs(v - fair_value))
    key = filter(lambda k: LEGACY_2_FAIR[base_year][k] == val,
                 LEGACY_2_FAIR[base_year].keys())

    return float(list(key)[0])


def get_grown_array_from_value(length: int,
                               value: float,
                               rate: float,
                               lin: bool = False) -> np.ndarray:
    """Derive an array of values under a specific growth rate based on
    an input value.

    This function growths a given value in a given time range under
    a given exponential or linear growth rate an returns a list. It 
    also limits the growth to be >= 0 and <= 100 percent.

    :param length:  number of entries in the list
    :type length: int
    :param value: base value in base_year
    :type value: float
    :param rate: growth rate to be applied to value
    :type rate: float
    :param lin: assume linear (True) or exponential (False) growth, defaults to False
    :type lin: bool, optional
    :return: growth curve for the number of years provided in length
    :rtype: np.ndarray
    """
    rate = -100 if rate < -100 else rate

    grown_array = value * np.ones((length + 1, ))
    j = np.array(range(0, length + 1))
    if lin:
        grown_array *= (1 + rate / 100 * j)
    else:
        grown_array *= (1 + rate / 100)**j
    grown_array[grown_array <= 0] = 0

    #debug_msg = 'Generated array from value: %.4f and rate: %.2f' % (value, rate)
    module_logger.debug('Generated array from value: %.4f and rate: %.2f',
                        value, rate)

    return grown_array


def is_jsonable(x):
    """Checks if the object passed as argument can be dumped to json or not

    :param x: object to be turned into json
    :type x: Any
    :return: True if object is jsonable, False otherwise
    :rtype: bool
    """
    try:
        json.dumps(x)
        return True
    except:
        return False


def get_iea_sector_for_nace_code(nace_code: str) -> str:
    """Returns IEA sector for a given NACE code. NACE code can be both 
    alphabetic (one digit) or numeric (two, three or four digits). 
    Only the first to digits of numeric input are used.

    :param nace_code: nace code to be mapped
    :type nace_code: str
    :raises XdcError: Error if an agricultural is selected, as no scenario 
    currently exists
    :return: iea sector corresponding to the nace code
    :rtype: str
    """
    if nace_code.isalpha():
        selector = nace_1dig_map.get(nace_code)
        if selector is None:
            module_logger.warning('NACE Code %s is set to "Other"', nace_code)
            selector = "Other"
    else:
        nace_code = nace_code.split('.')[0].zfill(2)
        selector = nace_2_or_3_digit_map.get(nace_code)
        if selector is None:
            error_message = f"NACE code {nace_code} not yet supported."
            raise XdcError(ErrorCodes.ERR_INPUT_CONFLICT, nace_code,
                           error_message)

    return selector


def get_git_info() -> dict:
    """Creates and returns a dictionay containing information about the 
    current repository status.
    return: dictionary with git info
    rtype: dict
    """
    try:
        repo = git.Repo(os.path.dirname(__file__),
                        search_parent_directories=True)
    except NameError:
        module_logger.warning("GitPython not installed")
        return {
            'author': 'unknown',
            'message': 'GitPython is not installed',
            'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
    except git.exc.InvalidGitRepositoryError:
        module_logger.warning("Git repository not found")
        return {
            'author': 'unknown',
            'message': 'Git repository not found',
            'timestamp': datetime.now().strftime('%Y-%m-%d %H:%M:%S')
        }
    last_cmt = repo.head.commit
    msg = last_cmt.message.splitlines()[0]

    status = {
        'author':
        last_cmt.committer.name,
        'message':
        msg,
        'timestamp':
        datetime.fromtimestamp(
            last_cmt.committed_date).strftime('%Y-%m-%d %H:%M:%S'),
        'clean':
        not repo.is_dirty()
    }

    return status


# def set_db_info_to_report(db_report: dict) -> uuid:
#     """[summary]

#     :param db_report: [description]
#     :type db_report: dict
#     :return: [description]
#     :rtype: uuid
#     """

#     db_report_uuid = str(uuid.uuid4())
#     db_report['db_report_id'] = db_report_uuid
#     ReportHelper.report_cache.append(db_report)

#     return db_report_uuid


def get_1digit_nace_code(nace_code) -> str:
    """ Get parent nace code for 2 digit nace code

    :param nace_code: 2 digit nace code
    :type nace_code: str
    :return: 1 digit nace code
    :rtype: str
    """
    nace_lookup_table = {
        '01': 'A',
        '02': 'A',
        '03': 'A',
        '05': 'B',
        '06': 'B',
        '07': 'B',
        '08': 'B',
        '09': 'B',
        '10': 'C',
        '11': 'C',
        '12': 'C',
        '13': 'C',
        '14': 'C',
        '15': 'C',
        '16': 'C',
        '17': 'C',
        '18': 'C',
        '19': 'C',
        '20': 'C',
        '21': 'C',
        '22': 'C',
        '23': 'C',
        '24': 'C',
        '25': 'C',
        '26': 'C',
        '27': 'C',
        '28': 'C',
        '29': 'C',
        '30': 'C',
        '31': 'C',
        '32': 'C',
        '33': 'C',
        '35': 'D',
        '36': 'E',
        '37': 'E',
        '38': 'E',
        '39': 'E',
        '41': 'F',
        '42': 'F',
        '43': 'F',
        '45': 'G',
        '46': 'G',
        '47': 'G',
        '49': 'H',
        '50': 'H',
        '51': 'H',
        '52': 'H',
        '53': 'H',
        '55': 'I',
        '56': 'I',
        '58': 'J',
        '59': 'J',
        '60': 'J',
        '61': 'J',
        '62': 'J',
        '63': 'J',
        '64': 'K',
        '65': 'K',
        '66': 'K',
        '68': 'L',
        '69': 'M',
        '70': 'M',
        '71': 'M',
        '72': 'M',
        '73': 'M',
        '74': 'M',
        '75': 'M',
        '77': 'N',
        '78': 'N',
        '79': 'N',
        '80': 'N',
        '81': 'N',
        '82': 'N',
        '84': 'O',
        '85': 'P',
        '86': 'Q',
        '87': 'Q',
        '88': 'Q',
        '90': 'R',
        '91': 'R',
        '92': 'R',
        '93': 'R',
        '94': 'S',
        '95': 'S',
        '96': 'S',
        '97': 'T',
        '98': 'T',
        '99': 'U'
    }
    return (nace_lookup_table[str(nace_code).format(".2f")])


def get_checksum(filename: str):
    """Generate checksum for file baed on hash function (MD5).

    :param filename: path to the file to be hashed
    :type filename: str
    :return: hash of the file
    :rtype: str
    """
    with open(filename, "rb") as f:
        bytes = f.read()
        readable_hash = hashlib.md5(bytes).hexdigest()
        return readable_hash


def check_and_get_food_mode_nace_code(nace_code: str = None):
    if nace_code is not None and nace_code[:2] in ("10", "11"):
        food_mode = True
        while nace_code not in AGRICULTURE_SECTOR_SHARES.keys():
            nace_code = nace_code[:-1]

    else:
        food_mode = False

    return food_mode, nace_code
