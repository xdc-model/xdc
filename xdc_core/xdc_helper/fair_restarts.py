from os import path
import functools
from copy import copy

import fair
from fair.RCPs import rcp85
from fair.ancil import cmip6_solar, cmip6_volcanic
import pandas as pd

from xdc_core.xdc_helper.fair_helper import convert_to_co2_equivalent
import xdc_core.xdc_constants as consts


def get_historical_emissions_array(base_year):
    """Get the historical CO2 emissions from a data file and combine them with
    the rcp8.5 scenario (up to the year before the base year), to get a 2d
    emissions array in the format required by FaIR.

    'Global_Carbon_Budget_2019v1.csv' contains emission data from fossil fuel
    combustion and industrial processes from Gilfillan et al.
    (https://energy.appstate.edu/CDIAC, last access: 27 September 2019) and
    emissions from land-use change from Houghton and Nassikas
    (https://doi.org/10.1002/2016GB005546) and Hansis et al.
    (https://doi.org/10.1002/2014GB004997). The data was downloaded from
    https://www.icos-cp.eu/science-and-impact/global-carbon-budget/2019
    (last access: 15 February 2021).

    :param base_year: base year of the company data, one year after the last
        year of these historical runs
    :type base_year: int
    :return: emissions in the form required for FaIR, containing data from
        rcp8.5 and observational data from the Integrated Carbon Observation
        System (ICOS, doi: 10.18160/gcp-2019) for CO2 emissions from fossil
        fuels and industry and CO2 emissions from land-use change
    :rtype: ndarray
    """
    # if base_year==2015, the restart file should go up to and include 2014
    historical_emissions = copy(rcp85.Emissions.emissions[0:base_year - 1765])

    filename = path.join(
        path.dirname(path.dirname(path.dirname(path.realpath(__file__)))),
        "xdc_data", 'Global_Carbon_Budget_2019v1.csv')
    obs = pd.read_csv(filename, sep=';')
    obs_start = obs['Year'][0]

    # observational CO2 emissions data is in GtC/yr, same as used in FaIR
    co2_fossil = obs['fossil fuel and industry'][0:base_year -
                                                 obs_start].values
    co2_land = obs['land-use change emissions'][0:base_year - obs_start].values
    historical_emissions[obs_start - base_year:, 1] = co2_fossil
    historical_emissions[obs_start - base_year:, 2] = co2_land
    return historical_emissions


@functools.lru_cache()
def get_restart_in(base_year):
    """Run FaIR from 1765 to the year before the base_year, to get the
    restart data required to start a FaIR run from the base_year.

    :param base_year: base year of the company data, one year after the last
        year of these historical runs
    :type base_year: int
    :return: FaIR restart information, parameters required for the carbon cycle
        and the temperature at the last time step
    :rtype: tuple
    """

    emissions = convert_to_co2_equivalent(
        get_historical_emissions_array(base_year))
    _, _, _, restart = fair.forward.fair_scm(
        emissions_driven=True,
        emissions=emissions,
        natural=consts.natural_emissions,
        F_volcanic=cmip6_volcanic.Forcing.volcanic[0:base_year - 1765],
        F_solar=cmip6_solar.Forcing.solar[0:base_year - 1765],
        fixPre1850RCP=False,
        ghg_forcing="meinshausen",
        gir_carbon_cycle=True,
        useMultigas=True,
        tcrecs=consts.historically_constrained_tcr_and_ecs,
        C_pi=consts.pi_concentrations,
        tropO3_forcing='cmip6_stevenson',
        restart_out=True,
    )

    return restart
