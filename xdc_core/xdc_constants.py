#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
"""
Initialization constants for the baseline input of the XDC
"""
BASELINE_DEFAULT = {
    'target_year': 2050,
    'base_year': 2017,
    'world_growth': 1.93,
    'emissions_growth': 0.88,
    'scope3_CH4_growth': 0.51,
    'scope3_N2O_growth': 0.8,
    'scope3_non_agriculture_growth': 0.92,
    'scope3_CH4_share': 0.31,
    'scope3_N2O_share': 0.27,
    'company_growth': 1.93,
    's1f': 100,
    's2f': 50,
    's3f': 50,
    'lin': False
}

AGRICULTURE_SECTOR_SHARES = {
    "10": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "11": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.1": {
        "nonAgri": 0.2278,
        "AgriN2O": 0.076,
        "AgriCH4": 0.3924,
        "AgriCO2": 0.3038
    },
    "10.2": {
        "nonAgri": 0.75,
        "AgriN2O": 0.25,
        "AgriCH4": 0.0,
        "AgriCO2": 0.0
    },
    "10.3": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.4": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "10.5": {
        "nonAgri": 0.2535,
        "AgriN2O": 0.0845,
        "AgriCH4": 0.4366,
        "AgriCO2": 0.2254
    },
    "10.6": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.7": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.8": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "10.81": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.82": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.83": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.84": {
        "nonAgri": 0.383,
        "AgriN2O": 0.4468,
        "AgriCH4": 0.0,
        "AgriCO2": 0.1702
    },
    "10.85": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "10.86": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "10.89": {
        "nonAgri": 0.18,
        "AgriN2O": 0.27,
        "AgriCH4": 0.31,
        "AgriCO2": 0.24
    },
    "10.9": {
        "nonAgri": 0.2535,
        "AgriN2O": 0.0845,
        "AgriCH4": 0.4366,
        "AgriCO2": 0.2254
    }
}
"""
 Atmospheric CO2 concentrations #from NOAA AGGI
- Formula: N.A.
- Units: CO2 equivalent mixing ratio (ppm)
- Definition: Atmospheric CO2 concentrations from NOAA AGGI
- Source: NOAA Earth System Research Laboratory
https://www.esrl.noaa.gov/gmd/aggi/aggi.html
"""
GHG_CONC = {}
GHG_CONC[1750] = 278
GHG_CONC[2009] = 465
GHG_CONC[2010] = 468
GHG_CONC[2011] = 471
GHG_CONC[2012] = 474
GHG_CONC[2013] = 478
GHG_CONC[2014] = 481
GHG_CONC[2015] = 485
GHG_CONC[2016] = 490
GHG_CONC[2017] = 493
GHG_CONC[2018] = 496
GHG_CONC[2019] = 500
"""
World Global Gross Value Added from GVA 2010 to 2018 
- Formula: N.A.
- Units: $ (USD)
- Definition: world Global Gross Value Added from GVA 2010 to 2018
- Source: The World Bank
https://data.worldbank.org/indicator/NY.GDP.FCST.CD?end=2016&start=2010 
"""
GVA_GLOBAL_OBSERVED_USD = {}
GVA_GLOBAL_OBSERVED_USD[2010] = 61401557.8290965
GVA_GLOBAL_OBSERVED_USD[2011] = 67245432.6139938
GVA_GLOBAL_OBSERVED_USD[2012] = 67886943.056621
GVA_GLOBAL_OBSERVED_USD[2013] = 68834953.6922292
GVA_GLOBAL_OBSERVED_USD[2014] = 70058656.1213211
GVA_GLOBAL_OBSERVED_USD[2015] = 65276681.0345472
GVA_GLOBAL_OBSERVED_USD[2016] = 66311293.3675467
GVA_GLOBAL_OBSERVED_USD[2017] = 70048792.4823195
GVA_GLOBAL_OBSERVED_USD[2018] = 73495479.7200071
"""
EUR/USD Conversion
- Formula: N.A.
- Units: EUR/USD -> GVA in EUR
- Definition: exchange rates for conversion from Dollar to Euro
- Source: OECD Data
https://data.oecd.org/conversion/exchange-rates.htm
"""
USD_TO_EUR = {}
USD_TO_EUR[2010] = 0.754309
USD_TO_EUR[2011] = 0.718414
USD_TO_EUR[2012] = 0.778338
USD_TO_EUR[2013] = 0.752945
USD_TO_EUR[2014] = 0.752728
USD_TO_EUR[2015] = 0.901296
USD_TO_EUR[2016] = 0.903421
USD_TO_EUR[2017] = 0.885206
USD_TO_EUR[2018] = 0.846773

GVA_GLOBAL_OBSERVED = {}
GVA_GLOBAL_OBSERVED[2010] = GVA_GLOBAL_OBSERVED_USD[2010] * USD_TO_EUR[2010]
GVA_GLOBAL_OBSERVED[2011] = GVA_GLOBAL_OBSERVED_USD[2011] * USD_TO_EUR[2011]
GVA_GLOBAL_OBSERVED[2012] = GVA_GLOBAL_OBSERVED_USD[2012] * USD_TO_EUR[2012]
GVA_GLOBAL_OBSERVED[2013] = GVA_GLOBAL_OBSERVED_USD[2013] * USD_TO_EUR[2013]
GVA_GLOBAL_OBSERVED[2014] = GVA_GLOBAL_OBSERVED_USD[2014] * USD_TO_EUR[2014]
GVA_GLOBAL_OBSERVED[2015] = GVA_GLOBAL_OBSERVED_USD[2015] * USD_TO_EUR[2015]
GVA_GLOBAL_OBSERVED[2016] = GVA_GLOBAL_OBSERVED_USD[2016] * USD_TO_EUR[2016]
GVA_GLOBAL_OBSERVED[2017] = GVA_GLOBAL_OBSERVED_USD[2017] * USD_TO_EUR[2017]
GVA_GLOBAL_OBSERVED[2018] = GVA_GLOBAL_OBSERVED_USD[2018] * USD_TO_EUR[2018]
"""
Growth rates
- Formula: N.A.
- Units: % (percentage)
- Definition: Growth rate assumptions for GVA and emissions for relevant regions/divisions
  of the world.
- Source: https://tntcat.iiasa.ac.at/SspDb/dsd?Action=htmlpage&page=welcome
"""
rates = {}
rates = {2050: {}, 2030: {}}

# International Energy Agency
rates[2050]['iea'] = {
    "world_growth": 3.2,
    "company_growth": 3.2,
    "emissions_growth": 3.2
}
# Organization for Economic Co-operation and Development
# company growth: Selected OECD countries Upper quartile 2010
# https://www.oecd.org/economy/growth/interest-rate-growth-differentials-and-government-debt-dynamics.pdf
rates[2050]['oecd'] = {
    "world_growth": 1.93,
    "company_growth": 1.53,
    "emissions_growth": 0.27
}
rates[2050]['world'] = {
    "world_growth": 1.93,
    "company_growth": 1.93,
    "emissions_growth": 0.88
}
# International Energy Agency
rates[2030]['iea'] = {
    "world_growth": 3.2,
    "company_growth": 3.2,
    "emissions_growth": 3.2
}

rates[2030]['oecd'] = {
    "world_growth": 1.64,
    "company_growth": 1.32,
    "emissions_growth": -0.08
}
rates[2030]['world'] = {
    "world_growth": 1.64,
    "company_growth": 1.64,
    "emissions_growth": 0.34
}
"""
Emissions of CH4
- Formula: N.A.
- Units: 	Mt CH4/yr
- Definition: Scaled emission arrays for CH4 emitted by the agricultural sector 
from 2010 til 2050.
- Source: SSP Public Database https://tntcat.iiasa.ac.at/SspDb 
(Tab CMIP6 Emissions > World > SSP2 4.5 > CH4  > Agriculture)
Data are only available in 10 years intervals; 
values for other years are calculated by linear interpolation.
"""
emissions_CH4 = {}
emissions_CH4[2010] = 132.143
emissions_CH4[2011] = 133.55202
emissions_CH4[2012] = 134.9609885
emissions_CH4[2013] = 136.3427473
emissions_CH4[2014] = 137.7245060
emissions_CH4[2015] = 139.2717995
emissions_CH4[2016] = 139.8633435
emissions_CH4[2017] = 140.4548875
emissions_CH4[2018] = 141.0464315
emissions_CH4[2019] = 141.6379755
emissions_CH4[2020] = 142.230
emissions_CH4[2021] = 143.417307
emissions_CH4[2022] = 144.6050944
emissions_CH4[2023] = 145.7928818
emissions_CH4[2024] = 146.9806692
emissions_CH4[2025] = 148.1684567
emissions_CH4[2026] = 149.3562441
emissions_CH4[2027] = 150.5440315
emissions_CH4[2028] = 151.7318189
emissions_CH4[2029] = 152.9196064
emissions_CH4[2030] = 154.107
emissions_CH4[2031] = 154.3918436
emissions_CH4[2032] = 154.6762934
emissions_CH4[2033] = 154.9607432
emissions_CH4[2034] = 155.245193
emissions_CH4[2035] = 155.5296427
emissions_CH4[2036] = 155.8140925
emissions_CH4[2037] = 156.0985423
emissions_CH4[2038] = 156.3829921
emissions_CH4[2039] = 156.6674419
emissions_CH4[2040] = 156.952
emissions_CH4[2041] = 156.4639294
emissions_CH4[2042] = 155.9759672
emissions_CH4[2043] = 155.4880049
emissions_CH4[2044] = 155.0000426
emissions_CH4[2045] = 154.5120804
emissions_CH4[2046] = 154.0241181
emissions_CH4[2047] = 153.5361558
emissions_CH4[2048] = 153.0481935
emissions_CH4[2049] = 152.5602313
emissions_CH4[2050] = 152.072
"""
Emissions of CH4
- Formula: N.A.
- Units: 	Mt N2/yr 
- Definition: Scaled emission arrays for N2O emitted by the agricultural sector
 from 2010 til 2050.
- Source: SSP Public Database
https://tntcat.iiasa.ac.at/SspDb (Tab CMIP6 Emissions > World > SSP2 4.5 > N2O)
The data are only available for all sectors together. The unit is kt N2O/yr; FaIR uses Mt N2/yr. 
Conversion kt N2O to Mt N2:
The molecular weight of N2O is about 44g / mol (https://www.convertunits.com/molarmass/N2O); 
the molecular weight of N is about 14g / mol (https://www.convertunits.com/molarmass/N). 
So the conversion factor from N2O to N2 is 
44/28 = 1.57 and you have to divide kt N2O by 1.57 * 1000 to get Mt N2.

Calculation of share of agriculture on N2O emissions:
Use share of N2O emissions of NACE A on N2O emissions for all sectors in EU 2016.
(https://ec.europa.eu/eurostat/web/climate-change/data/database, 
table env_ac_ainah_r2, data for EU 28 N2O emissions 2016, 
total all NACE activities and Agriculture, Fishing, Forestry)

Values for 2016 are still estimates! Current share is 82%; share used for FaIR was 78%.
"""

emissions_N2 = {}
emissions_N2[2010] = 5.201807982
emissions_N2[2011] = 5.348429907
emissions_N2[2012] = 5.410747421
emissions_N2[2013] = 5.572052901
emissions_N2[2014] = 5.675752704
emissions_N2[2015] = 5.663986364
emissions_N2[2016] = 5.457302574
emissions_N2[2017] = 5.499318524
emissions_N2[2018] = 5.541334473
emissions_N2[2019] = 5.583350423
emissions_N2[2020] = 5.625366373
emissions_N2[2021] = 5.66937916
emissions_N2[2022] = 5.713391947
emissions_N2[2023] = 5.757404734
emissions_N2[2024] = 5.801417521
emissions_N2[2025] = 5.845430308
emissions_N2[2026] = 5.889443095
emissions_N2[2027] = 5.933455882
emissions_N2[2028] = 5.977468669
emissions_N2[2029] = 6.021481456
emissions_N2[2030] = 6.065494243
emissions_N2[2031] = 6.08611288
emissions_N2[2032] = 6.106731516
emissions_N2[2033] = 6.127350153
emissions_N2[2034] = 6.14796879
emissions_N2[2035] = 6.168587426
emissions_N2[2036] = 6.189206063
emissions_N2[2037] = 6.2098247
emissions_N2[2038] = 6.230443336
emissions_N2[2039] = 6.251061973
emissions_N2[2040] = 6.27168061
emissions_N2[2041] = 6.269850928
emissions_N2[2042] = 6.268021247
emissions_N2[2043] = 6.266191565
emissions_N2[2044] = 6.264361884
emissions_N2[2045] = 6.262532202
emissions_N2[2046] = 6.26070252
emissions_N2[2047] = 6.258872839
emissions_N2[2048] = 6.257043157
emissions_N2[2049] = 6.255213476
emissions_N2[2050] = 6.253383794
"""
Assumed agricultural GVA
- Formula: N.A.
- Units: € (Euro)
- Definition: future assumed gross value added by the agricultural sector.
- Source: The World Bank
Statistics of the World Bank about Agriculture, forestry, and fishing, 
value added (current US$): https://data.worldbank.org/indicator/NV.AGR.TOTL.CD
Excel data can be obtained through this link. 
In the Excel file, select in column A country = 'World' 
and select the data columns for the years from 2010 to the last available year (02.07.2020: 2019).

For the USD/EUR conversion, the exchange rates of the World bank for 
Official exchange rate (LCU per US$, period average) was used. 
Excel data can be obtained through this link. 
In the Excel file, select in column A country = “Euro Area“ 
and select the data columns for the years from 2010 to the last available year (02.07.2020: 2019).

For years 2020-2050, the GVA development of the sector up to 2050 
is estimated using the SSP Database. We use the data on agricultural production from SSP2 - RCP4.5 
(same SSP/RCP combination as for agricultural emissions). 
The data are available at 10 year intervals, differentiated by crop for energy production, 
crop for non-energy production and livestock. 
The three categories are summed up. The unit in each case is million tons DM/year.

Since we have original GVA data until 2019 we compute the growth rate from 2019 to 2020 as follows:
Production_2019 = Production_2010 + (Production_2019 - Production_2010) * 9/10

For the next decades exponential growths is assumed.

For further information see the detailed calculation process: 
https://right-basedonscience.atlassian.net/wiki/spaces/XDCM/pages/83951627/XDC+Constants
"""
gva_agriculture = {}
gva_agriculture[2010] = 1.91962E+12
gva_agriculture[2011] = 2.1123E+12
gva_agriculture[2012] = 2.3634E+12
gva_agriculture[2013] = 2.4334E+12
gva_agriculture[2014] = 2.4868E+12
gva_agriculture[2015] = 2.8433E+12
gva_agriculture[2016] = 2.8502E+12
gva_agriculture[2017] = 2.9363E+12
gva_agriculture[2018] = 2.8651E+12
gva_agriculture[2019] = 3.1287E+12
gva_agriculture[2020] = 3.2298E+12
gva_agriculture[2021] = 3.27195E+12
gva_agriculture[2022] = 3.31465E+12
gva_agriculture[2023] = 3.35791E+12
gva_agriculture[2024] = 3.40173E+12
gva_agriculture[2025] = 3.44612E+12
gva_agriculture[2026] = 3.49109E+12
gva_agriculture[2027] = 3.53665E+12
gva_agriculture[2028] = 3.5828E+12
gva_agriculture[2029] = 3.62956E+12
gva_agriculture[2030] = 3.67693E+12
gva_agriculture[2031] = 3.7216E+12
gva_agriculture[2032] = 3.76682E+12
gva_agriculture[2033] = 3.81259E+12
gva_agriculture[2034] = 3.85891E+12
gva_agriculture[2035] = 3.90579E+12
gva_agriculture[2036] = 3.95325E+12
gva_agriculture[2037] = 4.00128E+12
gva_agriculture[2038] = 4.0499E+12
gva_agriculture[2039] = 4.0991E+12
gva_agriculture[2040] = 4.14891E+12
gva_agriculture[2041] = 4.19814E+12
gva_agriculture[2042] = 4.24795E+12
gva_agriculture[2043] = 4.29836E+12
gva_agriculture[2044] = 4.34936E+12
gva_agriculture[2045] = 4.40097E+12
gva_agriculture[2046] = 4.4532E+12
gva_agriculture[2047] = 4.50604E+12
gva_agriculture[2048] = 4.55951E+12
gva_agriculture[2049] = 4.61361E+12
gva_agriculture[2050] = 4.66835E+12
"""
Climate sensitivity 
- Formula: ∆Ts / ∆F = λ
- Units: K / (W m^-2)
- Definition: Global mean surface temperature response ∆Ts to the radiative forcing ∆F
- Source: Ramanathan, V., R. Cicerone, H. Singh, and J. Kiehl, 1985: 
Trace gas trends and their potential role in climate change. 
J. Geophys. Res., 90, 5547-5566. 
https://www.ipcc.ch/site/assets/uploads/2018/03/TAR-06.pdf (page 6/68, formula 6.1)
"""
CLIMATE_SENSITIVITY = 0.5
"""
Relation between global CO2 ppmv and CO2 amount
- Formula: 7.8 Gt CO2eq -> 1 ppmv
- Units: t (tons)
- Definition: 1 ppmv CO2 of the global atmosphere equals 2.12 GtC and 7.8 Gt CO2.
  1 Gt = 10^9t = 10^15g
- Source: R.T. Watson, H. Rodhe, H. Oeschger, U. Siegenthaler: Greenhouse Gases and Aerosols
https://archive.ipcc.ch/ipccreports/far/wg_I/ipcc_far_wg_I_chapter_01.pdf (page 9, section 1.2.1.1)
"""
CO2EQ_PPMV = 7.8E9
"""
Radiative Forcing Correction
- Formula: radiative_forcing_correction = -0.85
- Units: W m^-2
- Definition: Correction for non CO2 induced radiative forcing
  -0.85 is the sum of the 3 values for aerosol-radiation interactions (-0.45), 
  aerosol-cloud interactions (-0.45) and 
  Combined Contrails and contrail-induced cirrus (0.05). The values are the best estimates.
- Source: Climate Change 2013: The Physical Science Basis (page 696, table 8.6)
http://www.ipcc.ch/report/ar5/wg1/
https://www.ipcc.ch/site/assets/uploads/2018/02/WG1AR5_Chapter08_FINAL.pdf
"""

RADIATIVE_FORCING_CORRECTION = -0.85
"""
Radiative Forcing Factor
- Formula: α=5.35, e.g.: RF=α*ln(C/C0) 
- Units: W m^-2
- Definition: Radiative forcing factor of CO2
- Source: Gohar and Shine, Weather - Nov 2007, Vol. 62, No. 11, eq. 2
https://rmets.onlinelibrary.wiley.com/doi/epdf/10.1002/wea.103 (page 308)
"""
RADIATIVE_FORCING_FACTOR = 5.35
"""
Constants for converting from mass in CO2 to C and for converting N2O and CH4
to CO2 equivalents.
Global warming potentials were taken as quoted by Allen et al 2018
(https://doi.org/10.1038/s41612-018-0026-8) from the IPCC
(https://doi.org/10.1017/CBO9781107415324.018) 
"""
c_to_co2_ratio = 12 / 44
n2_to_n2o_ratio = 28 / 44
gwp_n2o = 265
gwp100_ch4 = 28
gwp20_ch4 = 84
"""
Pre-industrial concentrations of all gases considered in
FaIR, with gases other than CO2, CH4 and N2O set to zero.
The values were suggested by one of the core FaIR developers:
https://github.com/OMS-NetZero/FAIR/issues/28.
(This is because all the forcings are calculated relative to
the pre-industrial state. For minor gases the absolute
concentrations are not very important, and only the change is
considered in FaIR.)
"""
pi_concentrations = np.zeros(31)
pi_concentrations[0:3] = [278, 721.89409166, 272.96017722]
"""
Natural methane and N2O emissions for use in FaIR
The values follow the suggestion of one of the core FaIR 
developers: https://github.com/OMS-NetZero/FAIR/issues/28.
"""
natural_emissions = np.array([209.2492, 11.1555])  # CH4 and N2O
"""
Transient climate response and equilibrium climate sensitivity
for use in FaIR.
These historically constrained values are those given in
Smith et. al. 2018, as best fitting to observed temperatures
between 1880 and 2016.
"""
historically_constrained_tcr_and_ecs = np.array([1.53, 2.86])
