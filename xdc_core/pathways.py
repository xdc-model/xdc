import numpy as np

from xdc_core.xdc import XDC
import xdc_core.scope as scope
import xdc_core.emission as emission
import xdc_core.growth as growth
import xdc_core.xdc_helper.economic_adjustments_helper as eh
from xdc_core.country_code import CountryCode


# TODO: this was the old company pathway calculation (before FaIR):
# maybe we can get rid of it
def calculate_company_scope_pathway_curve(
    sector_growth_obj,
    company_base_year_eei: float,
    sector_base_year_eei: float,
    **kwargs,
):
    """The function calculates the growth curve the company must
    follow from base year up to the target year to have the same
    over-all EEI.

    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param company_base_year_eei: The EEI of the company in the base_year
    :type company_base_year_eei: float    
    :param sector_base_year_eei: The EEI of the sector in the base_year
    :type sector_base_year_eei: float
    :return: emission growth curve for the company
    :rtype: np.array
    """
    base_year = sector_growth_obj.base_year
    target_year = sector_growth_obj.target_year
    sector_eei_scope_curve = sector_growth_obj.emissions_growth

    time_frame = target_year - base_year + 1
    # Sum(Eei_company_scope1_curve) = A*sum_t+ timeframe *
    # company_scope1_eei
    # ((Sum(Eei_company_scope1_curve) - time_frame)/ company_Scope1_eei)
    # = A

    t = np.array(range(0, time_frame))
    sum_t = np.sum(t)

    cumulative_scope_eei = sum(sector_eei_scope_curve)

    company_scope_eei_base_year = company_base_year_eei / sector_base_year_eei

    curve_slope_scope = (cumulative_scope_eei -
                         time_frame * company_scope_eei_base_year) / sum_t

    scope_pathways = curve_slope_scope * t + company_scope_eei_base_year
    scope_pathways /= company_scope_eei_base_year

    return scope_pathways


def _get_company_curve_from_sector_curve(sector_eei_curve, base_eei,
                                         target_eei):
    """The function calculates the company's eei growth curve from the
    start and end points (base year and target year eei) and the shape
    of the sector's eei growth curve.

    :param sector_eei_curve: sector eei curve
    :type sector_eei_curve: np.array
    :param base_eei: EEI of the company in the base year
        divided by EEI of the sector in the base year
    :type base_eei: float
    :param target_eei: EEI of the company in the end year
        divided by EEI of the sector in the base year
    :type target_eei: float
    :return: eei growth curve for the company
    :rtype: np.array
    """
    if sector_eei_curve[-1] != sector_eei_curve[0]:
        # we can use the information about the shape of the sector's EEI curve,
        # to get the shape of the company's EEI pathway
        company_eei_curve = base_eei + (
            sector_eei_curve - sector_eei_curve[0]) * (
                (target_eei - base_eei) /
                (sector_eei_curve[-1] - sector_eei_curve[0]))
    else:  # if there is no change in emissions between the base and target
        # year of the sector: no information on the shape of the emissions
        # pathway, so we assume a straight line in emissions to the target
        years = len(sector_eei_curve)
        time_fraction = np.asarray(range(years)) / (years - 1)
        company_eei_curve = base_eei + time_fraction * (target_eei - base_eei)
    company_eei_curve /= base_eei
    return company_eei_curve


def eei_target_pathway(sector_growth_obj,
                       company_country: CountryCode,
                       company_base_year_eei: float,
                       sector_base_year_eei: float,
                       location_shares: list = [1 / 3] * 3,
                       ssp_scenario: str = 'ssp2',
                       **kwargs):
    """The function calculates the emissions growth curve the
    company must follow from base year up to the target year to
    reach the target EEI.

    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param company_country: country of the company
    :type company_country: CountryCode
    :param company_base_year_eei: The company's EEI in the base_year in PPP USD
    :type company_base_year_eei: float
    :param sector_base_year_eei: The sector's EEI in the base year in PPP USD
    :type: sector_base_year_eei: float
    :param location_shares: internationalization shares
    :type location_shares: list
    :param ssp_scenario: shared socioeconomic pathway, e.g. "ssp2"
    :type ssp_scenario: str
    :return: emission growth curve for the company
    :rtype: np.array
    """
    base_year = sector_growth_obj.base_year
    target_year = sector_growth_obj.target_year
    sector_eei_curve = sector_growth_obj.get_eei_curve()
    target_eei = sector_eei_curve[-1]
    base_eei = company_base_year_eei / sector_base_year_eei

    company_gva_curve = growth.Growth.get_baseline_amount_curve(
        base_year, target_year, company_country, ssp_scenario, location_shares)

    company_eei_curve = _get_company_curve_from_sector_curve(
        sector_eei_curve, base_eei, target_eei)
    company_e_curve = company_eei_curve * company_gva_curve

    return company_e_curve


def xdc_from_end_year_eei(target_eei,
                          base_eei,
                          sector_base_year_eei,
                          sector_growth_obj,
                          ssp_scenario,
                          emissions_to_xdc=XDC.emissions_to_legacy_xdc):
    """Calculate the XDC value given the target year EEI.

    :param target_eei: EEI of the company in the end year
        divided by EEI of the sector in the base year
        and multiplied by the company's end year GVA
        divided by the company's base year GVA
    :type target_eei: float
    :param base_eei: EEI of the company in the base year
        divided by EEI of the sector in the base year
    :type base_eei: float
    :param sector_base_year_eei: EEI of the sector in the base year
    :type sector_base_year_eei: float
    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param ssp_scenario: shared socioeconomic pathway, e.g. "ssp2"
    :type ssp_scenario: str
    :param emissions_to_xdc: method of calculating the xdc value
    :type emissions_to_xdc: Callable

    :return: absolute difference between company and target xdc values
    :rtype: float
    """
    base_year = sector_growth_obj.base_year
    target_year = sector_growth_obj.target_year
    sector_eei_curve = sector_growth_obj.get_eei_curve()
    eei_curve = _get_company_curve_from_sector_curve(sector_eei_curve,
                                                     base_eei, target_eei)

    company_scope_obj = scope.Scope(sector_base_year_eei * base_eei, eei_curve)
    sector_scope_obj = scope.Scope(sector_base_year_eei, sector_eei_curve)
    emissions = emission.Emission(base_year, target_year)
    emissions_sector = emission.Emission(base_year, target_year)
    emissions.add_scope(company_scope_obj, 'emission estimate')
    emissions_sector.add_scope(sector_scope_obj, 'sector')
    # upscale to world with different assumptions about world gva growth
    # for the sector and the company
    world_amount_curve = growth.Growth.get_world_growth_curve(
        base_year, target_year, ssp_scenario, 'baseline')
    world_amount_curve_for_sector = growth.Growth.get_world_growth_curve(
        base_year, target_year, 'ssp2', 'rcp26')
    upscaled_emissions_dict = emissions.get_scaled_emission(
        eh.get_adjusted_world_gva(base_year),
        world_amount_curve,
        total_scope=False)
    upscaled_emissions_dict.update(
        emissions_sector.get_scaled_emission(
            eh.get_adjusted_world_gva(base_year),
            world_amount_curve_for_sector,
            total_scope=False))
    xdc_dict = emissions_to_xdc(upscaled_emissions_dict, base_year,
                                target_year)

    return xdc_dict['emission estimate'] - xdc_dict['sector']


def _minimize(base_eei,
              sector_base_year_eei,
              sector_growth_obj,
              ssp_scenario,
              emissions_to_xdc,
              tol=10**-8,
              max_iter=50):
    """Find the end year EEI that produces the target XDC.
    Note that it is not always possible to do this, and in those
    cases an error will be raised.
    Also note that both the update loop to calculate the starting
    value for end_eei_minus and the _update function used
    to update either end_eei_minus or end_eei_plus
    are tailored to sectors with emission mitigation pathways
    that are approximately linear.

    :param base_eei: EEI in the base year (normed by sector EEI)
    :type base_eei: float
    :param sector_base_year_eei: EEI of the sector in the base year
    :type sector_base_year_eei: float
    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param emissions_to_xdc: method of calculating the xdc value
    :type emissions_to_xdc: Callable
    :param tol: how accurate should the XDC be (absolute diff)
    :type tol: float
    :param max_iter: how many iterations can be performed before
        giving up
    :type max_iter: int

    :raises RuntimeError: if a pathway cannot be found in the
        number of specified iterations, or negative emissions
        get too large

    :return: EEI in the end year
    :rtype: float
    """
    xdc_0a = xdc_from_end_year_eei(1, base_eei, sector_base_year_eei,
                                   sector_growth_obj, ssp_scenario,
                                   emissions_to_xdc)
    counter = 0
    if xdc_0a < 0:
        end_eei_minus = 1
        xdc_minus = xdc_0a
        end_eei_0b = 2 / base_eei
        xdc_plus = xdc_from_end_year_eei(end_eei_0b, base_eei,
                                         sector_base_year_eei,
                                         sector_growth_obj, ssp_scenario,
                                         emissions_to_xdc)
        while xdc_plus < 0:
            counter += 1
            if counter > max_iter:
                raise RuntimeError(
                    f'No pathway found after {counter} iterations')
            end_eei_0b *= 2
            xdc_plus = xdc_from_end_year_eei(end_eei_0b, base_eei,
                                             sector_base_year_eei,
                                             sector_growth_obj, ssp_scenario,
                                             emissions_to_xdc)
        end_eei_plus = end_eei_0b
    else:
        end_eei_plus = 1
        xdc_plus = xdc_0a
        end_eei_0b = 0
        xdc_minus = xdc_from_end_year_eei(end_eei_0b, base_eei,
                                          sector_base_year_eei,
                                          sector_growth_obj, ssp_scenario,
                                          emissions_to_xdc)
        failing = False
        while xdc_minus > 0:
            counter += 1
            if counter > max_iter:
                raise RuntimeError(
                    f'No pathway found after {counter} iterations')
            if not failing:
                end_eei_0b_passing = end_eei_0b
                if counter >= 1:
                    end_eei_0b -= 0.5 * base_eei
            failing = False
            try:
                xdc_minus = xdc_from_end_year_eei(end_eei_0b, base_eei,
                                                  sector_base_year_eei,
                                                  sector_growth_obj,
                                                  ssp_scenario,
                                                  emissions_to_xdc)
            except RuntimeError:
                end_eei_0b = 0.5 * (end_eei_0b + end_eei_0b_passing)
                failing = True
        end_eei_minus = end_eei_0b
    while xdc_minus < -tol and xdc_plus > tol:
        xdc_minus, xdc_plus, end_eei_minus, end_eei_plus = _update(
            xdc_minus, xdc_plus, end_eei_minus, end_eei_plus, base_eei,
            sector_base_year_eei, sector_growth_obj, ssp_scenario,
            emissions_to_xdc)
        counter += 1
        if counter > max_iter:
            raise RuntimeError(f'No pathway found after {counter} iterations')

    end_eei = np.asarray([end_eei_minus, end_eei_plus
                          ])[np.argmin([np.abs(xdc_minus), xdc_plus])]

    return end_eei


def _update(xdc_minus, xdc_plus, end_eei_minus, end_eei_plus, base_eei,
            sector_base_year_eei, sector_growth_obj, ssp_scenario,
            emissions_to_xdc):
    """Update the end year EEI towards the value that produces
    the target XDC.

    :param xdc_minus: Difference in XDC value when using
        end_emissions_minus compared to the target
    :type xdc_minus: float
    :param xdc_plus: Difference in XDC value when using
        end_emissions_plus compared to the target
    :type xdc_plus: float
    :param end_eei_minus: End EEI that is too small or too negative
    :type end_eei_minus: float
    :param end_eei_plus: End EEI that are too large (positive)
    :type end_eei_plus: float
    :param base_eei: EEI in the base year (normed by sector EEI)
    :type base_eei: float
    :param sector_base_year_eei: EEI of the sector in the base year
    :type sector_base_year_eei: float
    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param emissions_to_xdc: method of calculating the xdc value
    :type emissions_to_xdc: Callable

    :return: Same or updated value for xdc_minus
    :rtype: float
    :return: Same or updated value for xdc_plus
    :rtype: float
    :return: Same or updated value for end_eei_minus
    :rtype: float
    :return: Same or updated value for end_eei_plus
    :rtype: float
    """
    sector_eei = sector_growth_obj.get_eei_curve()
    eei_minus_curve = _get_company_curve_from_sector_curve(
        sector_eei, base_eei, end_eei_minus)
    eei_plus_curve = _get_company_curve_from_sector_curve(
        sector_eei, base_eei, end_eei_plus)
    world_economic_growth = growth.Growth.get_world_growth_curve(
        sector_growth_obj.base_year, sector_growth_obj.target_year,
        ssp_scenario, 'baseline')
    up_minus = np.sum(world_economic_growth * eei_minus_curve) * base_eei
    up_plus = np.sum(world_economic_growth * eei_plus_curve) * base_eei
    up_new = up_minus - xdc_minus * (up_plus - up_minus) / (xdc_plus -
                                                            xdc_minus)
    end_eei_new = base_eei + (
        up_new - base_eei * np.sum(world_economic_growth)) / np.sum(
            world_economic_growth *
            (sector_eei - sector_eei[0])) * (sector_eei[-1] - sector_eei[0])

    xdc_new = xdc_from_end_year_eei(end_eei_new, base_eei,
                                    sector_base_year_eei, sector_growth_obj,
                                    ssp_scenario, emissions_to_xdc)

    if xdc_new > 0:
        xdc_plus = xdc_new
        end_eei_plus = end_eei_new
    else:
        xdc_minus = xdc_new
        end_eei_minus = end_eei_new

    return xdc_minus, xdc_plus, end_eei_minus, end_eei_plus


def xdc_target_pathway(sector_growth_obj,
                       company_country: CountryCode,
                       company_base_year_eei: float,
                       sector_base_year_eei: float,
                       location_shares: list = [1 / 3] * 3,
                       ssp_scenario: str = 'ssp2',
                       emissions_to_xdc=XDC.emissions_to_legacy_xdc,
                       **kwargs):
    """Calculate the emission pathway of a company that leads to the same
    warming as the sector in the target year.

    :param sector_growth_obj: growth object for the sector
    :type sector_growth_obj: Growth
    :param company_country: country of the company
    :type company_country: CountryCode
    :param company_base_year_eei: EEI of the company in the base year in CO2eq per PPP$
    :type company_base_year_eei: float
    :param sector_base_year_eei: EEI of the sector in the base year in CO2eq per PPP$
    :type sector_base_year_eei: float
    :param location_shares: internationalization shares
    :type location_shares: list
    :param ssp_scenario: shared socioeconomic pathway, e.g. "ssp2"
    :type ssp_scenario: str
    :param emissions_to_xdc: method of calculating the xdc value
    :type emissions_to_xdc: Callable
    :return: emission growth curve for the company
    :rtype: np.array
    """
    base_year = sector_growth_obj.base_year
    target_year = sector_growth_obj.target_year

    base_eei = company_base_year_eei / sector_base_year_eei
    target_eei = _minimize(base_eei, sector_base_year_eei, sector_growth_obj,
                           ssp_scenario, emissions_to_xdc)

    sector_eei_curve = sector_growth_obj.get_eei_curve()
    company_eei_curve = _get_company_curve_from_sector_curve(
        sector_eei_curve, base_eei, target_eei)
    total_gva_growth = growth.Growth.get_baseline_amount_curve(
        base_year, target_year, company_country, ssp_scenario, location_shares)
    company_e_curve = company_eei_curve * total_gva_growth
    return company_e_curve
