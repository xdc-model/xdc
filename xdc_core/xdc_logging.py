import os
import yaml
import logging.config
import logging
import time

import xdc_core


def enable_logging(tool_name: str = None):
    """Enable the custom xdc logging functionality. If provided as argument, 
    the tool name can be used for the log file.

    :param tool_name: Name of the tool, defaults to None
    :type tool_name: str, optional
    """
    if 2 == len(logging.getLogger().handlers):
        return  # Already configured!
    path = locate_config_file()
    default_level = logging.INFO
    env_key = "LOG_CFG"
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        report_folder = os.path.join(os.path.dirname(path), 'Reports')
        if not os.path.exists(report_folder):
            logging.warning("Reports folder doesn't exist:%s", report_folder)
            os.mkdir(report_folder)
        with open(path, "rt") as f:
            try:
                config = yaml.safe_load(f.read())
                if tool_name is not None:
                    config['handlers']['catchall_file_handler'][
                        'filename'] = 'Reports/%s_%s.log' % (
                            tool_name, str(round(time.time())))
                logging.config.dictConfig(config)
            except Exception as e:
                logging.basicConfig(level=default_level)
                logging.warning(
                    "Error in Logging Configuration. Using default. Error: %s",
                    e)
    else:
        logging.basicConfig(level=default_level)
        logging.warning(
            "Failed to load configuration file. Using default configs")

    logging.info("Repo-Status: %s", xdc_core.xdc_helper.helper.get_git_info())


def locate_config_file() -> str:
    """Locate the configuration file. Search upwards until the config file is 
    found or the rood directory is reached.

    :return: Path to the config file
    :rtype: str
    """
    file_name = "logging.yaml"
    cur_dir = os.path.dirname(__file__)
    while True:
        file_list = os.listdir(cur_dir)
        parent_dir = os.path.dirname(cur_dir)
        if file_name in file_list:
            #print("File Exists in: ", cur_dir)
            break
        else:
            if cur_dir == parent_dir:  #if dir is root dir
                #print("File not found")
                break
            else:
                cur_dir = parent_dir
    return os.path.join(cur_dir, file_name)


def log_level_handler(lev=logging.INFO, hand="file"):
    """Modifies log handler level

    :param lev: log level, defaults to logging.INFO
    :param hand: log handler name, defaults to "file"
    """
    for fh in logging.getLogger().handlers[:]:
        if fh.get_name() is not None and hand in fh.get_name():
            fh.level = lev


def log_level_logger(lev=logging.DEBUG):
    """Set global logging level
    
    :param lev: log level, defaults to logging.DEBUG
    """
    logging.getLogger().setLevel(lev)


def log_level_by_test_args(sys_args):
    if "-v" in sys_args:
        log_level_handler(logging.ERROR, "console")
        logging.error("Setting log level to ERROR (-v).")
    elif "-vv" in sys_args:
        log_level_handler(logging.WARNING, "console")
        logging.warning("Setting log level to WARNING (-vv).")
    elif "-vvv" in sys_args:
        log_level_handler(logging.INFO, "console")
        logging.info("Setting log level to INFO (-vvv).")
    elif "-vvvv" in sys_args:
        log_level_handler(logging.DEBUG, "console")
    elif "-vvvvv" in sys_args:
        log_level_handler(logging.NOTSET, "console")
    else:
        log_level_handler(logging.CRITICAL, "console")
