#!/usr/bin/python3
# -*- coding: utf-8 -*-

from __future__ import annotations

import logging
import uuid
from copy import deepcopy
import numpy as np
import pandas as pd
from scipy.interpolate import interp1d

import xdc_core.xdc_constants as consts
import xdc_core.xdc_helper.data_api_helper as DataApiHelper
import xdc_core.xdc_helper.economic_adjustments_helper as EconomicAdjustmentsHelper
from xdc_core.country_code import CountryCode
from xdc_core.region import Region
from xdc_core.scenario_code import ScenarioCode
from xdc_core.sovereign_bonds import SovereignBondsReader
from xdc_core.xdc_error import XdcError, ErrorCodes
from xdc_core.xdc_helper.helper import get_grown_array_from_value
from xdc_data.scenario_budgets import BUDGET_DATA_SOURCES, BudgetSelector
from xdc_core import pathways
from xdc_core.xdc import XDC
from xdc_core.xdc_helper.fair_helper import n2o_to_co2_equivalent, ch4_to_co2_equivalent

import sys

logging.getLogger(__name__)


class Growth:
    """
    Class that generates the different growth curves. All curves should
    be normalized to 1 in the base_year and grow from there. Ideally it
    should be general and apply in the same way to countries, people,
    companies and real_estate

    Do we want the class to return list of growth rates or do we want
    to define this is an object and pass that along?
    """

    sb_cache = {}

    def __init__(self, base_year: int, target_year: int):
        """Constructor for the Growth object. Should set-up the lists
        for entity growth, world growth and emissions growth, as well
        as create a UUID.

        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        """
        self.validate_time_period(
            base_year=base_year,
            target_year=target_year,
            base_year_range=(2010, 2018),
            target_year_range=(2030, 2050),
        )
        self.base_year = base_year
        self.target_year = target_year
        self.entity_growth = None
        self.emissions_growth = None
        self.obj_uuid = uuid.uuid4()

    def __hash__(self):
        """Creates a hash of a growth-object. Useful when using the object
        as a key, eg in a cache

        :return: Hash of the object
        :rtype: hash
        """
        return hash(str(self))

    def __eq__(self, other):
        """Compares the object to other to check if they are identical.

        :param other: Growth object being compared
        :type other: Growth
        :return: Whether the two objects are identical
        :rtype: bool
        """
        return (self.base_year == other.base_year
                and self.target_year == other.target_year
                and np.array_equal(self.entity_growth, other.entity_growth) and
                np.array_equal(self.emissions_growth, other.emissions_growth))

    def __str__(self):
        """Returns a printable version of the growth object

        :return: String with the growth paramters
        :rtype: str
        """
        return str(self.__dict__)

    @classmethod
    # @lru_cache
    def from_baseline_assumptions(cls,
                                  base_year: int,
                                  target_year: int,
                                  country: CountryCode,
                                  ssp_scenario: str = "ssp2",
                                  gas: str = 'kyotoGases'):
        """Class method returning three different growth object using the
        following the country, the region and a global growth assumption.

        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :param country: the country in which the company has its headquarter
        :type region: str
        :param ssp_scenario: SSP Scenario used to retrieve data from the data base
        :type ssp_scenario: str
        :param gas: specifies gas to be retrived from the data base
        :type gas: str, can be ["kyotoGases", "N2O", "CH4", "CO2"]

        :return: a growth object with baseline amount and emission
            growth rates
        :rtype: Growth
        """
        growth_obj_country = cls(base_year=base_year, target_year=target_year)
        growth_obj_region = cls(base_year=base_year, target_year=target_year)
        growth_obj_global = cls(base_year=base_year, target_year=target_year)
        region = DataApiHelper.get_region_by_country(country)
        # Retrieve yearly GDP for country and extract normalized growth rate
        country_yearly_gdp = DataApiHelper.get_country_gdp(
            country, ssp_scenario)
        # If there is no GDP for the country, use the region's GDP instead
        if len(country_yearly_gdp) == 0:
            region_yearly_gdp = DataApiHelper.get_region_gdp(
                region, ssp_scenario, "Baseline")
            growth_obj_country.entity_growth = ScenarioReader.\
                get_normalized_curve_from_gdp(region_yearly_gdp, base_year,
                                              target_year)
        else:
            growth_obj_country.entity_growth = ScenarioReader.\
                get_normalized_curve_from_gdp(country_yearly_gdp, base_year,
                                              target_year, country=True)
        # Retrieve yearly GDP for region and extract normalized growth rate
        yearly_gdp_region = DataApiHelper.get_region_gdp(
            region, ssp_scenario, "Baseline")
        growth_obj_region.entity_growth = ScenarioReader.\
            get_normalized_curve_from_gdp(yearly_gdp_region, base_year,
                                          target_year)
        # Retrieve yearly global GDP and extract normalized growth rate
        yearly_gdp_global = DataApiHelper.get_global_gdp(
            ssp_scenario, "Baseline")
        growth_obj_global.entity_growth = ScenarioReader.\
            get_normalized_curve_from_gdp(yearly_gdp_global, base_year,
                                          target_year)
        # Retrieve yearly emissions for country and region and extract
        # normalized growth rate
        yearly_emissions_region = DataApiHelper.get_region_emissions(
            region, ssp_scenario, "Baseline")
        yearly_emissions_region_normalized_growth = ScenarioReader.\
            get_normalized_curve_from_emissions(yearly_emissions_region,
                                                base_year,
                                                target_year,
                                                gas)

        growth_obj_country.emissions_growth = \
            yearly_emissions_region_normalized_growth
        growth_obj_region.emissions_growth = \
            yearly_emissions_region_normalized_growth
        # Retrieve yearly global emissions and extract normalized growth rate
        yearly_emissions_global = DataApiHelper.get_global_emissions(
            ssp_scenario, "Baseline")
        growth_obj_global.emissions_growth = ScenarioReader.\
            get_normalized_curve_from_emissions(yearly_emissions_global,
                                                base_year,
                                                target_year,
                                                gas)
        logging.debug("Initialized country growth object with: %s",
                      growth_obj_country)
        logging.debug("Initialized region growth object with: %s",
                      growth_obj_region)
        logging.debug("Initialized global growth object with: %s",
                      growth_obj_global)

        return growth_obj_country, growth_obj_region, growth_obj_global

    @classmethod
    def from_scenario(
        cls,
        base_year: int,
        target_year: int,
        entity_growth: np.ndarray,
        emissions_growth: np.ndarray,
    ):
        """Class method returning a growth object with the given arrays as 
        entity growth and emissions growth.

        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :param entity_growth: Array of growth rates per year for the
                                  entity's amount
        :type entity_growth: np.ndarray
        :param emissions_growth: Array of growth rates per year for
                                      the entity's emission
        :type emissions_growth: np.ndarray

        :return: a growth object with amount and emission growth
            specified
        :rtype: Growth
        """
        growth_obj = cls(base_year=base_year, target_year=target_year)
        growth_obj.validate_array_length(entity_growth)
        growth_obj.entity_growth = entity_growth
        growth_obj.validate_array_length(emissions_growth)
        growth_obj.emissions_growth = emissions_growth
        logging.debug("Initialized growth object with: %s", growth_obj)

        return growth_obj

    @classmethod
    def from_growth_rate(
        cls,
        base_year: int,
        target_year: int,
        entity_growth: float,
        emissions_growth: float,
    ):
        """Class method returning a growth object that grows continuously by 
        the specified amount (this is a percentage) every year

        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :param entity_growth: growth rates per year for the
                                  entity's amount
        :type entity_growth: float
        :param emissions_growth: growth rate per year for
                                      the entity's emission
        :type emissions_growth: float

        :return: a growth object with amount and emission growth
            specified
        :rtype: Growth
        """
        growth_obj = cls(base_year=base_year, target_year=target_year)
        number_of_years = growth_obj.target_year - growth_obj.base_year
        growth_obj.entity_growth = get_grown_array_from_value(
            number_of_years, 1, entity_growth)
        growth_obj.emissions_growth = get_grown_array_from_value(
            number_of_years, 1, emissions_growth)
        logging.debug("Initialized growth object with: %s", growth_obj)

        return growth_obj

    @classmethod
    def from_world_assumptions(cls,
                               base_year: int,
                               target_year: int = 2050,
                               ssp_selector: str = 'ssp2',
                               forcing: str = 'baseline',
                               gas: str = "kyotoGases"):
        growth_obj_global = cls(base_year=base_year, target_year=target_year)
        # Retrieve yearly global GDP and extract normalized growth rate
        yearly_gdp_global = DataApiHelper.get_global_gdp(ssp_selector, forcing)

        growth_obj_global.entity_growth = ScenarioReader.\
            get_normalized_curve_from_gdp(yearly_gdp_global, base_year,
                                          target_year)
        # Retrieve yearly emissions for country and region and extract
        # Retrieve yearly global emissions and extract normalized growth rate
        yearly_emissions_global = DataApiHelper.get_global_emissions(
            ssp_selector, forcing)
        growth_obj_global.emissions_growth = ScenarioReader.\
            get_normalized_curve_from_emissions(yearly_emissions_global,
                                                base_year, target_year, gas)
        return growth_obj_global

    @classmethod
    def _verify_selectors(cls, selectors: list):
        """Turns selector strings into BudgetSelector ENUMs.

        :param selectors: [description]
        :type selectors: list
        :return: [description]
        :rtype: [type]
        """
        budget_selectors = []
        for selector in selectors:
            selector = BudgetSelector.value_of(selector)
            assert (
                selector is not None
            ), "Unknown budget selector %s, selector must be in the following list %s" % (
                selector, [b.scenario_selector for b in BudgetSelector])
            budget_selectors.append(selector)
        return budget_selectors

    @classmethod
    # lru cache seems to deliver inconsistent results see XDC-1141
    # activating this will make the portfolio sector target xdc
    # endpoint fail.
    # @lru_cache
    def from_predefined_scenario(cls,
                                 selector: str,
                                 base_year: int,
                                 target_year: int,
                                 scenario: ScenarioCode,
                                 subtractors: list = [],
                                 ssp_selector: str = 'ssp2'):
        """
        Class method returning the growth object for a given budget. The budget
        is selected with the parameters below.

        :param selector:  IEA sector or gas present in the scenario_budgets,
                            eg transport or industry, CH4 or Kyoto
        :type selector: str
        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :param scenario:The scenario to be picked for the
                        reduction pathway, eg 2DS or B2DS
        :type scenario: ScenarioCode
        :param region: The region in which the company is located,
                       which will pick the corresponding reduction
                       pathway.
        :type region: Region
        :raises AssertionError: Raises error if region is not in Region
        :raises AssertionError: Raises error if scenario is not a ScenarioCode
        :raises AssertionError: Raises error if iea sector is unknown
        :return: a growth object with baseline amount assumptions
            and emissions based on the IEA budget
        :rtype: Growth
        """
        #ssp_selector = "ssp2"
        #assert type(region) == Region, "region should be a Region object"
        # Verify selectors and subtractors
        selector = cls._verify_selectors([selector])[0]
        subtractors = cls._verify_selectors(subtractors)

        # Verify scenario
        assert type(
            scenario) == ScenarioCode, "scenario must be a valid ScenarioCode"

        # Do this more dynamically, eg by checking first not-none entry in
        # the corresponding scenario budget
        if scenario == ScenarioCode.RCP26:
            min_base_year = 2015
        elif scenario == ScenarioCode.BTDS:
            min_base_year = 2014
        else:
            min_base_year = 2013
        cls.validate_time_period(base_year, target_year, (min_base_year, 2018),
                                 (2030, 2050))

        # create an object from baseline assumptions, so we have baseline
        # GVA growth
        if ssp_selector == 'ssp3':
            forcing = 'baseline'
        else:
            forcing = 'rcp26'
        growth_obj = cls.from_world_assumptions(base_year=base_year,
                                                target_year=target_year,
                                                ssp_selector=ssp_selector,
                                                forcing=forcing)

        # TODO: remove this line to correct GVA growth assumptions
        # It's here for now to not change the results
        #growth_obj.entity_growth = growth_obj._get_sector_growth_curves(
        #    "world")

        # overwrite emissions_growth according to IEA budget
        growth_obj.emissions_growth = ScenarioReader.normalized_curve_of(
            region_code=Region.WORLD,
            scenario_code=scenario,
            budget_selector=selector,
            budget_subtractors=subtractors,
            base_year=base_year,
            target_year=target_year,
        )
        logging.debug("Initialized growth object with: %s", growth_obj)

        return growth_obj

    @classmethod
    def create_entity_pathway_from_sector_path(
        cls,
        sector_growth: Growth,
        company_country: CountryCode,
        company_base_year_eei: float,
        sector_base_year_eei: float,
        location_shares: list = [1 / 3] * 3,
        ssp_scenario: str = "ssp2",
        pathway=pathways.calculate_company_scope_pathway_curve,
        emissions_to_xdc=None,
    ):
        """Class method returning a growth object with the growth of an entity
        needs to adhere to, to emit the same amount of emissions as the one
        specified by the sector_growth based on the ration of the company to
        the sector in the base_year

        :param sector_growth: reduction pathway of the sector
        :type sector_growth: Growth
        :param company_country: county assigned to the company
        :type: CountryCode
        :param company_base_year_eei: EEI of the company in the base year
        :type company_base_year_eei: float
        :param sector_base_year_eei: EEI of the sector in the base year
        :type sector_base_year_eei: float
        :param pathway: type of pathway to be calculated
            (choose between calculate_company_scope_pathway_curve,
            eei_target_pathway and xdc_target_pathway)
        :type pathway: Callable
        :param emissions_to_xdc: method of calculating the XDC value,
            only used if pathway=pathways.xdc_target_pathway
        :type emissions_to_xdc: Callable
        :raises AssertionError: If sector_growth is not a growth object
        :raises AssertionError: If sector_growth has no region
        :raises AssertionError: If sector_growth has no emission_growth
        :raises AssertionError: If xdc_target_pathway is being used, but
            no emissions_to_xdc method has been specified
        :raises ValueError: If xdc_target_pathway is being used, but
            no value has been input for sector_base_year_eei
        :return: a growth object with the emission reduction pathway
            based on that of the sector, but multiplied by the ratio
            provided
        :rtype: Growth
        """

        assert type(sector_growth) == Growth, ("The sector_growth needs to " +
                                               "be a growth object")

        assert sector_growth.emissions_growth is not None, (
            "The sector " + "object must contain emissions_growth")
        pathway_choices = [
            pathways.calculate_company_scope_pathway_curve,
            pathways.xdc_target_pathway, pathways.eei_target_pathway
        ]
        assert pathway in pathway_choices, (f"The pathway must be one of "
                                            f"{pathway_choices}")
        emissions_to_xdc_choices = [
            XDC.emissions_to_legacy_xdc, XDC.emissions_to_converted_fair_xdc,
            XDC.emissions_to_fair_xdc
        ]
        if pathway == pathways.xdc_target_pathway:
            assert emissions_to_xdc in emissions_to_xdc_choices, (
                "If using XDC target pathway, you need to specify the XDC"
                f"calculation method. "
                f"It must be one of {emissions_to_xdc_choices}")
        # print(sector_base_year_eei)
        if not isinstance(sector_base_year_eei,
                          (float, int)) or sector_base_year_eei == 0:
            raise ValueError(
                "If using XDC target pathway, you must specify the sector"
                "base year EEI (float) as not 0")
        if not location_shares:
            location_shares = [1 / 3, 1 / 3, 1 / 3]

        amount_growth = Growth.get_baseline_amount_curve(
            sector_growth.base_year, sector_growth.target_year,
            company_country, ssp_scenario, location_shares)

        try:
            emissions_growth = pathway(
                sector_growth,
                company_country=company_country,
                company_base_year_eei=company_base_year_eei,
                sector_base_year_eei=sector_base_year_eei,
                location_shares=location_shares,
                ssp_scenario=ssp_scenario,
                emissions_to_xdc=emissions_to_xdc,
            )
        except RuntimeError:
            logging.warning('Could not compute company target pathway')
            return np.nan
        growth_obj = Growth(sector_growth.base_year, sector_growth.target_year)
        growth_obj.entity_growth = amount_growth
        growth_obj.emissions_growth = emissions_growth
        logging.debug("Initialized growth object with: %s", growth_obj)
        return growth_obj

    @classmethod
    def create_entity_pathway_from_predefined_scenario(
        cls,
        selector: str,
        base_year: int,
        target_year: int,
        scenario: ScenarioCode,
        company_country: CountryCode,
        company_base_year_eei: float,
        sector_base_year_eei: float,
        location_shares: list,
        ssp_scenario: str = "ssp2",
        pathway=pathways.calculate_company_scope_pathway_curve,
        emissions_to_xdc=XDC.emissions_to_converted_fair_xdc,
    ):
        """Same as set_entity_growth_from_sector_path, with the
        difference that the sector path is calculated on the fly, using
        the selector, base_year, scenario and region.

        :param selector:  IEA sector present in the sector_budgets,
                            eg transport or industry
        :type selector: str
        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :param scenario:The scenario to be picked for the
                        reduction pathway, eg 2DS or B2DS
        :type scenario: ScenarioCode
        :param region: The region in which the company is located,
                       which will pick the corresponding reduction
                       pathway.
        :type region: Region
        :param company_base_year_eei: EEI of the company in the base year
        :type company_base_year_eei: float
        :param sector_base_year_eei: EEI of the sector in the base year
        :type sector_base_year_eei: float
        :param pathway: type of pathway to be calculated
        :type pathway: Callable
        :param emissions_to_xdc: method of calculating the XDC value,
            only used if pathway=pathways.xdc_target_pathway
        :type emissions_to_xdc: Callable

        :return: a growth object with the emission reduction pathway
            based on that of the sector, but multiplied by the ratio
            provided
        :rtype: Growth
        """
        sector_growth_obj = Growth.from_predefined_scenario(
            selector=selector,
            base_year=base_year,
            target_year=target_year,
            scenario=scenario,
            ssp_selector=ssp_scenario,
        )
        entity_growth_obj = cls.create_entity_pathway_from_sector_path(
            sector_growth_obj,
            company_country,
            company_base_year_eei,
            sector_base_year_eei,
            location_shares,
            ssp_scenario,
            pathway,
            emissions_to_xdc,
        )
        logging.debug("Initialized growth object with: %s", entity_growth_obj)

        return entity_growth_obj

    @classmethod
    def __get_SB(cls, country: str):
        """Set up a sovereign bonds object per area in cache or retreive
        one form cache if it already exists.

        :param country: String identifying the country using ISO3
        :type country: str
        :return: Growth object for the sovereign bond
        :rtype: Growth
        """

        area = SovereignBondsReader.get_region_from_country(country)

        if area not in Growth.sb_cache:
            logging.info(
                "Performing new SB calculation in Growth class (%s/%s).",
                country, area)
            sbx = SovereignBondsReader()
            # sbx.run_calculation(area)
            # sbx.run_calculation_all()
            Growth.sb_cache[area] = sbx
        else:
            logging.info(
                "Loading cached SB calculation in Growth class (%s/%s).",
                country, area)
            sbx = Growth.sb_cache[area]
        return sbx

    @classmethod
    def from_SB_baseline(cls, country: str, base_year: int, target_year: int):
        """Set growth rates for baseline growth of sovereign bonds.
        Methodology to be determined

        :param country: String identifying the country using ISO3
        :type country: str
        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :return: Growth object for the sovereign bond
        :rtype: Growth
        """

        sbx = Growth.__get_SB(country)
        #country = country.value
        entity_growth = np.array(
            sbx.get_population_growth_curve(country, base_year,
                                            target_year)[0])
        emissions_growth = np.array(
            sbx.get_emissions_baseline_growth_curve(country, base_year,
                                                    target_year)[0])

        growth_obj = cls(base_year=base_year, target_year=target_year)
        growth_obj.validate_array_length(entity_growth)
        growth_obj.entity_growth = entity_growth
        growth_obj.validate_array_length(emissions_growth)
        growth_obj.emissions_growth = emissions_growth
        logging.debug("Initialized growth object with: %s", growth_obj)

        return growth_obj

    @classmethod
    def from_SB_target(cls, country: str, base_year: int, target_year: int):
        """Set growth rates for baseline growth of sovereign bonds.
        Methodology to be determined

        :param country: String identifying the country using ISO3
        :type country: str
        :param base_year: year in which the projection starts
        :type base_year: int
        :param target_year: year up to which the projection runs
        :type target_year: int
        :return: Growth object for the sovereign bond
        :rtype: Growth
        """

        sbx = Growth.__get_SB(country)
        #country = country.value
        entity_growth = np.array(
            sbx.get_population_growth_curve(country, base_year,
                                            target_year)[0])
        emissions_growth = np.array(
            sbx.get_emissions_target_growth_curve(country, base_year,
                                                  target_year)[0])

        growth_obj = cls(base_year=base_year, target_year=target_year)
        growth_obj.validate_array_length(entity_growth)
        growth_obj.entity_growth = entity_growth
        growth_obj.validate_array_length(emissions_growth)
        growth_obj.emissions_growth = emissions_growth
        logging.debug("Initialized growth object with: %s", growth_obj)

        return growth_obj

    @classmethod
    def get_baseline_amount_curve(cls, base_year: int, target_year: int,
                                  country_code: CountryCode, ssp_scenario: str,
                                  location_shares: list):
        growth_country, growth_region, growth_global = cls.\
                from_baseline_assumptions(base_year=base_year,
                                          target_year=target_year,
                                          country=country_code,
                                          ssp_scenario=ssp_scenario,
                                          gas="kyotoGases")

        country_gva_factor = EconomicAdjustmentsHelper.get_gva_adjustment_factor(
            year=base_year, country=country_code)
        region_gva_factor = EconomicAdjustmentsHelper.get_gva_adjustment_factor(
            year=base_year,
            region=DataApiHelper.get_region_by_country(country_code))
        world_gva_factor = EconomicAdjustmentsHelper.get_gva_adjustment_factor(
            year=base_year)
        growths = [growth_country, growth_region, growth_global]
        factors = [country_gva_factor, region_gva_factor, world_gva_factor]
        total_gva_growth = sum([
            f * l * g.get_entity_amount_curve()
            for f, l, g in zip(factors, location_shares, growths)
        ])
        total_gva_growth = total_gva_growth / total_gva_growth[0]
        #print(total_gva_growth)
        return total_gva_growth

    def get_entity_amount_curve(self) -> np.array:
        """Return the growth curve for only the entity amount (not
        multiplied by emissions or world amount)

        :return: np.array containing the growth curve of the amount
        :rtype: np.array
        """

        return self.entity_growth

    def get_entity_emission_curve(self) -> np.array:
        """Return the growth curve for only the emissions (not
        multiplied by emissions or world amount)

        :return: np.array containing the growth curve of emissions
        :rtype: np.array
        """

        return self.emissions_growth

    #@staticmethod
    def get_world_amount_curve(self, ) -> np.array:
        """Return the growth curve for only the world amount (not
        multiplied by emissions or world amount)

        :return: np.array containing the growth curve of world amout
        :rtype: np.array
        """
        rates = self._get_regional_growth_rates(Region.WORLD)
        number_of_years = self.target_year - self.base_year
        world = get_grown_array_from_value(number_of_years, 1,
                                           rates["world_growth"])

        return world

    def get_eei_curve(self) -> np.array:
        """Return the growth curve for entity emissions/entity amount growth
        :raises ZeroDivisionError: Error if amount growht is 0
        :return: np.array containing the growth curve of EEI
        :rtype: np.array
        """

        if np.any(self.entity_growth == 0) or np.any(
                self.entity_growth is None):
            raise ZeroDivisionError
            # ("Entity amount has reached 0 for %i" %
            # (self.base_year + j))
        eei = self.emissions_growth / self.entity_growth
        return eei

    def _get_regional_growth_rates(self, region: Region) -> dict:
        """Return regional growth-rates for company, world

        :param region: region defining the growth rates
        :type region: Region
        :raises XdcError: Error if the parameter is not a region
        :raises NotImplementedError: Error for non-existant targetyear
        :raises NotImplementedError: Error for undefined region
        :return: dictionary with the growth rates for that region
        :rtype: dict
        """
        if type(region) is not Region:
            msg = " region must be of type Region"
            raise XdcError(ErrorCodes.ERR_WRONG_TYPE, region, msg)
        if self.target_year not in consts.rates.keys():
            raise NotImplementedError("The growth rates for this target year\
                                       have not been determined yet ")
        if region.value not in consts.rates[self.target_year].keys():
            raise NotImplementedError("The growth rates for this region have\
                                       not been implemented yet ")
        return consts.rates[self.target_year][region.value]

    def _get_sector_growth_curves(self, region: Region) -> np.array:
        """Internal function to get the growth curve of the sector to
        normalize the IEA budgets to EEI growth only.

        :param region: Region for the growth assumptions
        :type region: Region
        :return: np.array with the sector growth curve
        """
        rates = self._get_regional_growth_rates(region)
        number_of_years = self.target_year - self.base_year
        return get_grown_array_from_value(number_of_years, 1,
                                          rates["world_growth"])

    @staticmethod
    def validate_time_period(
            base_year: int,
            target_year: int,
            base_year_range: (int, int),
            target_year_range: (int, int),
    ):
        """Verify that the input base year and target year are valid

        :param base_year: start of the growth curve
        :type base_year: int
        :param target_year: end of the growth curve
        :type target_year: int
        :param base_year_range: range of valid start years
        :type base_year_range: [type]
        :param target_year_range: range of valid target_years
        :type target_year_range: [type]
        :raises XdcError: Raises error if the base_year is outside the
        base_year range
        :raises XdcError: Raises error if the target_year is outside the
        target_year range
        :raises XdcError: Raises error if the period between base and
        target year is less than 10 years.
        """
        if type(base_year) != int or type(target_year) != int:
            msg = "The base and target year must be integers"
            raise XdcError(ErrorCodes.ERR_WRONG_TYPE, (base_year, target_year),
                           msg)
        if base_year < min(base_year_range):
            msg = "The growth can only be calculated from %i onwards" % min(
                base_year_range)
            raise XdcError(ErrorCodes.ERR_BELOW_MIN, base_year, msg)
        if base_year > max(base_year_range):
            msg = "The growth can only be calculated from the present onwards"
            raise XdcError(ErrorCodes.ERR_ABOVE_MAX, base_year, msg)
        if target_year < min(target_year_range):
            msg = "The target_year must be after %i" % min(target_year_range)
            raise XdcError(ErrorCodes.ERR_BELOW_MIN, base_year, msg)
        if target_year > max(target_year_range):
            msg = "The target_year must be before %i" % max(target_year_range)
            raise XdcError(ErrorCodes.ERR_ABOVE_MAX, base_year, msg)
        if target_year - base_year <= 10:
            msg = "The minimum period between base and target year is 10 years"
            raise XdcError(ErrorCodes.ERR_INPUT_CONFLICT,
                           [target_year, base_year], msg)

    def validate_array_length(self, array_to_check: np.ndarray):
        """Check that the length of the array match the time period given
        by base year and target year

        :param array_to_check: array to be checked
        :type array_to_check: np.ndarray
        :raises XdcError: ERR_BAD_LENGTH, aborts if the array has the
                          wrong length
        """
        # TODO: Find a way to pass the name of the array checked, to make error
        #  more explicit
        n_entries = self.target_year - self.base_year + 1
        if array_to_check.shape[0] != n_entries:
            msg = "The arrays must be of length %i, currently %i" % (
                n_entries,
                len(array_to_check),
            )
            raise XdcError(ErrorCodes.ERR_BAD_LENGTH, array_to_check, msg)

    @staticmethod
    # @lru_cache?
    def get_world_growth_curve(base_year: int,
                               target_year: int,
                               ssp_scenario: str = 'ssp2',
                               ssp_forcing: str = 'BASELINE'):
        """Get world growth curve

        By retrieving global GDP from the database and normalizing the
        values to a growth curve.

        :param base_year: Base year of calculation 
        :type base_year: int
        :param target_year: Target year of calculation 
        :type target_year: int
        :param ssp_scenario: SSP Scenario used to retrieve data from the data base
        :type ssp_scenario: str
        :param ssp_forcing: One of "RCP26" and "BASELINE"
        :type ssp_forcing: str
        :return: World growth curve from base to target year
        :rtype: np.array
        """
        #print(base_year, target_year, ssp_scenario, ssp_forcing)
        yearly_gdp_global = DataApiHelper.get_global_gdp(
            ssp_scenario, ssp_forcing)
        world_growth_curve = ScenarioReader.\
            get_normalized_curve_from_gdp(yearly_gdp_global, base_year,
                                          target_year)
        return world_growth_curve

    @staticmethod
    # @lru_cache?
    def get_null_curve(base_year: int, target_year: int):
        """Get a curve with zeroes.

        :param base_year: base year
        :type base_year: int
        :param target_year: target year
        :type target_year: int
        :return: empty curve
        :rtype: ndarray
        """
        null_curve = [0] * (target_year - base_year + 1)
        return np.array(null_curve)


class ScenarioReader:

    curve_cache = {}

    @staticmethod
    def _validate_scenario_selection(scenario_code: ScenarioCode,
                                     budget_selector: BudgetSelector):
        """Confirm that the scenario picked (2DS, RCP26) is compatible with
        the data source selected (IEA, RCP)

        :param scenario_code: Scenario to be picked from the budgets
        :type scenario_code: ScenarioCode
        :param budget_selector: data source and sector/gas to be used
        :type budget_selector: BudgetSelector
        """

        assert type(scenario_code) == ScenarioCode, ("scenario_code must be" +
                                                     " of type ScenarioCode")
        assert type(
            budget_selector
        ) == BudgetSelector, "budget_selector must be of type BudgetSelector"

        if scenario_code == ScenarioCode.RCP26:
            assert (
                budget_selector.scenario_data == "RCP_AGRICULTURE_BUDGET_DATA"
            ), "The scenario %s is not available from the selected source %s"\
                % (
                    scenario_code.value,
                    budget_selector.scenario_data,
                )
        elif scenario_code == ScenarioCode.SSP2B or scenario_code == ScenarioCode.SSP2T:
            assert (
                budget_selector.scenario_data == "SSP2_SCENARIO_DATA"
            ), "The scenario %s is not available from the selected source %s"\
                % (
                    scenario_code.value,
                    budget_selector.scenario_data,
                )
        else:
            assert (
                budget_selector.scenario_data == "IEA_ETP_ANNUAL_BUDGET_DATA"
            ), "The scenario %s is not available from the selected source %s"\
                % (
                    scenario_code.value,
                    budget_selector.scenario_data,
                )

    @staticmethod
    def _get_years(data_source: str) -> list:
        """Returns a list of years based on the data table selected

        :param data_source: name of the key to use in budget_data_SOURCES
        :type data_source: str
        :raises KeyError: Raises error when key doesn't exist
        :return: return list of years in the source
        :rtype: list
        """
        try:
            data = BUDGET_DATA_SOURCES[data_source]
        except KeyError:
            raise KeyError("The requested data source is not available. \
                            Please check the spelling")

        return list(data[0])[3:]

    @staticmethod
    def _get_emissions(region: Region, scenario_code: ScenarioCode,
                       budget_selector: BudgetSelector) -> np.array:
        """Returns the growth curve calculated from the budget data for region,
           scenario_code and the budget_selector

        :param region: region for which the scenario is selected
        :type region: Region
        :param scenario_code: scenario (2DS, B2DS) to be used
        :type scenario_code: ScenarioCode
        :param budget_selector: budget source (IEA, RCP) and budget_selector to be used
        :type budget_selector: BudgetSelector
        :return: The growth curve corresponding to the given scenario
        :rtype: np.array
        """
        ScenarioReader._validate_scenario_selection(scenario_code,
                                                    budget_selector)
        df = pd.DataFrame(
            BUDGET_DATA_SOURCES[budget_selector.scenario_data][1:],
            columns=BUDGET_DATA_SOURCES[budget_selector.scenario_data][0],
        )
        selected_row_df = df[(df["Region"] == region.value)
                             & (df["Scenario"] == scenario_code.value)
                             & (df["Selector"] == str(budget_selector))]
        selected_data = selected_row_df.drop(
            columns=["Region", "Scenario", "Selector"])
        data_list = selected_data.head().values.tolist()[0]
        data_list = np.array(data_list)

        if str(budget_selector) == "CH4":
            data_list = ch4_to_co2_equivalent(data_list)
        elif str(budget_selector) == "N2O":
            data_list = n2o_to_co2_equivalent(data_list)
        return data_list

    @classmethod
    def normalized_curve_of(cls,
                            region_code: Region,
                            scenario_code: ScenarioCode,
                            budget_selector: BudgetSelector,
                            base_year: int,
                            target_year: int,
                            rounding: int = 12,
                            budget_subtractors: list = []):

        years, curve = cls._curve_of(region_code=region_code,
                                     scenario_code=scenario_code,
                                     budget_selector=budget_selector,
                                     base_year=base_year,
                                     target_year=target_year,
                                     rounding=rounding)

        for budget_subtractor in budget_subtractors:
            subyears, subcurve = cls._curve_of(
                region_code=region_code,
                scenario_code=scenario_code,
                budget_selector=budget_subtractor,
                base_year=base_year,
                target_year=target_year,
                rounding=rounding)
            assert (years.all() == subyears.all())
            curve = np.array([a - b for a, b in zip(curve, subcurve)])

        # print("years %s" % years)
        # print("curve %s" % curve)
        # print("Curve:", list(zip(years, curve)))
        base_year_emissions = curve[years == base_year]
        # print("base_year_emissions %s" % base_year_emissions)
        base_year_index = np.argmax(years == base_year)
        num_years = target_year - base_year + 1
        requested_curve = curve[base_year_index:base_year_index + num_years]
        requested_curve = np.round(requested_curve / base_year_emissions,
                                   rounding)

        return requested_curve

    @classmethod
    def _curve_of(
        cls,
        region_code: Region,
        scenario_code: ScenarioCode,
        budget_selector: BudgetSelector,
        base_year: int,
        target_year: int,
        rounding: int = 12,
    ):
        """Get the intrapolated curve of the specified iea_budget

        :param region_code: Worldregion for which the budget should be selected
        :type region_code: Region
        :param scenario_code: Scenario for the selected budget
        :type scenario_code: ScenarioCode
        :param budget_selector: Sector for the selected budget
        :type budget_selector: BudgetSelector
        :param base_year: Start_year of the intrapoplated curve
        :type base_year: int
        :param target_year: Target_year of the intrapolated curve
        :type target_year: int
        :param rounding: Number of decimal places to round the growth curve to
        type rounding: int
        :raises NotImplementedError: There are not agricultural path for any
        region that is not Region.WORLD
        :raises NotImplementedError: Base_year is outside of the IEA budgets
        :return: normalized curve from start_year to target_year for selected
        IEA/RCP budget
        :rtype: np.array
        """
        ScenarioReader._validate_scenario_selection(scenario_code,
                                                    budget_selector)
        if scenario_code == ScenarioCode.RCP26 and region_code != Region.WORLD:
            raise NotImplementedError(
                "The agricultural curves are only available for region world")

        # Check if base_year and scenario codes are valid pairs
        if scenario_code == ScenarioCode.RCP26 and base_year <= 2015:
            msg = "Base year of %s is not available for\
                %s" % (base_year, scenario_code)
            logging.error(msg)
            raise NotImplementedError(msg)
        elif scenario_code == ScenarioCode.BTDS and base_year <= 2013:
            msg = "Base year of %s is not available for\
                %s" % (base_year, scenario_code)
            logging.error(msg)
            raise NotImplementedError(msg)
        elif base_year < 2013:
            msg = "Base year of %s is not available for\
                %s" % (base_year, scenario_code)
            logging.error(msg)
            raise NotImplementedError(msg)

        cache_key = str(region_code.value + "_" + scenario_code.value + "_" +
                        str(budget_selector))

        if cache_key not in ScenarioReader.curve_cache.keys():
            budget = ScenarioReader._get_emissions(region_code, scenario_code,
                                                   budget_selector)
            years = ScenarioReader._get_years(
                data_source=budget_selector.scenario_data)
            # Remove the entry for 2013 if B2DS is selected
            if np.isnan(budget[0]):
                budget = budget[1:]
                years = years[1:]
                logging.info(
                    "Budget in base_year %s is None, interpolating from %s",
                    base_year, list(zip(years, budget)))
            interpolation_func = interp1d(years, budget)

            years = np.array(range(years[0], years[-1] + 1))
            interpolated_curve = interpolation_func(years)
            ScenarioReader.curve_cache[cache_key] = years, interpolated_curve

        return ScenarioReader.curve_cache[cache_key]

    @staticmethod
    def get_normalized_curve_from_gdp(curve: list,
                                      base_year: int,
                                      target_year: int,
                                      country: bool = False) -> np.array:
        """[summary]

        [extended_summary]

        :param curve: [description]
        :type curve: list
        :return: [description]
        :rtype: np.array
        """
        # if country set start year to 2010, else start year is 2005
        start_year = 2010 if country else 2005
        base_year_index = base_year - start_year
        num_years = target_year - base_year + 1
        years = [entry["year"] for entry in curve]
        gdps = [entry["gdp"] for entry in curve]

        interpolation_function = interp1d(years, gdps)
        years = np.array(range(years[0], years[-1], 1))
        interpolated_curve = interpolation_function(years)
        curve = interpolated_curve[base_year_index:base_year_index + num_years]
        normalized_curve = np.array([point / curve[0] for point in curve])

        return normalized_curve

    def get_normalized_curve_from_emissions(curve: list, base_year: int,
                                            target_year: int,
                                            gas: str) -> np.array:
        """[summary]

        [extended_summary]

        :param curve: [description]
        :type curve: list
        :return: [description]
        :rtype: np.array
        """

        base_year_index = base_year - 2005
        num_years = target_year - base_year + 1
        years = [entry["year"] for entry in curve]
        if gas in ["N2O", "CH4", "CO2"]:
            try:
                emissions = [entry["%sAfolu" % gas.lower()] for entry in curve]
            except:
                emissions = [
                    entry["%sLulucf" % gas.lower()] for entry in curve
                ]
        else:
            emissions = [entry[gas] for entry in curve]

        interpolation_function = interp1d(years, emissions)
        years = np.array(range(years[0], years[-1], 1))
        interpolated_curve = interpolation_function(years)
        curve = interpolated_curve[base_year_index:base_year_index + num_years]

        normalized_curve = np.array([point / curve[0] for point in curve])

        return normalized_curve
