#!/usr/bin/python3
# -*- coding: utf-8 -*-
import logging
import uuid
import numpy as np
from copy import deepcopy

logging.getLogger(__name__)


class Scope:
    """Class containing the EEI pathway and weight of a
    particular scope.
    """
    def __init__(self,
                 start_eei: float = None,
                 growth_curve: np.ndarray = None,
                 yearly_eei: np.ndarray = None,
                 scope_weight=1.0,
                 scope_name="",
                 scope_tags=[]):
        """Initiate the Scope object. Set object properties.
        Generate the obj uuid.

        :param start_eei: base_year eei
        :type start_eei: float
        :param growth_curve: growth rate with which the EEI grows, property of
        the Growth object
        :type growth_curve: np.ndarray
        :param yearly_eei: array of EEIs per year for the scope
        :type yearly_eei: np.ndarray
        :param scope_weight: weight of the scope
        :type scope_weight: float
        """

        self.scope_weight = scope_weight
        self.name = scope_name
        self.tags = scope_tags
        if yearly_eei is not None:
            self.yearly_eei = yearly_eei
        elif isinstance(start_eei, (float, int)) and growth_curve is not None:
            self.yearly_eei = start_eei * growth_curve
        else:
            msg = """Please input either an array of yearly EEI values
                ("yearly_eei") OR both a starting EEI value AND a
                growth curve ("start_eei" and "growth_curve")."""
            logging.error(msg)
            raise TypeError(msg)
        self.obj_uuid = str(uuid.uuid4())
        logging.debug("Created scope object with uuid: %s", self.obj_uuid)
        # TODO: Check if that logging can be speed up, yes it can :)
        #logging.debug(
        #    "Input parameters: start_eei: %s, growth_curve: %s, yearly_eei: %s, scope_weigth: %s",
        #    start_eei, growth_curve, yearly_eei, scope_weight)

    def __str__(self):
        """Returns a printable version of the scope object

        :return: String with the emissions paramters
        :rtype: str
        """
        return str(self.__dict__)

    def __repr__(self):
        """Returns a printable representation of the scope object

        :return: String with the emissions paramters
        :rtype: str
        """
        return str(self.__dict__)

    def __eq__(self, other):
        """Compares the object to other to check if they are identical.

        :param other: Growth object being compared
        :type other: Growth
        :return: Whether the two objects are identical
        :rtype: bool
        """
        return np.allclose(self.yearly_eei, other.yearly_eei, rtol=1.e-8) and\
            self.scope_weight == other.scope_weight

    def __add__(self, other):
        """Add up scopes and return a new scope.
        This will be used for portfolio XDCs and for the
        food manufacturing mode in scope3.

        :param other: Scope to be added to existing scope
        :type other: Scope

        :raises ValueError: If the yearly_eei shapes of
            the scopes to be added together do not match
        :raised ValueError: If weights of the scopes to be
        summed up are not equal

        :return: weighted sum of scopes
        :rtype: Scope
        """
        if self.yearly_eei.shape != other.yearly_eei.shape:
            logging.error("Scope EEI arrays must have the same shape")
            raise ValueError("Scope EEI arrays must have the same shape")

        if self.scope_weight != other.scope_weight:
            logging.error("Scopes to be added must have the same weight")
            raise ValueError("Scopes to be added must have the same weight")

        summed_yearly_eei = self.yearly_eei + other.yearly_eei
        return Scope(yearly_eei=summed_yearly_eei,
                     scope_weight=self.scope_weight)

    def __sub__(self, other):
        other = deepcopy(other)
        other.scale(-1 * other.scope_weight)
        other.scope_weight = 1
        return self + other

    def scale(self, scale: int or float or np.ndarray):
        """Multiply self.yearly_eei by a particular value or
        array of values (e.g. a growth curve).

        :param scale: number(s) by which the scope should be scaled
        :type scale: int/float/np.ndarray
        """
        self.yearly_eei *= scale

    def get_eei(self) -> np.ndarray:
        """Return an array of the EEIs of the scope. This should be the
        pathway of the respective scope.

        :return: 1d array of EEI values from base_year to target_year
            for the scope
        :rtype: np.ndarray
        """
        return self.yearly_eei
