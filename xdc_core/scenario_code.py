import enum
import logging

module_logger = logging.getLogger(__name__)


class ScenarioCode(enum.Enum):
    """Definition of enum values:
        - 2DS: 2°C Scenario
        - B2DS: Beyond 2°C Scenario
        - RCP26: Representative Concentration Pathway 2.6
        - SSP2B: SSP2 Baseline Scenario
        - SSP2T: SSP2-2.6 Target Scenario
    """
    TDS = "2DS"
    BTDS = "B2DS"
    RCP26 = "RCP26"
    SSP2B = "SSP2B"
    SSP2T = "SSP2T"

    @staticmethod
    def value_of(value: str) -> enum.Enum:
        """Return the enum element (if any) associated to the value passed as
        argument

        :param value: Required value
        :type value: str
        :return: The enum element with value equal to the parameter
        :rtype: enum.Enum
        """
        result_scenario = None
        for m, mm in ScenarioCode.__members__.items():
            if mm.value == value.upper():
                result_scenario = mm

        if result_scenario is None:
            #warn_msg = 'ScenarioCode %s does not exist.' % value
            module_logger.warn('ScenarioCode %s does not exist.' , value)

        return result_scenario

    def __str__(self):
        return self.value

    def __eq__(self, other):
        return self.value == other.value and self.name == other.name

    def __hash__(self):
        return hash(str(self))
