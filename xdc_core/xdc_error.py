import enum
import sys


class XdcError(Exception):
    """Custom Exception
    """
    def __init__(self, error_code, varname, message='', *args, **kwargs):

        # Raise a separate exception in case the error code passed isn't specified in the ErrorCodes enum
        if not isinstance(error_code, ErrorCodes):
            msg = 'Error code passed in the error_code param must be of type {0}'
            raise XdcError(ErrorCodes.ERR_INCORRECT_ERRCODE, msg,
                           ErrorCodes.__class__.__name__)

        # Storing the error code on the exception object
        self.error_code = error_code

        # variable throwing the error
        self.varname = varname

        # store error message passed to the error
        self.msg = message

        # storing the traceback which provides useful information about where the exception occurred
        self.traceback = sys.exc_info()
        # Prefixing the error code to the exception message
        try:
            msg = '[{0}] {1}'.format(error_code.name,
                                     message.format(*args, **kwargs))
        except (IndexError, KeyError, AttributeError):
            msg = '[{0}] {1}'.format(error_code.name, message)

        super().__init__(msg)


# Error codes for all module exceptions
# @unique
class ErrorCodes(str, enum.Enum):
    # error code passed is not specified in enum ErrorCodes
    ERR_INCORRECT_ERRCODE = enum.auto()
    ERR_BELOW_MIN = 401  # <varname> - Entered value too small
    ERR_ABOVE_MAX = 402  # MAX_ <varname> - Entered value too large
    ERR_MISSING_VAL = 403  # MISSING_ <varname> - Expected value is missing
    ERR_NAN_VAL = 404  # NAN_ <varname> - The entered value is not a number
    ERR_WRONG_TYPE = 405  # TYPE_ <varname> - The entered value is not of the expected type
    # TARGET_UNREACHABLE - The specified target XDC value cannot be achieved by reducing emissions.
    ERR_TARGET_UNREACH = 406
    ERR_JSON_FAIL = 407  # JSON_FAIL - Input JSON cannot be read
    ERR_CALC_FAIL = 408  # CALC_FAIL - Calculation failed for unknown reason
    ERR_INPUT_CONFLICT = 409  # There are conflicting input parameter
    ERR_BAD_LENGTH = 410  # Length of an array is not as expected
    ERR_MISSING_DATA = 500  # Data for requested company not found in DB

    WARN_BELOW_MIN = 301  # <varname> - Entered value too small
    WARN_ABOVE_MAX = 302  # MAX_ <varname> - Entered value too large
    WARN_MISSING_VAL = 303  # MISSING_ <varname> - Expected value is missing
    WARN_NAN_VAL = 304  # NAN_ <varname> - The entered value is not a number
    WARN_WRONG_TYPE = 305  # TYPE_ <varname> - The entered value is not of the expected type
    # TARGET_UNREACHABLE - The specified target XDC value cannot be achieved by reducing emissions.
    WARN_TARGET_UNREACH = 306
    WARN_JSON_FAIL = 307  # JSON_FAIL - Input JSON cannot be read
    WARN_CALC_FAIL = 308  # CALC_FAIL - Calculation failed for unknown reason
    WARN_INPUT_CONFLICT = 309  # There are conflicting input parameters
