import enum
import logging

module_logger = logging.getLogger(__name__)


class Region(enum.Enum):
    """Definition of enum values:
        - OECD: Organisation for Economic Co-operation and Development
        - LAM: Latin America and the Caribbean
        - MAF: Middle East and Africa
        - REF: Eastern Europe and Former Soviet Union
        - ASIA: Asia excluding OECD countries
    As off July 2020, regions are only divided by OECD and world meaning
    that LAM, MAF, REF and ASIA are set to world.
    """
    OECD = "oecd"
    WORLD = "world"
    LAM = "LAM"
    MAF = "MAF"
    REF = "REF"
    ASIA = "ASIA"

    @staticmethod
    def value_of(value: str) -> enum.Enum:
        """Return the enum element (if any) associated to the value passed as 
        argument

        :param value: Required value
        :type value: str
        :return: The enum element with value equal to the parameter
        :rtype: enum.Enum
        """
        if value is None:
            return None
        value = value.strip()
        result_region = None
        for m, mm in Region.__members__.items():
            if m == value.upper():
                result_region = mm

        if result_region is None:
            #warn_msg = 'Region %s does not exist.' % value
            module_logger.warn('Region %s does not exist.', value)

        return result_region

    def __str__(self):
        return self.value

    def __eq__(self, other):
        return self.value == other.value and self.name == other.name

    def __hash__(self):
        return hash(str(self))
