## XDC Model

This is the python implementation of the XDC Model created by right. based on science GmbH.

## Repository Status
The code in this repository represents and old version of the XDC Model compared to the one, which is currently used in production. The aim of this repository is to enable users, or any interested party, to understand and familiarize with the basic concepts of the XDC methodology. For now, the goal is not to provide a runnable version of the model and this repository should be considered for learning purposes only. In the future, the repository will be updated with a more current version of the XDC Model in a way that it will be possible to check it out and run it on local machines.

## Visuals
The key idea behind the XDC Model calculation can be summarized by the following slide.
![4 Steps](/Assets/4_steps.png)

## Usage
In order to support the understanding of the XDC Model logic and how it is used in the products, the `XDCCalcTool` module has been provided as an example. 
A `XDCCalcTool` object can be created passing as arguments information such as the base and target year or the target scenarios.
The `XDCCalcTool` object has then dedicated functions to trigger the calculation process such as `run_calculation()` and `run_calculation_wrapped()` thanks to which it's possible to iterate through a dataframe of companies.
Withing those function, the baseline XDC is calculated through the following steps:
- `calc_baseline_emissions()`
- `add_to_xdc_df()` that allows to calculate a portfolio XDC
- the world growth curve is computed with the `get_world_growth_curve()` frunction from the `Growth` class
- emissions are upscaled to the global level through the `get_scaled_emission` from the `Emission` class
- at this point the array of yearly global emissions from base year to target year can be passed as input to the FaIR Model via the `emissions_to_fair_xdc()` and `_run_fair()` functions from the `XDC` class

## Support
In case of questions, please contact the [XDC Model Team](model-team@right-basedonscience.de)

## Roadmap
Coming soon

## Contributing
As stated above, at the moment the content of this repository should be intended for learning purposes only. Therefore, no contributions are expected at the moment.
When the current production version of the XDC Model will be made available, a set of guidelines for contributions as well as the relevant documentation will be provided.

## License
The [GNU Affero General Public License](/LICENSE) (GNU AGPLv3) applies.

